# Squid splash
Not exactly Quip Lash

[![svelte](https://img.shields.io/badge/Svelte-v3-blue.svg)](https://svelte.dev)
[![svelte](https://img.shields.io/badge/Peerjs-v1-blue.svg)](https://peerjs.com/)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Introduction
Squid splash is an open source knock off of Quip Lash.  The main goal was to create a serverless version of the game which runs peer to peer from a single html file.  

It's also a learning experiment for me to get familiar with svelte and webrtc

Very much still a work in progress.

NB as stands the current question list contains questions that are not suitable for work or children.

## Installation

To just run the game copy the public folder to a web server or you can run directly open the files in a web browser if you want to try it on a single machine.

To set up a development environment install Svete and install the required node modules - see change required to peerjs below.


## Requirements
Squid splash requires the excellent https://peerjs.com/ library however to get it to run from the node modules folder I had to add the following line:

`window.global = window;`

before

`var parcelRequire = ...`



https://svelte.dev/ if you want to set up a development environment


## Sources
The original list of questions came from https://github.com/yodigi7/quiplash - Many thanks

## Contributioms

Contributions are welcome - I'm particularly keen to see lots more cool questions added to the questions.js file

## License
GPLv3

## Todo
* testing environment and tests - https://github.com/testing-library/svelte-testing-library ?
* add min players check - smallest number of players required is 3
* change display of votes to separate component?
* make game code case insensitive
* fix back to start reset - or just reload page
* countdown timers on answer and voting and on scores
* stop multiple votes and provide voting feedback
* stop players voting on their own answer or when there is one other answer
* overall look and feel with animations
* option to play again with same players
* more interesting scoring mechanism - ability for players to add one bonus per round
* final round where everyone answers same question?
* Add svg animation to countdown
* incorporate blob characters to represent players
* add sound effects - scoring sound plus new round splash?
* voice over?
* more questions
* yet more questions
* count of votes and sound after voting
* speech bubble with person who answered after voting
* change results of voting to separate component?
* points animation on voted screen (change expressions)