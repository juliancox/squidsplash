
(function(l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
var app = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    let src_url_equal_anchor;
    function src_url_equal(element_src, url) {
        if (!src_url_equal_anchor) {
            src_url_equal_anchor = document.createElement('a');
        }
        src_url_equal_anchor.href = url;
        return element_src === src_url_equal_anchor.href;
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function validate_store(store, name) {
        if (store != null && typeof store.subscribe !== 'function') {
            throw new Error(`'${name}' is not a store with a 'subscribe' method`);
        }
    }
    function subscribe(store, ...callbacks) {
        if (store == null) {
            return noop;
        }
        const unsub = store.subscribe(...callbacks);
        return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }
    function component_subscribe(component, store, callback) {
        component.$$.on_destroy.push(subscribe(store, callback));
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }
    function update_slot_base(slot, slot_definition, ctx, $$scope, slot_changes, get_slot_context_fn) {
        if (slot_changes) {
            const slot_context = get_slot_context(slot_definition, ctx, $$scope, get_slot_context_fn);
            slot.p(slot_context, slot_changes);
        }
    }
    function get_all_dirty_from_scope($$scope) {
        if ($$scope.ctx.length > 32) {
            const dirty = [];
            const length = $$scope.ctx.length / 32;
            for (let i = 0; i < length; i++) {
                dirty[i] = -1;
            }
            return dirty;
        }
        return -1;
    }
    function null_to_empty(value) {
        return value == null ? '' : value;
    }
    function set_store_value(store, ret, value) {
        store.set(value);
        return ret;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function svg_element(name) {
        return document.createElementNS('http://www.w3.org/2000/svg', name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        input.value = value == null ? '' : value;
    }
    function set_style(node, key, value, important) {
        if (value === null) {
            node.style.removeProperty(key);
        }
        else {
            node.style.setProperty(key, value, important ? 'important' : '');
        }
    }
    function custom_event(type, detail, { bubbles = false, cancelable = false } = {}) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, cancelable, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function afterUpdate(fn) {
        get_current_component().$$.after_update.push(fn);
    }
    function onDestroy(fn) {
        get_current_component().$$.on_destroy.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail, { cancelable = false } = {}) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail, { cancelable });
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
                return !event.defaultPrevented;
            }
            return true;
        };
    }
    // TODO figure out if we still want to support
    // shorthand events, or if we want to implement
    // a real bubbling mechanism
    function bubble(component, event) {
        const callbacks = component.$$.callbacks[event.type];
        if (callbacks) {
            // @ts-ignore
            callbacks.slice().forEach(fn => fn.call(this, event));
        }
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    // flush() calls callbacks in this order:
    // 1. All beforeUpdate callbacks, in order: parents before children
    // 2. All bind:this callbacks, in reverse order: children before parents.
    // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
    //    for afterUpdates called during the initial onMount, which are called in
    //    reverse order: children before parents.
    // Since callbacks might update component values, which could trigger another
    // call to flush(), the following steps guard against this:
    // 1. During beforeUpdate, any updated components will be added to the
    //    dirty_components array and will cause a reentrant call to flush(). Because
    //    the flush index is kept outside the function, the reentrant call will pick
    //    up where the earlier call left off and go through all dirty components. The
    //    current_component value is saved and restored so that the reentrant call will
    //    not interfere with the "parent" flush() call.
    // 2. bind:this callbacks cannot trigger new flush() calls.
    // 3. During afterUpdate, any updated components will NOT have their afterUpdate
    //    callback called a second time; the seen_callbacks set, outside the flush()
    //    function, guarantees this behavior.
    const seen_callbacks = new Set();
    let flushidx = 0; // Do *not* move this inside the flush() function
    function flush() {
        const saved_component = current_component;
        do {
            // first, call beforeUpdate functions
            // and update components
            while (flushidx < dirty_components.length) {
                const component = dirty_components[flushidx];
                flushidx++;
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            flushidx = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        seen_callbacks.clear();
        set_current_component(saved_component);
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
        else if (callback) {
            callback();
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.49.0' }, detail), { bubbles: true }));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    const subscriber_queue = [];
    /**
     * Creates a `Readable` store that allows reading by subscription.
     * @param value initial value
     * @param {StartStopNotifier}start start and stop notifications for subscriptions
     */
    function readable(value, start) {
        return {
            subscribe: writable(value, start).subscribe
        };
    }
    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     * @param {*=}value initial value
     * @param {StartStopNotifier=}start start and stop notifications for subscriptions
     */
    function writable(value, start = noop) {
        let stop;
        const subscribers = new Set();
        function set(new_value) {
            if (safe_not_equal(value, new_value)) {
                value = new_value;
                if (stop) { // store is ready
                    const run_queue = !subscriber_queue.length;
                    for (const subscriber of subscribers) {
                        subscriber[1]();
                        subscriber_queue.push(subscriber, value);
                    }
                    if (run_queue) {
                        for (let i = 0; i < subscriber_queue.length; i += 2) {
                            subscriber_queue[i][0](subscriber_queue[i + 1]);
                        }
                        subscriber_queue.length = 0;
                    }
                }
            }
        }
        function update(fn) {
            set(fn(value));
        }
        function subscribe(run, invalidate = noop) {
            const subscriber = [run, invalidate];
            subscribers.add(subscriber);
            if (subscribers.size === 1) {
                stop = start(set) || noop;
            }
            run(value);
            return () => {
                subscribers.delete(subscriber);
                if (subscribers.size === 0) {
                    stop();
                    stop = null;
                }
            };
        }
        return { set, update, subscribe };
    }
    function derived(stores, fn, initial_value) {
        const single = !Array.isArray(stores);
        const stores_array = single
            ? [stores]
            : stores;
        const auto = fn.length < 2;
        return readable(initial_value, (set) => {
            let inited = false;
            const values = [];
            let pending = 0;
            let cleanup = noop;
            const sync = () => {
                if (pending) {
                    return;
                }
                cleanup();
                const result = fn(single ? values[0] : values, set);
                if (auto) {
                    set(result);
                }
                else {
                    cleanup = is_function(result) ? result : noop;
                }
            };
            const unsubscribers = stores_array.map((store, i) => subscribe(store, (value) => {
                values[i] = value;
                pending &= ~(1 << i);
                if (inited) {
                    sync();
                }
            }, () => {
                pending |= (1 << i);
            }));
            inited = true;
            sync();
            return function stop() {
                run_all(unsubscribers);
                cleanup();
            };
        });
    }

    const state = writable('welcome');
    const servercode = writable('');
    const clientcode = writable('');
    const player = writable(null);
    const players = writable([]);
    const numrounds = writable(1);
    const playersCanVoteOnOwnAnswers = writable(true);
    const round = writable(0);
    const answer = writable();
    const question = writable();
    const ballot = writable();
    const vote = writable();
    const myPeerId = writable();
    const serverCommand = writable();

    const gamecode$2 = derived([servercode, clientcode], ([$servercode, $clientcode]) => $servercode ? $servercode : $clientcode);
    const isServer = derived([servercode, clientcode], ([$servercode, $clientcode]) => $servercode ? true : false);
    const playerKeys$1 = derived([players], ([$players]) => Object.keys($players));
    const numPlayers = derived([playerKeys$1], ([$playerKeys]) => $playerKeys.length);
    // export const allVoted = derived([ballot, numPlayers], ([$ballot, $numplayers]) => $ballot && $ballot[2] && ($ballot[2].reduce((count, answer) => count + answer[2].length, 0) >= $numplayers) ? $ballot : false);
    const allVoted = derived([ballot, numPlayers], ([$ballot, $numplayers]) => $ballot && $ballot[3] && ($ballot[3].length == $numplayers) ? $ballot : false);
    derived([ballot, player], ([$ballot, $myPeerId]) => $ballot && $ballot[2] && ($ballot[2].reduce((combined, answer) => combined.concat(answer[2]), []).indexOf($myPeerId) >= 0));

    const timeoutAnswer = 'Doh! Too slow to answer';

    const resetAllState = function () {
        state.set('welcome');
        servercode.set('');
        clientcode.set('');
        player.set(null);
        players.set([]);
        numrounds.set(1);
        playersCanVoteOnOwnAnswers.set(true);
        round.set(0);
        answer.set(null);
        question.set(null);
        ballot.set(null);
        vote.set(null);
        myPeerId.set(null);
        serverCommand.set(null);
    };

    const resetGame = function() {
        round.set(0);
        answer.set(null);
        question.set(null);
        ballot.set(null);
        vote.set(null);
    };

    function getDefaultExportFromCjs (x) {
    	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
    }

    function createCommonjsModule(fn) {
      var module = { exports: {} };
    	return fn(module, module.exports), module.exports;
    }

    function commonjsRequire (target) {
    	throw new Error('Could not dynamically require "' + target + '". Please configure the dynamicRequireTargets option of @rollup/plugin-commonjs appropriately for this require call to behave properly.');
    }

    var peerjs_min = createCommonjsModule(function (module, exports) {
    // modules are defined as an array
    // [ module function, map of requires ]
    //
    // map of requires is short require name -> numeric require
    //
    // anything defined in a previous bundle is accessed via the
    // orig method which is the require for previous bundles

    // eslint-disable-next-line no-global-assign
    window.global = window;
    var parcelRequire = (function(modules, cache, entry, globalName) {
            // Save the require from previous bundle to this closure if any
            var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
            var nodeRequire = typeof commonjsRequire === 'function' && commonjsRequire;

            function newRequire(name, jumped) {
                if (!cache[name]) {
                    if (!modules[name]) {
                        // if we cannot find the module within our internal map or
                        // cache jump to the current global require ie. the last bundle
                        // that was added to the page.
                        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
                        if (!jumped && currentRequire) {
                            return currentRequire(name, true);
                        }

                        // If there are other bundles on this page the require from the
                        // previous one is saved to 'previousRequire'. Repeat this as
                        // many times as there are bundles until the module is found or
                        // we exhaust the require chain.
                        if (previousRequire) {
                            return previousRequire(name, true);
                        }

                        // Try the node require function if it exists.
                        if (nodeRequire && typeof name === 'string') {
                            return nodeRequire(name);
                        }

                        var err = new Error('Cannot find module \'' + name + '\'');
                        err.code = 'MODULE_NOT_FOUND';
                        throw err;
                    }

                    localRequire.resolve = resolve;
                    localRequire.cache = {};

                    var module = cache[name] = new newRequire.Module(name);

                    modules[name][0].call(module.exports, localRequire, module, module.exports, this);
                }

                return cache[name].exports;

                function localRequire(x) {
                    return newRequire(localRequire.resolve(x));
                }

                function resolve(x) {
                    return modules[name][1][x] || x;
                }
            }

            function Module(moduleName) {
                this.id = moduleName;
                this.bundle = newRequire;
                this.exports = {};
            }

            newRequire.isParcelRequire = true;
            newRequire.Module = Module;
            newRequire.modules = modules;
            newRequire.cache = cache;
            newRequire.parent = previousRequire;
            newRequire.register = function(id, exports) {
                modules[id] = [function(require, module) {
                    module.exports = exports;
                }, {}];
            };

            for (var i = 0; i < entry.length; i++) {
                newRequire(entry[i]);
            }

            if (entry.length) {
                // Expose entry point to Node, AMD or browser globals
                // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
                var mainExports = newRequire(entry[entry.length - 1]);

                // CommonJS
                {
                    module.exports = mainExports;

                    // RequireJS
                }
            }

            // Override the current require with this new one
            return newRequire;
        })({
            "vHo1": [function(require, module, exports) {
                var e = {};
                e.useBlobBuilder = function() { try { return new Blob([]), !1 } catch (e) { return !0 } }(), e.useArrayBufferView = !e.useBlobBuilder && function() { try { return 0 === new Blob([new Uint8Array([])]).size } catch (e) { return !0 } }(), module.exports.binaryFeatures = e;
                var r = module.exports.BlobBuilder;

                function t() { this._pieces = [], this._parts = []; }
                "undefined" != typeof window && (r = module.exports.BlobBuilder = window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder || window.BlobBuilder), t.prototype.append = function(e) { "number" == typeof e ? this._pieces.push(e) : (this.flush(), this._parts.push(e)); }, t.prototype.flush = function() {
                    if (this._pieces.length > 0) {
                        var r = new Uint8Array(this._pieces);
                        e.useArrayBufferView || (r = r.buffer), this._parts.push(r), this._pieces = [];
                    }
                }, t.prototype.getBuffer = function() { if (this.flush(), e.useBlobBuilder) { for (var t = new r, i = 0, u = this._parts.length; i < u; i++) t.append(this._parts[i]); return t.getBlob() } return new Blob(this._parts) }, module.exports.BufferBuilder = t;
            }, {}],
            "lHOc": [function(require, module, exports) {
                var t = require("./bufferbuilder").BufferBuilder,
                    e = require("./bufferbuilder").binaryFeatures,
                    i = { unpack: function(t) { return new r(t).unpack() }, pack: function(t) { var e = new n; return e.pack(t), e.getBuffer() } };

                function r(t) { this.index = 0, this.dataBuffer = t, this.dataView = new Uint8Array(this.dataBuffer), this.length = this.dataBuffer.byteLength; }

                function n() { this.bufferBuilder = new t; }

                function u(t) { var e = t.charCodeAt(0); return e <= 2047 ? "00" : e <= 65535 ? "000" : e <= 2097151 ? "0000" : e <= 67108863 ? "00000" : "000000" }

                function a(t) { return t.length > 600 ? new Blob([t]).size : t.replace(/[^\u0000-\u007F]/g, u).length }
                module.exports = i, r.prototype.unpack = function() {
                    var t, e = this.unpack_uint8();
                    if (e < 128) return e;
                    if ((224 ^ e) < 32) return (224 ^ e) - 32;
                    if ((t = 160 ^ e) <= 15) return this.unpack_raw(t);
                    if ((t = 176 ^ e) <= 15) return this.unpack_string(t);
                    if ((t = 144 ^ e) <= 15) return this.unpack_array(t);
                    if ((t = 128 ^ e) <= 15) return this.unpack_map(t);
                    switch (e) {
                        case 192:
                            return null;
                        case 193:
                            return;
                        case 194:
                            return !1;
                        case 195:
                            return !0;
                        case 202:
                            return this.unpack_float();
                        case 203:
                            return this.unpack_double();
                        case 204:
                            return this.unpack_uint8();
                        case 205:
                            return this.unpack_uint16();
                        case 206:
                            return this.unpack_uint32();
                        case 207:
                            return this.unpack_uint64();
                        case 208:
                            return this.unpack_int8();
                        case 209:
                            return this.unpack_int16();
                        case 210:
                            return this.unpack_int32();
                        case 211:
                            return this.unpack_int64();
                        case 212:
                        case 213:
                        case 214:
                        case 215:
                            return;
                        case 216:
                            return t = this.unpack_uint16(), this.unpack_string(t);
                        case 217:
                            return t = this.unpack_uint32(), this.unpack_string(t);
                        case 218:
                            return t = this.unpack_uint16(), this.unpack_raw(t);
                        case 219:
                            return t = this.unpack_uint32(), this.unpack_raw(t);
                        case 220:
                            return t = this.unpack_uint16(), this.unpack_array(t);
                        case 221:
                            return t = this.unpack_uint32(), this.unpack_array(t);
                        case 222:
                            return t = this.unpack_uint16(), this.unpack_map(t);
                        case 223:
                            return t = this.unpack_uint32(), this.unpack_map(t)
                    }
                }, r.prototype.unpack_uint8 = function() { var t = 255 & this.dataView[this.index]; return this.index++, t }, r.prototype.unpack_uint16 = function() {
                    var t = this.read(2),
                        e = 256 * (255 & t[0]) + (255 & t[1]);
                    return this.index += 2, e
                }, r.prototype.unpack_uint32 = function() {
                    var t = this.read(4),
                        e = 256 * (256 * (256 * t[0] + t[1]) + t[2]) + t[3];
                    return this.index += 4, e
                }, r.prototype.unpack_uint64 = function() {
                    var t = this.read(8),
                        e = 256 * (256 * (256 * (256 * (256 * (256 * (256 * t[0] + t[1]) + t[2]) + t[3]) + t[4]) + t[5]) + t[6]) + t[7];
                    return this.index += 8, e
                }, r.prototype.unpack_int8 = function() { var t = this.unpack_uint8(); return t < 128 ? t : t - 256 }, r.prototype.unpack_int16 = function() { var t = this.unpack_uint16(); return t < 32768 ? t : t - 65536 }, r.prototype.unpack_int32 = function() { var t = this.unpack_uint32(); return t < Math.pow(2, 31) ? t : t - Math.pow(2, 32) }, r.prototype.unpack_int64 = function() { var t = this.unpack_uint64(); return t < Math.pow(2, 63) ? t : t - Math.pow(2, 64) }, r.prototype.unpack_raw = function(t) { if (this.length < this.index + t) throw new Error("BinaryPackFailure: index is out of range " + this.index + " " + t + " " + this.length); var e = this.dataBuffer.slice(this.index, this.index + t); return this.index += t, e }, r.prototype.unpack_string = function(t) { for (var e, i, r = this.read(t), n = 0, u = ""; n < t;)(e = r[n]) < 128 ? (u += String.fromCharCode(e), n++) : (192 ^ e) < 32 ? (i = (192 ^ e) << 6 | 63 & r[n + 1], u += String.fromCharCode(i), n += 2) : (i = (15 & e) << 12 | (63 & r[n + 1]) << 6 | 63 & r[n + 2], u += String.fromCharCode(i), n += 3); return this.index += t, u }, r.prototype.unpack_array = function(t) { for (var e = new Array(t), i = 0; i < t; i++) e[i] = this.unpack(); return e }, r.prototype.unpack_map = function(t) {
                    for (var e = {}, i = 0; i < t; i++) {
                        var r = this.unpack(),
                            n = this.unpack();
                        e[r] = n;
                    }
                    return e
                }, r.prototype.unpack_float = function() {
                    var t = this.unpack_uint32(),
                        e = (t >> 23 & 255) - 127;
                    return (0 == t >> 31 ? 1 : -1) * (8388607 & t | 8388608) * Math.pow(2, e - 23)
                }, r.prototype.unpack_double = function() {
                    var t = this.unpack_uint32(),
                        e = this.unpack_uint32(),
                        i = (t >> 20 & 2047) - 1023;
                    return (0 == t >> 31 ? 1 : -1) * ((1048575 & t | 1048576) * Math.pow(2, i - 20) + e * Math.pow(2, i - 52))
                }, r.prototype.read = function(t) { var e = this.index; if (e + t <= this.length) return this.dataView.subarray(e, e + t); throw new Error("BinaryPackFailure: read index out of range") }, n.prototype.getBuffer = function() { return this.bufferBuilder.getBuffer() }, n.prototype.pack = function(t) {
                    var i = typeof t;
                    if ("string" == i) this.pack_string(t);
                    else if ("number" == i) Math.floor(t) === t ? this.pack_integer(t) : this.pack_double(t);
                    else if ("boolean" == i) !0 === t ? this.bufferBuilder.append(195) : !1 === t && this.bufferBuilder.append(194);
                    else if ("undefined" == i) this.bufferBuilder.append(192);
                    else {
                        if ("object" != i) throw new Error('Type "' + i + '" not yet supported');
                        if (null === t) this.bufferBuilder.append(192);
                        else {
                            var r = t.constructor;
                            if (r == Array) this.pack_array(t);
                            else if (r == Blob || r == File) this.pack_bin(t);
                            else if (r == ArrayBuffer) e.useArrayBufferView ? this.pack_bin(new Uint8Array(t)) : this.pack_bin(t);
                            else if ("BYTES_PER_ELEMENT" in t) e.useArrayBufferView ? this.pack_bin(new Uint8Array(t.buffer)) : this.pack_bin(t.buffer);
                            else if (r == Object) this.pack_object(t);
                            else if (r == Date) this.pack_string(t.toString());
                            else {
                                if ("function" != typeof t.toBinaryPack) throw new Error('Type "' + r.toString() + '" not yet supported');
                                this.bufferBuilder.append(t.toBinaryPack());
                            }
                        }
                    }
                    this.bufferBuilder.flush();
                }, n.prototype.pack_bin = function(t) {
                    var e = t.length || t.byteLength || t.size;
                    if (e <= 15) this.pack_uint8(160 + e);
                    else if (e <= 65535) this.bufferBuilder.append(218), this.pack_uint16(e);
                    else {
                        if (!(e <= 4294967295)) throw new Error("Invalid length");
                        this.bufferBuilder.append(219), this.pack_uint32(e);
                    }
                    this.bufferBuilder.append(t);
                }, n.prototype.pack_string = function(t) {
                    var e = a(t);
                    if (e <= 15) this.pack_uint8(176 + e);
                    else if (e <= 65535) this.bufferBuilder.append(216), this.pack_uint16(e);
                    else {
                        if (!(e <= 4294967295)) throw new Error("Invalid length");
                        this.bufferBuilder.append(217), this.pack_uint32(e);
                    }
                    this.bufferBuilder.append(t);
                }, n.prototype.pack_array = function(t) {
                    var e = t.length;
                    if (e <= 15) this.pack_uint8(144 + e);
                    else if (e <= 65535) this.bufferBuilder.append(220), this.pack_uint16(e);
                    else {
                        if (!(e <= 4294967295)) throw new Error("Invalid length");
                        this.bufferBuilder.append(221), this.pack_uint32(e);
                    }
                    for (var i = 0; i < e; i++) this.pack(t[i]);
                }, n.prototype.pack_integer = function(t) {
                    if (-32 <= t && t <= 127) this.bufferBuilder.append(255 & t);
                    else if (0 <= t && t <= 255) this.bufferBuilder.append(204), this.pack_uint8(t);
                    else if (-128 <= t && t <= 127) this.bufferBuilder.append(208), this.pack_int8(t);
                    else if (0 <= t && t <= 65535) this.bufferBuilder.append(205), this.pack_uint16(t);
                    else if (-32768 <= t && t <= 32767) this.bufferBuilder.append(209), this.pack_int16(t);
                    else if (0 <= t && t <= 4294967295) this.bufferBuilder.append(206), this.pack_uint32(t);
                    else if (-2147483648 <= t && t <= 2147483647) this.bufferBuilder.append(210), this.pack_int32(t);
                    else if (-0x8000000000000000 <= t && t <= 0x8000000000000000) this.bufferBuilder.append(211), this.pack_int64(t);
                    else {
                        if (!(0 <= t && t <= 0x10000000000000000)) throw new Error("Invalid integer");
                        this.bufferBuilder.append(207), this.pack_uint64(t);
                    }
                }, n.prototype.pack_double = function(t) {
                    var e = 0;
                    t < 0 && (e = 1, t = -t);
                    var i = Math.floor(Math.log(t) / Math.LN2),
                        r = t / Math.pow(2, i) - 1,
                        n = Math.floor(r * Math.pow(2, 52)),
                        u = Math.pow(2, 32),
                        a = e << 31 | i + 1023 << 20 | n / u & 1048575,
                        p = n % u;
                    this.bufferBuilder.append(203), this.pack_int32(a), this.pack_int32(p);
                }, n.prototype.pack_object = function(t) {
                    var e = Object.keys(t).length;
                    if (e <= 15) this.pack_uint8(128 + e);
                    else if (e <= 65535) this.bufferBuilder.append(222), this.pack_uint16(e);
                    else {
                        if (!(e <= 4294967295)) throw new Error("Invalid length");
                        this.bufferBuilder.append(223), this.pack_uint32(e);
                    }
                    for (var i in t) t.hasOwnProperty(i) && (this.pack(i), this.pack(t[i]));
                }, n.prototype.pack_uint8 = function(t) { this.bufferBuilder.append(t); }, n.prototype.pack_uint16 = function(t) { this.bufferBuilder.append(t >> 8), this.bufferBuilder.append(255 & t); }, n.prototype.pack_uint32 = function(t) {
                    var e = 4294967295 & t;
                    this.bufferBuilder.append((4278190080 & e) >>> 24), this.bufferBuilder.append((16711680 & e) >>> 16), this.bufferBuilder.append((65280 & e) >>> 8), this.bufferBuilder.append(255 & e);
                }, n.prototype.pack_uint64 = function(t) {
                    var e = t / Math.pow(2, 32),
                        i = t % Math.pow(2, 32);
                    this.bufferBuilder.append((4278190080 & e) >>> 24), this.bufferBuilder.append((16711680 & e) >>> 16), this.bufferBuilder.append((65280 & e) >>> 8), this.bufferBuilder.append(255 & e), this.bufferBuilder.append((4278190080 & i) >>> 24), this.bufferBuilder.append((16711680 & i) >>> 16), this.bufferBuilder.append((65280 & i) >>> 8), this.bufferBuilder.append(255 & i);
                }, n.prototype.pack_int8 = function(t) { this.bufferBuilder.append(255 & t); }, n.prototype.pack_int16 = function(t) { this.bufferBuilder.append((65280 & t) >> 8), this.bufferBuilder.append(255 & t); }, n.prototype.pack_int32 = function(t) { this.bufferBuilder.append(t >>> 24 & 255), this.bufferBuilder.append((16711680 & t) >>> 16), this.bufferBuilder.append((65280 & t) >>> 8), this.bufferBuilder.append(255 & t); }, n.prototype.pack_int64 = function(t) {
                    var e = Math.floor(t / Math.pow(2, 32)),
                        i = t % Math.pow(2, 32);
                    this.bufferBuilder.append((4278190080 & e) >>> 24), this.bufferBuilder.append((16711680 & e) >>> 16), this.bufferBuilder.append((65280 & e) >>> 8), this.bufferBuilder.append(255 & e), this.bufferBuilder.append((4278190080 & i) >>> 24), this.bufferBuilder.append((16711680 & i) >>> 16), this.bufferBuilder.append((65280 & i) >>> 8), this.bufferBuilder.append(255 & i);
                };
            }, { "./bufferbuilder": "vHo1" }],
            "sXtV": [function(require, module, exports) {
                Object.defineProperty(exports, "__esModule", { value: !0 }), exports.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription, exports.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection, exports.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate;
            }, {}],
            "BHXf": [function(require, module, exports) {
                var n = this && this.__importStar || function(e) {
                        if (e && e.__esModule) return e;
                        var n = {};
                        if (null != e)
                            for (var r in e) Object.hasOwnProperty.call(e, r) && (n[r] = e[r]);
                        return n.default = e, n
                    };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var r = n(require("js-binarypack")),
                    t = require("./adapter"),
                    a = { iceServers: [{ urls: "stun:stun.l.google.com:19302" }], sdpSemantics: "unified-plan" },
                    o = function() {
                        function e() {}
                        var n;
                        return e.noop = function() {}, e.validateId = function(e) { return !e || /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/.test(e) }, e.chunk = function(n) {
                            for (var r, t = [], a = n.size, o = Math.ceil(a / e.chunkedMTU), i = r = 0; i < a;) {
                                var u = Math.min(a, i + e.chunkedMTU),
                                    c = n.slice(i, u),
                                    d = { __peerData: this._dataCount, n: r, data: c, total: o };
                                t.push(d), i = u, r++;
                            }
                            return this._dataCount++, t
                        }, e.blobToArrayBuffer = function(e, n) {
                            var r = new FileReader;
                            r.onload = function(e) { n(e.target.result); }, r.readAsArrayBuffer(e);
                        }, e.blobToBinaryString = function(e, n) {
                            var r = new FileReader;
                            r.onload = function(e) { n(e.target.result); }, r.readAsBinaryString(e);
                        }, e.binaryStringToArrayBuffer = function(e) { for (var n = new Uint8Array(e.length), r = 0; r < e.length; r++) n[r] = 255 & e.charCodeAt(r); return n.buffer }, e.randomToken = function() { return Math.random().toString(36).substr(2) }, e.isSecure = function() { return "https:" === location.protocol }, e.CLOUD_HOST = "0.peerjs.com", e.CLOUD_PORT = 443, e.chunkedBrowsers = { Chrome: 1 }, e.chunkedMTU = 16300, e.defaultConfig = a, e.browser = (n = window).mozRTCPeerConnection ? "Firefox" : n.webkitRTCPeerConnection ? "Chrome" : n.RTCPeerConnection ? "Supported" : "Unsupported", e.supports = function() {
                            if (void 0 === t.RTCPeerConnection) return {};
                            var e, n, r = !0,
                                o = !0,
                                i = !1,
                                u = !1,
                                c = !!window.webkitRTCPeerConnection;
                            try { e = new t.RTCPeerConnection(a, { optional: [{ RtpDataChannels: !0 }] }); } catch (l) { r = !1, o = !1; }
                            if (r) try { n = e.createDataChannel("_PEERJSTEST"); } catch (l) { r = !1; }
                            if (r) {
                                try { n.binaryType = "blob", i = !0; } catch (l) {}
                                var d = new t.RTCPeerConnection(a, {});
                                try { u = d.createDataChannel("_PEERJSRELIABLETEST", {}).ordered; } catch (l) {}
                                d.close();
                            }
                            return o && (o = !!e.addStream), e && e.close(), { audioVideo: o, data: r, binaryBlob: i, binary: u, reliable: u, sctp: u, onnegotiationneeded: c }
                        }(), e.pack = r.pack, e.unpack = r.unpack, e._dataCount = 1, e
                    }();
                exports.util = o;
            }, { "js-binarypack": "lHOc", "./adapter": "sXtV" }],
            "2JJl": [function(require, module, exports) {
                var e = Object.prototype.hasOwnProperty,
                    t = "~";

                function n() {}

                function r(e, t, n) { this.fn = e, this.context = t, this.once = n || !1; }

                function o(e, n, o, s, i) {
                    if ("function" != typeof o) throw new TypeError("The listener must be a function");
                    var c = new r(o, s || e, i),
                        f = t ? t + n : n;
                    return e._events[f] ? e._events[f].fn ? e._events[f] = [e._events[f], c] : e._events[f].push(c) : (e._events[f] = c, e._eventsCount++), e
                }

                function s(e, t) { 0 == --e._eventsCount ? e._events = new n : delete e._events[t]; }

                function i() { this._events = new n, this._eventsCount = 0; }
                Object.create && (n.prototype = Object.create(null), (new n).__proto__ || (t = !1)), i.prototype.eventNames = function() { var n, r, o = []; if (0 === this._eventsCount) return o; for (r in n = this._events) e.call(n, r) && o.push(t ? r.slice(1) : r); return Object.getOwnPropertySymbols ? o.concat(Object.getOwnPropertySymbols(n)) : o }, i.prototype.listeners = function(e) {
                    var n = t ? t + e : e,
                        r = this._events[n];
                    if (!r) return [];
                    if (r.fn) return [r.fn];
                    for (var o = 0, s = r.length, i = new Array(s); o < s; o++) i[o] = r[o].fn;
                    return i
                }, i.prototype.listenerCount = function(e) {
                    var n = t ? t + e : e,
                        r = this._events[n];
                    return r ? r.fn ? 1 : r.length : 0
                }, i.prototype.emit = function(e, n, r, o, s, i) {
                    var c = t ? t + e : e;
                    if (!this._events[c]) return !1;
                    var f, u, a = this._events[c],
                        l = arguments.length;
                    if (a.fn) {
                        switch (a.once && this.removeListener(e, a.fn, void 0, !0), l) {
                            case 1:
                                return a.fn.call(a.context), !0;
                            case 2:
                                return a.fn.call(a.context, n), !0;
                            case 3:
                                return a.fn.call(a.context, n, r), !0;
                            case 4:
                                return a.fn.call(a.context, n, r, o), !0;
                            case 5:
                                return a.fn.call(a.context, n, r, o, s), !0;
                            case 6:
                                return a.fn.call(a.context, n, r, o, s, i), !0
                        }
                        for (u = 1, f = new Array(l - 1); u < l; u++) f[u - 1] = arguments[u];
                        a.fn.apply(a.context, f);
                    } else {
                        var v, h = a.length;
                        for (u = 0; u < h; u++) switch (a[u].once && this.removeListener(e, a[u].fn, void 0, !0), l) {
                            case 1:
                                a[u].fn.call(a[u].context);
                                break;
                            case 2:
                                a[u].fn.call(a[u].context, n);
                                break;
                            case 3:
                                a[u].fn.call(a[u].context, n, r);
                                break;
                            case 4:
                                a[u].fn.call(a[u].context, n, r, o);
                                break;
                            default:
                                if (!f)
                                    for (v = 1, f = new Array(l - 1); v < l; v++) f[v - 1] = arguments[v];
                                a[u].fn.apply(a[u].context, f);
                        }
                    }
                    return !0
                }, i.prototype.on = function(e, t, n) { return o(this, e, t, n, !1) }, i.prototype.once = function(e, t, n) { return o(this, e, t, n, !0) }, i.prototype.removeListener = function(e, n, r, o) {
                    var i = t ? t + e : e;
                    if (!this._events[i]) return this;
                    if (!n) return s(this, i), this;
                    var c = this._events[i];
                    if (c.fn) c.fn !== n || o && !c.once || r && c.context !== r || s(this, i);
                    else {
                        for (var f = 0, u = [], a = c.length; f < a; f++)(c[f].fn !== n || o && !c[f].once || r && c[f].context !== r) && u.push(c[f]);
                        u.length ? this._events[i] = 1 === u.length ? u[0] : u : s(this, i);
                    }
                    return this
                }, i.prototype.removeAllListeners = function(e) { var r; return e ? (r = t ? t + e : e, this._events[r] && s(this, r)) : (this._events = new n, this._eventsCount = 0), this }, i.prototype.off = i.prototype.removeListener, i.prototype.addListener = i.prototype.on, i.prefixed = t, i.EventEmitter = i, "undefined" != typeof module && (module.exports = i);
            }, {}],
            "8WOs": [function(require, module, exports) {
                var r = this && this.__read || function(r, e) {
                        var o = "function" == typeof Symbol && r[Symbol.iterator];
                        if (!o) return r;
                        var t, n, l = o.call(r),
                            i = [];
                        try {
                            for (;
                                (void 0 === e || e-- > 0) && !(t = l.next()).done;) i.push(t.value);
                        } catch (s) { n = { error: s }; } finally { try { t && !t.done && (o = l.return) && o.call(l); } finally { if (n) throw n.error } }
                        return i
                    },
                    e = this && this.__spread || function() { for (var e = [], o = 0; o < arguments.length; o++) e = e.concat(r(arguments[o])); return e };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var o, t = "PeerJS: ";
                ! function(r) { r[r.Disabled = 0] = "Disabled", r[r.Errors = 1] = "Errors", r[r.Warnings = 2] = "Warnings", r[r.All = 3] = "All"; }(o = exports.LogLevel || (exports.LogLevel = {}));
                var n = function() {
                    function r() { this._logLevel = o.Disabled; }
                    return Object.defineProperty(r.prototype, "logLevel", { get: function() { return this._logLevel }, set: function(r) { this._logLevel = r; }, enumerable: !0, configurable: !0 }), r.prototype.log = function() {
                        for (var r = [], t = 0; t < arguments.length; t++) r[t] = arguments[t];
                        this._logLevel >= o.All && this._print.apply(this, e([o.All], r));
                    }, r.prototype.warn = function() {
                        for (var r = [], t = 0; t < arguments.length; t++) r[t] = arguments[t];
                        this._logLevel >= o.Warnings && this._print.apply(this, e([o.Warnings], r));
                    }, r.prototype.error = function() {
                        for (var r = [], t = 0; t < arguments.length; t++) r[t] = arguments[t];
                        this._logLevel >= o.Errors && this._print.apply(this, e([o.Errors], r));
                    }, r.prototype.setLogFunction = function(r) { this._print = r; }, r.prototype._print = function(r) {
                        for (var n = [], l = 1; l < arguments.length; l++) n[l - 1] = arguments[l];
                        var i = e([t], n);
                        for (var s in i) i[s] instanceof Error && (i[s] = "(" + i[s].name + ") " + i[s].message);
                        r >= o.All ? console.log.apply(console, e(i)) : r >= o.Warnings ? console.warn.apply(console, e(["WARNING"], i)) : r >= o.Errors && console.error.apply(console, e(["ERROR"], i));
                    }, r
                }();
                exports.default = new n;
            }, {}],
            "9ZRY": [function(require, module, exports) {
                Object.defineProperty(exports, "__esModule", { value: !0 }),
                    function(e) { e.Open = "open", e.Stream = "stream", e.Data = "data", e.Close = "close", e.Error = "error", e.IceStateChanged = "iceStateChanged"; }(exports.ConnectionEventType || (exports.ConnectionEventType = {})),
                    function(e) { e.Data = "data", e.Media = "media"; }(exports.ConnectionType || (exports.ConnectionType = {})),
                    function(e) { e.Open = "open", e.Close = "close", e.Connection = "connection", e.Call = "call", e.Disconnected = "disconnected", e.Error = "error"; }(exports.PeerEventType || (exports.PeerEventType = {})),
                    function(e) { e.BrowserIncompatible = "browser-incompatible", e.Disconnected = "disconnected", e.InvalidID = "invalid-id", e.InvalidKey = "invalid-key", e.Network = "network", e.PeerUnavailable = "peer-unavailable", e.SslUnavailable = "ssl-unavailable", e.ServerError = "server-error", e.SocketError = "socket-error", e.SocketClosed = "socket-closed", e.UnavailableID = "unavailable-id", e.WebRTC = "webrtc"; }(exports.PeerErrorType || (exports.PeerErrorType = {})),
                    function(e) { e.Binary = "binary", e.BinaryUTF8 = "binary-utf8", e.JSON = "json"; }(exports.SerializationType || (exports.SerializationType = {})),
                    function(e) { e.Message = "message", e.Disconnected = "disconnected", e.Error = "error", e.Close = "close"; }(exports.SocketEventType || (exports.SocketEventType = {})),
                    function(e) { e.Heartbeat = "HEARTBEAT", e.Candidate = "CANDIDATE", e.Offer = "OFFER", e.Answer = "ANSWER", e.Open = "OPEN", e.Error = "ERROR", e.IdTaken = "ID-TAKEN", e.InvalidKey = "INVALID-KEY", e.Leave = "LEAVE", e.Expire = "EXPIRE"; }(exports.ServerMessageType || (exports.ServerMessageType = {}));
            }, {}],
            "wJlv": [function(require, module, exports) {
                var e = this && this.__extends || function() {
                        var e = function(t, s) {
                            return (e = Object.setPrototypeOf || { __proto__: [] }
                                instanceof Array && function(e, t) { e.__proto__ = t; } || function(e, t) { for (var s in t) t.hasOwnProperty(s) && (e[s] = t[s]); })(t, s)
                        };
                        return function(t, s) {
                            function n() { this.constructor = t; }
                            e(t, s), t.prototype = null === s ? Object.create(s) : (n.prototype = s.prototype, new n);
                        }
                    }(),
                    t = this && this.__read || function(e, t) {
                        var s = "function" == typeof Symbol && e[Symbol.iterator];
                        if (!s) return e;
                        var n, o, r = s.call(e),
                            i = [];
                        try {
                            for (;
                                (void 0 === t || t-- > 0) && !(n = r.next()).done;) i.push(n.value);
                        } catch (c) { o = { error: c }; } finally { try { n && !n.done && (s = r.return) && s.call(r); } finally { if (o) throw o.error } }
                        return i
                    },
                    s = this && this.__spread || function() { for (var e = [], s = 0; s < arguments.length; s++) e = e.concat(t(arguments[s])); return e },
                    n = this && this.__values || function(e) {
                        var t = "function" == typeof Symbol && e[Symbol.iterator],
                            s = 0;
                        return t ? t.call(e) : { next: function() { return e && s >= e.length && (e = void 0), { value: e && e[s++], done: !e } } }
                    },
                    o = this && this.__importDefault || function(e) { return e && e.__esModule ? e : { default: e } };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var r = require("eventemitter3"),
                    i = o(require("./logger")),
                    c = require("./enums"),
                    a = function(t) {
                        function o(e, s, n, o, r) {
                            var i = t.call(this) || this;
                            i.WEB_SOCKET_PING_INTERVAL = 2e4, i._disconnected = !1, i._messagesQueue = [];
                            var c = e ? "wss://" : "ws://";
                            return i._wsUrl = c + s + ":" + n + o + "peerjs?key=" + r, i
                        }
                        return e(o, t), o.prototype.start = function(e, t) { this._id = e, this._wsUrl += "&id=" + e + "&token=" + t, this._startWebSocket(); }, o.prototype._startWebSocket = function() {
                            var e = this;
                            this._socket || (this._socket = new WebSocket(this._wsUrl), this._socket.onmessage = function(t) {
                                var s;
                                try { s = JSON.parse(t.data); } catch (n) { return void i.default.log("Invalid server message", t.data) }
                                e.emit(c.SocketEventType.Message, s);
                            }, this._socket.onclose = function(t) { i.default.log("Socket closed.", t), e._disconnected = !0, clearTimeout(e._wsPingTimer), e.emit(c.SocketEventType.Disconnected); }, this._socket.onopen = function() { e._disconnected || (e._sendQueuedMessages(), i.default.log("Socket open"), e._scheduleHeartbeat()); });
                        }, o.prototype._scheduleHeartbeat = function() {
                            var e = this;
                            this._wsPingTimer = setTimeout(function() { e._sendHeartbeat(); }, this.WEB_SOCKET_PING_INTERVAL);
                        }, o.prototype._sendHeartbeat = function() {
                            if (this._wsOpen()) {
                                var e = JSON.stringify({ type: c.ServerMessageType.Heartbeat });
                                this._socket.send(e), this._scheduleHeartbeat();
                            } else i.default.log("Cannot send heartbeat, because socket closed");
                        }, o.prototype._wsOpen = function() { return !!this._socket && 1 == this._socket.readyState }, o.prototype._sendQueuedMessages = function() {
                            var e, t, o = s(this._messagesQueue);
                            this._messagesQueue = [];
                            try {
                                for (var r = n(o), i = r.next(); !i.done; i = r.next()) {
                                    var c = i.value;
                                    this.send(c);
                                }
                            } catch (a) { e = { error: a }; } finally { try { i && !i.done && (t = r.return) && t.call(r); } finally { if (e) throw e.error } }
                        }, o.prototype.send = function(e) {
                            if (!this._disconnected)
                                if (this._id)
                                    if (e.type) {
                                        if (this._wsOpen()) {
                                            var t = JSON.stringify(e);
                                            this._socket.send(t);
                                        }
                                    } else this.emit(c.SocketEventType.Error, "Invalid message");
                            else this._messagesQueue.push(e);
                        }, o.prototype.close = function() {!this._disconnected && this._socket && (this._socket.close(), this._disconnected = !0, clearTimeout(this._wsPingTimer)); }, o
                    }(r.EventEmitter);
                exports.Socket = a;
            }, { "eventemitter3": "2JJl", "./logger": "8WOs", "./enums": "9ZRY" }],
            "T9kO": [function(require, module, exports) {
                var r = require("js-binarypack"),
                    t = {
                        debug: !1,
                        inherits: function(e, r) { e.super_ = r, e.prototype = Object.create(r.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }); },
                        extend: function(e, r) { for (var t in r) r.hasOwnProperty(t) && (e[t] = r[t]); return e },
                        pack: r.pack,
                        unpack: r.unpack,
                        log: function() {
                            if (t.debug) {
                                for (var e = [], r = 0; r < arguments.length; r++) e[r] = arguments[r];
                                e.unshift("Reliable: "), console.log.apply(console, e);
                            }
                        },
                        setZeroTimeout: function(e) {
                            var r = [],
                                t = "zero-timeout-message";

                            function n(n) { n.source == e && n.data == t && (n.stopPropagation && n.stopPropagation(), r.length && r.shift()()); }
                            return e.addEventListener ? e.addEventListener("message", n, !0) : e.attachEvent && e.attachEvent("onmessage", n),
                                function(n) { r.push(n), e.postMessage(t, "*"); }
                        }(this),
                        blobToArrayBuffer: function(e, r) {
                            var t = new FileReader;
                            t.onload = function(e) { r(e.target.result); }, t.readAsArrayBuffer(e);
                        },
                        blobToBinaryString: function(e, r) {
                            var t = new FileReader;
                            t.onload = function(e) { r(e.target.result); }, t.readAsBinaryString(e);
                        },
                        binaryStringToArrayBuffer: function(e) { for (var r = new Uint8Array(e.length), t = 0; t < e.length; t++) r[t] = 255 & e.charCodeAt(t); return r.buffer },
                        randomToken: function() { return Math.random().toString(36).substr(2) }
                    };
                module.exports = t;
            }, { "js-binarypack": "lHOc" }],
            "aYFJ": [function(require, module, exports) {
                var t = require("./util");

                function e(n, i) {
                    if (!(this instanceof e)) return new e(n);
                    this._dc = n, t.debug = i, this._outgoing = {}, this._incoming = {}, this._received = {}, this._window = 1e3, this._mtu = 500, this._interval = 0, this._count = 0, this._queue = [], this._setupDC();
                }
                e.prototype.send = function(e) {
                    var n = t.pack(e);
                    n.size < this._mtu ? this._handleSend(["no", n]) : (this._outgoing[this._count] = { ack: 0, chunks: this._chunk(n) }, t.debug && (this._outgoing[this._count].timer = new Date), this._sendWindowedChunks(this._count), this._count += 1);
                }, e.prototype._setupInterval = function() {
                    var t = this;
                    this._timeout = setInterval(function() {
                        var e = t._queue.shift();
                        if (e._multiple)
                            for (var n = 0, i = e.length; n < i; n += 1) t._intervalSend(e[n]);
                        else t._intervalSend(e);
                    }, this._interval);
                }, e.prototype._intervalSend = function(e) {
                    var n = this;
                    e = t.pack(e), t.blobToBinaryString(e, function(t) { n._dc.send(t); }), 0 === n._queue.length && (clearTimeout(n._timeout), n._timeout = null);
                }, e.prototype._processAcks = function() { for (var t in this._outgoing) this._outgoing.hasOwnProperty(t) && this._sendWindowedChunks(t); }, e.prototype._handleSend = function(t) {
                    for (var e = !0, n = 0, i = this._queue.length; n < i; n += 1) {
                        var o = this._queue[n];
                        o === t ? e = !1 : o._multiple && -1 !== o.indexOf(t) && (e = !1);
                    }
                    e && (this._queue.push(t), this._timeout || this._setupInterval());
                }, e.prototype._setupDC = function() {
                    var e = this;
                    this._dc.onmessage = function(n) {
                        var i = n.data;
                        if (i.constructor === String) {
                            var o = t.binaryStringToArrayBuffer(i);
                            i = t.unpack(o), e._handleMessage(i);
                        }
                    };
                }, e.prototype._handleMessage = function(e) {
                    var n, i = e[1],
                        o = this._incoming[i],
                        s = this._outgoing[i];
                    switch (e[0]) {
                        case "no":
                            var a = i;
                            a && this.onmessage(t.unpack(a));
                            break;
                        case "end":
                            if (n = o, this._received[i] = e[2], !n) break;
                            this._ack(i);
                            break;
                        case "ack":
                            if (n = s) {
                                var h = e[2];
                                n.ack = Math.max(h, n.ack), n.ack >= n.chunks.length ? (t.log("Time: ", new Date - n.timer), delete this._outgoing[i]) : this._processAcks();
                            }
                            break;
                        case "chunk":
                            if (!(n = o)) {
                                if (!0 === this._received[i]) break;
                                n = { ack: ["ack", i, 0], chunks: [] }, this._incoming[i] = n;
                            }
                            var r = e[2],
                                u = e[3];
                            n.chunks[r] = new Uint8Array(u), r === n.ack[2] && this._calculateNextAck(i), this._ack(i);
                            break;
                        default:
                            this._handleSend(e);
                    }
                }, e.prototype._chunk = function(e) {
                    for (var n = [], i = e.size, o = 0; o < i;) {
                        var s = Math.min(i, o + this._mtu),
                            a = { payload: e.slice(o, s) };
                        n.push(a), o = s;
                    }
                    return t.log("Created", n.length, "chunks."), n
                }, e.prototype._ack = function(t) {
                    var e = this._incoming[t].ack;
                    this._received[t] === e[2] && (this._complete(t), this._received[t] = !0), this._handleSend(e);
                }, e.prototype._calculateNextAck = function(t) {
                    for (var e = this._incoming[t], n = e.chunks, i = 0, o = n.length; i < o; i += 1)
                        if (void 0 === n[i]) return void(e.ack[2] = i);
                    e.ack[2] = n.length;
                }, e.prototype._sendWindowedChunks = function(e) {
                    t.log("sendWindowedChunks for: ", e);
                    for (var n = this._outgoing[e], i = n.chunks, o = [], s = Math.min(n.ack + this._window, i.length), a = n.ack; a < s; a += 1) i[a].sent && a !== n.ack || (i[a].sent = !0, o.push(["chunk", e, a, i[a].payload]));
                    n.ack + this._window >= i.length && o.push(["end", e, i.length]), o._multiple = !0, this._handleSend(o);
                }, e.prototype._complete = function(e) {
                    t.log("Completed called for", e);
                    var n = this,
                        i = this._incoming[e].chunks,
                        o = new Blob(i);
                    t.blobToArrayBuffer(o, function(e) { n.onmessage(t.unpack(e)); }), delete this._incoming[e];
                }, e.higherBandwidthSDP = function(t) { var e = navigator.appVersion.match(/Chrome\/(.*?) /); if (e && (e = parseInt(e[1].split(".").shift())) < 31) { var n = t.split("b=AS:30"); if (n.length > 1) return n[0] + "b=AS:102400" + n[1] } return t }, e.prototype.onmessage = function(t) {}, module.exports = e;
            }, { "./util": "T9kO" }],
            "HCdX": [function(require, module, exports) {
                var e = this && this.__assign || function() {
                        return (e = Object.assign || function(e) {
                            for (var n, t = 1, o = arguments.length; t < o; t++)
                                for (var i in n = arguments[t]) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i]);
                            return e
                        }).apply(this, arguments)
                    },
                    n = this && this.__awaiter || function(e, n, t, o) {
                        return new(t || (t = Promise))(function(i, r) {
                            function c(e) { try { s(o.next(e)); } catch (n) { r(n); } }

                            function a(e) { try { s(o.throw(e)); } catch (n) { r(n); } }

                            function s(e) { e.done ? i(e.value) : new t(function(n) { n(e.value); }).then(c, a); }
                            s((o = o.apply(e, n || [])).next());
                        })
                    },
                    t = this && this.__generator || function(e, n) {
                        var t, o, i, r, c = { label: 0, sent: function() { if (1 & i[0]) throw i[1]; return i[1] }, trys: [], ops: [] };
                        return r = { next: a(0), throw: a(1), return: a(2) }, "function" == typeof Symbol && (r[Symbol.iterator] = function() { return this }), r;

                        function a(r) {
                            return function(a) {
                                return function(r) {
                                    if (t) throw new TypeError("Generator is already executing.");
                                    for (; c;) try {
                                        if (t = 1, o && (i = 2 & r[0] ? o.return : r[0] ? o.throw || ((i = o.return) && i.call(o), 0) : o.next) && !(i = i.call(o, r[1])).done) return i;
                                        switch (o = 0, i && (r = [2 & r[0], i.value]), r[0]) {
                                            case 0:
                                            case 1:
                                                i = r;
                                                break;
                                            case 4:
                                                return c.label++, { value: r[1], done: !1 };
                                            case 5:
                                                c.label++, o = r[1], r = [0];
                                                continue;
                                            case 7:
                                                r = c.ops.pop(), c.trys.pop();
                                                continue;
                                            default:
                                                if (!(i = (i = c.trys).length > 0 && i[i.length - 1]) && (6 === r[0] || 2 === r[0])) { c = 0; continue }
                                                if (3 === r[0] && (!i || r[1] > i[0] && r[1] < i[3])) { c.label = r[1]; break }
                                                if (6 === r[0] && c.label < i[1]) { c.label = i[1], i = r; break }
                                                if (i && c.label < i[2]) { c.label = i[2], c.ops.push(r); break }
                                                i[2] && c.ops.pop(), c.trys.pop();
                                                continue
                                        }
                                        r = n.call(e, c);
                                    } catch (a) { r = [6, a], o = 0; } finally { t = i = 0; }
                                    if (5 & r[0]) throw r[1];
                                    return { value: r[0] ? r[1] : void 0, done: !0 }
                                }([r, a])
                            }
                        }
                    },
                    o = this && this.__importStar || function(e) {
                        if (e && e.__esModule) return e;
                        var n = {};
                        if (null != e)
                            for (var t in e) Object.hasOwnProperty.call(e, t) && (n[t] = e[t]);
                        return n.default = e, n
                    },
                    i = this && this.__importDefault || function(e) { return e && e.__esModule ? e : { default: e } };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var r = o(require("reliable")),
                    c = require("./util"),
                    a = i(require("./logger")),
                    s = require("./adapter"),
                    l = require("./enums"),
                    d = function() {
                        function o(e) { this.connection = e; }
                        return o.prototype.startConnection = function(e) {
                            var n = this._startPeerConnection();
                            if (this.connection.peerConnection = n, this.connection.type === l.ConnectionType.Media && e._stream && this._addTracksToConnection(e._stream, n), e.originator) {
                                if (this.connection.type === l.ConnectionType.Data) {
                                    var t = this.connection,
                                        o = {};
                                    c.util.supports.sctp || (o = { reliable: e.reliable });
                                    var i = n.createDataChannel(t.label, o);
                                    t.initialize(i);
                                }
                                this._makeOffer();
                            } else this.handleSDP("OFFER", e.sdp);
                        }, o.prototype._startPeerConnection = function() {
                            a.default.log("Creating RTCPeerConnection.");
                            var e = {};
                            this.connection.type !== l.ConnectionType.Data || c.util.supports.sctp ? this.connection.type === l.ConnectionType.Media && (e = { optional: [{ DtlsSrtpKeyAgreement: !0 }] }) : e = { optional: [{ RtpDataChannels: !0 }] };
                            var n = new s.RTCPeerConnection(this.connection.provider.options.config, e);
                            return this._setupListeners(n), n
                        }, o.prototype._setupListeners = function(e) {
                            var n = this,
                                t = this.connection.peer,
                                o = this.connection.connectionId,
                                i = this.connection.type,
                                r = this.connection.provider;
                            a.default.log("Listening for ICE candidates."), e.onicecandidate = function(e) { e.candidate && (a.default.log("Received ICE candidates for:", t), r.socket.send({ type: l.ServerMessageType.Candidate, payload: { candidate: e.candidate, type: i, connectionId: o }, dst: t })); }, e.oniceconnectionstatechange = function() {
                                switch (e.iceConnectionState) {
                                    case "failed":
                                        a.default.log("iceConnectionState is failed, closing connections to " + t), n.connection.emit(l.ConnectionEventType.Error, new Error("Negotiation of connection to " + t + " failed.")), n.connection.close();
                                        break;
                                    case "closed":
                                        a.default.log("iceConnectionState is closed, closing connections to " + t), n.connection.emit(l.ConnectionEventType.Error, new Error("Negotiation of connection to " + t + " failed.")), n.connection.close();
                                        break;
                                    case "disconnected":
                                        a.default.log("iceConnectionState is disconnected, closing connections to " + t);
                                        break;
                                    case "completed":
                                        e.onicecandidate = c.util.noop;
                                }
                                n.connection.emit(l.ConnectionEventType.IceStateChanged, e.iceConnectionState);
                            }, a.default.log("Listening for data channel"), e.ondatachannel = function(e) {
                                a.default.log("Received data channel");
                                var n = e.channel;
                                r.getConnection(t, o).initialize(n);
                            }, a.default.log("Listening for remote stream"), e.ontrack = function(e) {
                                a.default.log("Received remote stream");
                                var i = e.streams[0],
                                    c = r.getConnection(t, o);
                                if (c.type === l.ConnectionType.Media) {
                                    var s = c;
                                    n._addStreamToMediaConnection(i, s);
                                }
                            };
                        }, o.prototype.cleanup = function() {
                            a.default.log("Cleaning up PeerConnection to " + this.connection.peer);
                            var e = this.connection.peerConnection;
                            if (e) {
                                this.connection.peerConnection = null, e.onicecandidate = e.oniceconnectionstatechange = e.ondatachannel = e.ontrack = function() {};
                                var n = "closed" !== e.signalingState,
                                    t = !1;
                                if (this.connection.type === l.ConnectionType.Data) {
                                    var o = this.connection.dataChannel;
                                    t = o.readyState && "closed" !== o.readyState;
                                }(n || t) && e.close();
                            }
                        }, o.prototype._makeOffer = function() {
                            return n(this, void 0, Promise, function() {
                                var n, o, i, s, d, p, u;
                                return t(this, function(t) {
                                    switch (t.label) {
                                        case 0:
                                            n = this.connection.peerConnection, o = this.connection.provider, t.label = 1;
                                        case 1:
                                            return t.trys.push([1, 7, , 8]), [4, n.createOffer(this.connection.options.constraints)];
                                        case 2:
                                            i = t.sent(), a.default.log("Created offer."), c.util.supports.sctp || this.connection.type !== l.ConnectionType.Data || (d = this.connection).reliable && (i.sdp = r.higherBandwidthSDP(i.sdp)), this.connection.options.sdpTransform && "function" == typeof this.connection.options.sdpTransform && (i.sdp = this.connection.options.sdpTransform(i.sdp) || i.sdp), t.label = 3;
                                        case 3:
                                            return t.trys.push([3, 5, , 6]), [4, n.setLocalDescription(i)];
                                        case 4:
                                            return t.sent(), a.default.log("Set localDescription:", i, "for:" + this.connection.peer), s = { sdp: i, type: this.connection.type, connectionId: this.connection.connectionId, metadata: this.connection.metadata, browser: c.util.browser }, this.connection.type === l.ConnectionType.Data && (d = this.connection, s = e({}, s, { label: d.label, reliable: d.reliable, serialization: d.serialization })), o.socket.send({ type: l.ServerMessageType.Offer, payload: s, dst: this.connection.peer }), [3, 6];
                                        case 5:
                                            return "OperationError: Failed to set local offer sdp: Called in wrong state: kHaveRemoteOffer" != (p = t.sent()) && (o.emitError(l.PeerErrorType.WebRTC, p), a.default.log("Failed to setLocalDescription, ", p)), [3, 6];
                                        case 6:
                                            return [3, 8];
                                        case 7:
                                            return u = t.sent(), o.emitError(l.PeerErrorType.WebRTC, u), a.default.log("Failed to createOffer, ", u), [3, 8];
                                        case 8:
                                            return [2]
                                    }
                                })
                            })
                        }, o.prototype._makeAnswer = function() {
                            return n(this, void 0, Promise, function() {
                                var e, n, o, i, s;
                                return t(this, function(t) {
                                    switch (t.label) {
                                        case 0:
                                            e = this.connection.peerConnection, n = this.connection.provider, t.label = 1;
                                        case 1:
                                            return t.trys.push([1, 7, , 8]), [4, e.createAnswer()];
                                        case 2:
                                            o = t.sent(), a.default.log("Created answer."), c.util.supports.sctp || this.connection.type !== l.ConnectionType.Data || this.connection.reliable && (o.sdp = r.higherBandwidthSDP(o.sdp)), t.label = 3;
                                        case 3:
                                            return t.trys.push([3, 5, , 6]), [4, e.setLocalDescription(o)];
                                        case 4:
                                            return t.sent(), a.default.log("Set localDescription:", o, "for:" + this.connection.peer), n.socket.send({ type: l.ServerMessageType.Answer, payload: { sdp: o, type: this.connection.type, connectionId: this.connection.connectionId, browser: c.util.browser }, dst: this.connection.peer }), [3, 6];
                                        case 5:
                                            return i = t.sent(), n.emitError(l.PeerErrorType.WebRTC, i), a.default.log("Failed to setLocalDescription, ", i), [3, 6];
                                        case 6:
                                            return [3, 8];
                                        case 7:
                                            return s = t.sent(), n.emitError(l.PeerErrorType.WebRTC, s), a.default.log("Failed to create answer, ", s), [3, 8];
                                        case 8:
                                            return [2]
                                    }
                                })
                            })
                        }, o.prototype.handleSDP = function(e, o) {
                            return n(this, void 0, Promise, function() {
                                var n, i, r, c;
                                return t(this, function(t) {
                                    switch (t.label) {
                                        case 0:
                                            o = new s.RTCSessionDescription(o), n = this.connection.peerConnection, i = this.connection.provider, a.default.log("Setting remote description", o), r = this, t.label = 1;
                                        case 1:
                                            return t.trys.push([1, 5, , 6]), [4, n.setRemoteDescription(o)];
                                        case 2:
                                            return t.sent(), a.default.log("Set remoteDescription:" + e + " for:" + this.connection.peer), "OFFER" !== e ? [3, 4] : [4, r._makeAnswer()];
                                        case 3:
                                            t.sent(), t.label = 4;
                                        case 4:
                                            return [3, 6];
                                        case 5:
                                            return c = t.sent(), i.emitError(l.PeerErrorType.WebRTC, c), a.default.log("Failed to setRemoteDescription, ", c), [3, 6];
                                        case 6:
                                            return [2]
                                    }
                                })
                            })
                        }, o.prototype.handleCandidate = function(e) {
                            return n(this, void 0, Promise, function() {
                                var n, o, i, r, c;
                                return t(this, function(t) {
                                    switch (t.label) {
                                        case 0:
                                            n = e.candidate, o = e.sdpMLineIndex, i = this.connection.peerConnection, r = this.connection.provider, t.label = 1;
                                        case 1:
                                            return t.trys.push([1, 3, , 4]), [4, i.addIceCandidate(new s.RTCIceCandidate({ sdpMLineIndex: o, candidate: n }))];
                                        case 2:
                                            return t.sent(), a.default.log("Added ICE candidate for:" + this.connection.peer), [3, 4];
                                        case 3:
                                            return c = t.sent(), r.emitError(l.PeerErrorType.WebRTC, c), a.default.log("Failed to handleCandidate, ", c), [3, 4];
                                        case 4:
                                            return [2]
                                    }
                                })
                            })
                        }, o.prototype._addTracksToConnection = function(e, n) {
                            if (a.default.log("add tracks from stream " + e.id + " to peer connection"), !n.addTrack) return a.default.error("Your browser does't support RTCPeerConnection#addTrack. Ignored.");
                            e.getTracks().forEach(function(t) { n.addTrack(t, e); });
                        }, o.prototype._addStreamToMediaConnection = function(e, n) { a.default.log("add stream " + e.id + " to media connection " + n.connectionId), n.addStream(e); }, o
                    }();
                exports.Negotiator = d;
            }, { "reliable": "aYFJ", "./util": "BHXf", "./logger": "8WOs", "./adapter": "sXtV", "./enums": "9ZRY" }],
            "tQFK": [function(require, module, exports) {
                var t = this && this.__extends || function() {
                    var t = function(e, r) {
                        return (t = Object.setPrototypeOf || { __proto__: [] }
                            instanceof Array && function(t, e) { t.__proto__ = e; } || function(t, e) { for (var r in e) e.hasOwnProperty(r) && (t[r] = e[r]); })(e, r)
                    };
                    return function(e, r) {
                        function n() { this.constructor = e; }
                        t(e, r), e.prototype = null === r ? Object.create(r) : (n.prototype = r.prototype, new n);
                    }
                }();
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var e = require("eventemitter3"),
                    r = function(e) {
                        function r(t, r, n) { var o = e.call(this) || this; return o.peer = t, o.provider = r, o.options = n, o._open = !1, o.metadata = n.metadata, o }
                        return t(r, e), Object.defineProperty(r.prototype, "open", { get: function() { return this._open }, enumerable: !0, configurable: !0 }), r
                    }(e.EventEmitter);
                exports.BaseConnection = r;
            }, { "eventemitter3": "2JJl" }],
            "dbHP": [function(require, module, exports) {
                var e = this && this.__extends || function() {
                        var e = function(t, o) {
                            return (e = Object.setPrototypeOf || { __proto__: [] }
                                instanceof Array && function(e, t) { e.__proto__ = t; } || function(e, t) { for (var o in t) t.hasOwnProperty(o) && (e[o] = t[o]); })(t, o)
                        };
                        return function(t, o) {
                            function n() { this.constructor = t; }
                            e(t, o), t.prototype = null === o ? Object.create(o) : (n.prototype = o.prototype, new n);
                        }
                    }(),
                    t = this && this.__assign || function() {
                        return (t = Object.assign || function(e) {
                            for (var t, o = 1, n = arguments.length; o < n; o++)
                                for (var r in t = arguments[o]) Object.prototype.hasOwnProperty.call(t, r) && (e[r] = t[r]);
                            return e
                        }).apply(this, arguments)
                    },
                    o = this && this.__values || function(e) {
                        var t = "function" == typeof Symbol && e[Symbol.iterator],
                            o = 0;
                        return t ? t.call(e) : { next: function() { return e && o >= e.length && (e = void 0), { value: e && e[o++], done: !e } } }
                    },
                    n = this && this.__importDefault || function(e) { return e && e.__esModule ? e : { default: e } };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var r = require("./util"),
                    i = n(require("./logger")),
                    a = require("./negotiator"),
                    s = require("./enums"),
                    l = require("./baseconnection"),
                    c = function(n) {
                        function l(e, t, o) { var i = n.call(this, e, t, o) || this; return i._localStream = i.options._stream, i.connectionId = i.options.connectionId || l.ID_PREFIX + r.util.randomToken(), i._negotiator = new a.Negotiator(i), i._localStream && i._negotiator.startConnection({ _stream: i._localStream, originator: !0 }), i }
                        return e(l, n), Object.defineProperty(l.prototype, "type", { get: function() { return s.ConnectionType.Media }, enumerable: !0, configurable: !0 }), Object.defineProperty(l.prototype, "localStream", { get: function() { return this._localStream }, enumerable: !0, configurable: !0 }), Object.defineProperty(l.prototype, "remoteStream", { get: function() { return this._remoteStream }, enumerable: !0, configurable: !0 }), l.prototype.addStream = function(e) { i.default.log("Receiving stream", e), this._remoteStream = e, n.prototype.emit.call(this, s.ConnectionEventType.Stream, e); }, l.prototype.handleMessage = function(e) {
                            var t = e.type,
                                o = e.payload;
                            switch (e.type) {
                                case s.ServerMessageType.Answer:
                                    this._negotiator.handleSDP(t, o.sdp), this._open = !0;
                                    break;
                                case s.ServerMessageType.Candidate:
                                    this._negotiator.handleCandidate(o.candidate);
                                    break;
                                default:
                                    i.default.warn("Unrecognized message type:" + t + " from peer:" + this.peer);
                            }
                        }, l.prototype.answer = function(e) {
                            var n, r;
                            if (this._localStream) i.default.warn("Local stream already exists on this MediaConnection. Are you answering a call twice?");
                            else {
                                this._localStream = e, this._negotiator.startConnection(t({}, this.options._payload, { _stream: e }));
                                var a = this.provider._getMessages(this.connectionId);
                                try {
                                    for (var s = o(a), l = s.next(); !l.done; l = s.next()) {
                                        var c = l.value;
                                        this.handleMessage(c);
                                    }
                                } catch (p) { n = { error: p }; } finally { try { l && !l.done && (r = s.return) && r.call(s); } finally { if (n) throw n.error } }
                                this._open = !0;
                            }
                        }, l.prototype.close = function() { this._negotiator && (this._negotiator.cleanup(), this._negotiator = null), this._localStream = null, this._remoteStream = null, this.provider && (this.provider._removeConnection(this), this.provider = null), this.options && this.options._stream && (this.options._stream = null), this.open && (this._open = !1, n.prototype.emit.call(this, s.ConnectionEventType.Close)); }, l.ID_PREFIX = "mc_", l
                    }(l.BaseConnection);
                exports.MediaConnection = c;
            }, { "./util": "BHXf", "./logger": "8WOs", "./negotiator": "HCdX", "./enums": "9ZRY", "./baseconnection": "tQFK" }],
            "GBTQ": [function(require, module, exports) {
                var e = this && this.__extends || function() {
                        var e = function(t, n) {
                            return (e = Object.setPrototypeOf || { __proto__: [] }
                                instanceof Array && function(e, t) { e.__proto__ = t; } || function(e, t) { for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]); })(t, n)
                        };
                        return function(t, n) {
                            function i() { this.constructor = t; }
                            e(t, n), t.prototype = null === n ? Object.create(n) : (i.prototype = n.prototype, new i);
                        }
                    }(),
                    t = this && this.__values || function(e) {
                        var t = "function" == typeof Symbol && e[Symbol.iterator],
                            n = 0;
                        return t ? t.call(e) : { next: function() { return e && n >= e.length && (e = void 0), { value: e && e[n++], done: !e } } }
                    },
                    n = this && this.__importStar || function(e) {
                        if (e && e.__esModule) return e;
                        var t = {};
                        if (null != e)
                            for (var n in e) Object.hasOwnProperty.call(e, n) && (t[n] = e[n]);
                        return t.default = e, t
                    };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var i = require("reliable"),
                    r = require("./util"),
                    o = n(require("./logger")),
                    a = require("./negotiator"),
                    s = require("./enums"),
                    u = require("./baseconnection"),
                    l = function(n) {
                        function u(e, t, i) { var o = n.call(this, e, t, i) || this; return o._buffer = [], o._bufferSize = 0, o._buffering = !1, o._chunkedData = {}, o.connectionId = o.options.connectionId || u.ID_PREFIX + r.util.randomToken(), o.label = o.options.label || o.connectionId, o.serialization = o.options.serialization || s.SerializationType.Binary, o.reliable = o.options.reliable, o.options._payload && (o._peerBrowser = o.options._payload.browser), o._negotiator = new a.Negotiator(o), o._negotiator.startConnection(o.options._payload || { originator: !0 }), o }
                        return e(u, n), Object.defineProperty(u.prototype, "type", { get: function() { return s.ConnectionType.Data }, enumerable: !0, configurable: !0 }), Object.defineProperty(u.prototype, "dataChannel", { get: function() { return this._dc }, enumerable: !0, configurable: !0 }), Object.defineProperty(u.prototype, "bufferSize", { get: function() { return this._bufferSize }, enumerable: !0, configurable: !0 }), u.prototype.initialize = function(e) { this._dc = e, this._configureDataChannel(); }, u.prototype._configureDataChannel = function() {
                            var e = this;
                            if (r.util.supports.sctp && (this.dataChannel.binaryType = "arraybuffer"), this.dataChannel.onopen = function() { o.default.log("Data channel connection success"), e._open = !0, e.emit(s.ConnectionEventType.Open); }, !r.util.supports.sctp && this.reliable) {
                                var t = o.default.logLevel > o.LogLevel.Disabled;
                                this._reliable = new i.Reliable(this.dataChannel, t);
                            }
                            this._reliable ? this._reliable.onmessage = function(t) { e.emit(s.ConnectionEventType.Data, t); } : this.dataChannel.onmessage = function(t) { e._handleDataMessage(t); }, this.dataChannel.onclose = function() { o.default.log("DataChannel closed for:", e.peer), e.close(); };
                        }, u.prototype._handleDataMessage = function(e) {
                            var t = this,
                                i = e.data,
                                o = i.constructor;
                            if (this.serialization === s.SerializationType.Binary || this.serialization === s.SerializationType.BinaryUTF8) {
                                if (o === Blob) return void r.util.blobToArrayBuffer(i, function(e) { i = r.util.unpack(e), t.emit(s.ConnectionEventType.Data, i); });
                                if (o === ArrayBuffer) i = r.util.unpack(i);
                                else if (o === String) {
                                    var a = r.util.binaryStringToArrayBuffer(i);
                                    i = r.util.unpack(a);
                                }
                            } else this.serialization === s.SerializationType.JSON && (i = JSON.parse(i));
                            if (i.__peerData) {
                                var u = i.__peerData,
                                    l = this._chunkedData[u] || { data: [], count: 0, total: i.total };
                                return l.data[i.n] = i.data, l.count++, l.total === l.count && (delete this._chunkedData[u], i = new Blob(l.data), this._handleDataMessage({ data: i })), void(this._chunkedData[u] = l)
                            }
                            n.prototype.emit.call(this, s.ConnectionEventType.Data, i);
                        }, u.prototype.close = function() { this._buffer = [], this._bufferSize = 0, this._negotiator && (this._negotiator.cleanup(), this._negotiator = null), this.provider && (this.provider._removeConnection(this), this.provider = null), this.open && (this._open = !1, n.prototype.emit.call(this, s.ConnectionEventType.Close)); }, u.prototype.send = function(e, t) {
                            var i = this;
                            if (this.open)
                                if (this._reliable) this._reliable.send(e);
                                else if (this.serialization === s.SerializationType.JSON) this._bufferedSend(JSON.stringify(e));
                            else if (this.serialization === s.SerializationType.Binary || this.serialization === s.SerializationType.BinaryUTF8) {
                                var o = r.util.pack(e);
                                if ((r.util.chunkedBrowsers[this._peerBrowser] || r.util.chunkedBrowsers[r.util.browser]) && !t && o.size > r.util.chunkedMTU) return void this._sendChunks(o);
                                r.util.supports.sctp ? r.util.supports.binaryBlob ? this._bufferedSend(o) : r.util.blobToArrayBuffer(o, function(e) { i._bufferedSend(e); }) : r.util.blobToBinaryString(o, function(e) { i._bufferedSend(e); });
                            } else this._bufferedSend(e);
                            else n.prototype.emit.call(this, s.ConnectionEventType.Error, new Error("Connection is not open. You should listen for the `open` event before sending messages."));
                        }, u.prototype._bufferedSend = function(e) {!this._buffering && this._trySend(e) || (this._buffer.push(e), this._bufferSize = this._buffer.length); }, u.prototype._trySend = function(e) { var t = this; if (!this.open) return !1; try { this.dataChannel.send(e); } catch (n) { return this._buffering = !0, setTimeout(function() { t._buffering = !1, t._tryBuffer(); }, 100), !1 } return !0 }, u.prototype._tryBuffer = function() {
                            if (this.open && 0 !== this._buffer.length) {
                                var e = this._buffer[0];
                                this._trySend(e) && (this._buffer.shift(), this._bufferSize = this._buffer.length, this._tryBuffer());
                            }
                        }, u.prototype._sendChunks = function(e) {
                            var n, i, o = r.util.chunk(e);
                            try {
                                for (var a = t(o), s = a.next(); !s.done; s = a.next()) {
                                    var u = s.value;
                                    this.send(u, !0);
                                }
                            } catch (l) { n = { error: l }; } finally { try { s && !s.done && (i = a.return) && i.call(a); } finally { if (n) throw n.error } }
                        }, u.prototype.handleMessage = function(e) {
                            var t = e.payload;
                            switch (e.type) {
                                case s.ServerMessageType.Answer:
                                    this._peerBrowser = t.browser, this._negotiator.handleSDP(e.type, t.sdp);
                                    break;
                                case s.ServerMessageType.Candidate:
                                    this._negotiator.handleCandidate(t.candidate);
                                    break;
                                default:
                                    o.default.warn("Unrecognized message type:", e.type, "from peer:", this.peer);
                            }
                        }, u.ID_PREFIX = "dc_", u
                    }(u.BaseConnection);
                exports.DataConnection = l;
            }, { "reliable": "aYFJ", "./util": "BHXf", "./logger": "8WOs", "./negotiator": "HCdX", "./enums": "9ZRY", "./baseconnection": "tQFK" }],
            "in7L": [function(require, module, exports) {
                var t = this && this.__awaiter || function(t, e, r, o) {
                        return new(r || (r = Promise))(function(n, s) {
                            function i(t) { try { a(o.next(t)); } catch (e) { s(e); } }

                            function u(t) { try { a(o.throw(t)); } catch (e) { s(e); } }

                            function a(t) { t.done ? n(t.value) : new r(function(e) { e(t.value); }).then(i, u); }
                            a((o = o.apply(t, e || [])).next());
                        })
                    },
                    e = this && this.__generator || function(t, e) {
                        var r, o, n, s, i = { label: 0, sent: function() { if (1 & n[0]) throw n[1]; return n[1] }, trys: [], ops: [] };
                        return s = { next: u(0), throw: u(1), return: u(2) }, "function" == typeof Symbol && (s[Symbol.iterator] = function() { return this }), s;

                        function u(s) {
                            return function(u) {
                                return function(s) {
                                    if (r) throw new TypeError("Generator is already executing.");
                                    for (; i;) try {
                                        if (r = 1, o && (n = 2 & s[0] ? o.return : s[0] ? o.throw || ((n = o.return) && n.call(o), 0) : o.next) && !(n = n.call(o, s[1])).done) return n;
                                        switch (o = 0, n && (s = [2 & s[0], n.value]), s[0]) {
                                            case 0:
                                            case 1:
                                                n = s;
                                                break;
                                            case 4:
                                                return i.label++, { value: s[1], done: !1 };
                                            case 5:
                                                i.label++, o = s[1], s = [0];
                                                continue;
                                            case 7:
                                                s = i.ops.pop(), i.trys.pop();
                                                continue;
                                            default:
                                                if (!(n = (n = i.trys).length > 0 && n[n.length - 1]) && (6 === s[0] || 2 === s[0])) { i = 0; continue }
                                                if (3 === s[0] && (!n || s[1] > n[0] && s[1] < n[3])) { i.label = s[1]; break }
                                                if (6 === s[0] && i.label < n[1]) { i.label = n[1], n = s; break }
                                                if (n && i.label < n[2]) { i.label = n[2], i.ops.push(s); break }
                                                n[2] && i.ops.pop(), i.trys.pop();
                                                continue
                                        }
                                        s = e.call(t, i);
                                    } catch (u) { s = [6, u], o = 0; } finally { r = n = 0; }
                                    if (5 & s[0]) throw s[1];
                                    return { value: s[0] ? s[1] : void 0, done: !0 }
                                }([s, u])
                            }
                        }
                    },
                    r = this && this.__importDefault || function(t) { return t && t.__esModule ? t : { default: t } };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var o = require("./util"),
                    n = r(require("./logger")),
                    s = function() {
                        function r(t) { this._options = t; }
                        return r.prototype._buildUrl = function(t) { var e = (this._options.secure ? "https://" : "http://") + this._options.host + ":" + this._options.port + this._options.path + this._options.key + "/" + t; return e += "?ts=" + (new Date).getTime() + Math.random() }, r.prototype.retrieveId = function() {
                            return t(this, void 0, Promise, function() {
                                var t, r, s, i;
                                return e(this, function(e) {
                                    switch (e.label) {
                                        case 0:
                                            t = this._buildUrl("id"), e.label = 1;
                                        case 1:
                                            return e.trys.push([1, 3, , 4]), [4, fetch(t)];
                                        case 2:
                                            if (200 !== (r = e.sent()).status) throw new Error("Error. Status:" + r.status);
                                            return [2, r.text()];
                                        case 3:
                                            throw s = e.sent(), n.default.error("Error retrieving ID", s), i = "", "/" === this._options.path && this._options.host !== o.util.CLOUD_HOST && (i = " If you passed in a `path` to your self-hosted PeerServer, you'll also need to pass in that same path when creating a new Peer."), new Error("Could not get an ID from the server." + i);
                                        case 4:
                                            return [2]
                                    }
                                })
                            })
                        }, r.prototype.listAllPeers = function() {
                            return t(this, void 0, Promise, function() {
                                var t, r, s, i;
                                return e(this, function(e) {
                                    switch (e.label) {
                                        case 0:
                                            t = this._buildUrl("peers"), e.label = 1;
                                        case 1:
                                            return e.trys.push([1, 3, , 4]), [4, fetch(t)];
                                        case 2:
                                            if (200 !== (r = e.sent()).status) { if (401 === r.status) throw s = "", s = this._options.host === o.util.CLOUD_HOST ? "It looks like you're using the cloud server. You can email team@peerjs.com to enable peer listing for your API key." : "You need to enable `allow_discovery` on your self-hosted PeerServer to use this feature.", new Error("It doesn't look like you have permission to list peers IDs. " + s); throw new Error("Error. Status:" + r.status) }
                                            return [2, r.json()];
                                        case 3:
                                            throw i = e.sent(), n.default.error("Error retrieving list peers", i), new Error("Could not get list peers from the server." + i);
                                        case 4:
                                            return [2]
                                    }
                                })
                            })
                        }, r
                    }();
                exports.API = s;
            }, { "./util": "BHXf", "./logger": "8WOs" }],
            "Hxpd": [function(require, module, exports) {
                var e = this && this.__extends || function() {
                        var e = function(t, n) {
                            return (e = Object.setPrototypeOf || { __proto__: [] }
                                instanceof Array && function(e, t) { e.__proto__ = t; } || function(e, t) { for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]); })(t, n)
                        };
                        return function(t, n) {
                            function r() { this.constructor = t; }
                            e(t, n), t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r);
                        }
                    }(),
                    t = this && this.__assign || function() {
                        return (t = Object.assign || function(e) {
                            for (var t, n = 1, r = arguments.length; n < r; n++)
                                for (var o in t = arguments[n]) Object.prototype.hasOwnProperty.call(t, o) && (e[o] = t[o]);
                            return e
                        }).apply(this, arguments)
                    },
                    n = this && this.__values || function(e) {
                        var t = "function" == typeof Symbol && e[Symbol.iterator],
                            n = 0;
                        return t ? t.call(e) : { next: function() { return e && n >= e.length && (e = void 0), { value: e && e[n++], done: !e } } }
                    },
                    r = this && this.__read || function(e, t) {
                        var n = "function" == typeof Symbol && e[Symbol.iterator];
                        if (!n) return e;
                        var r, o, i = n.call(e),
                            s = [];
                        try {
                            for (;
                                (void 0 === t || t-- > 0) && !(r = i.next()).done;) s.push(r.value);
                        } catch (c) { o = { error: c }; } finally { try { r && !r.done && (n = i.return) && n.call(i); } finally { if (o) throw o.error } }
                        return s
                    },
                    o = this && this.__importDefault || function(e) { return e && e.__esModule ? e : { default: e } };
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var i = require("eventemitter3"),
                    s = require("./util"),
                    c = o(require("./logger")),
                    a = require("./socket"),
                    l = require("./mediaconnection"),
                    d = require("./dataconnection"),
                    u = require("./enums"),
                    p = require("./api"),
                    f = function(o) {
                        function i(e, n) { var r = o.call(this) || this; return r._destroyed = !1, r._disconnected = !1, r._open = !1, r._connections = new Map, r._lostMessages = new Map, e && e.constructor == Object ? (n = e, e = void 0) : e && (e = e.toString()), n = t({ debug: 0, host: s.util.CLOUD_HOST, port: s.util.CLOUD_PORT, path: "/", key: i.DEFAULT_KEY, token: s.util.randomToken(), config: s.util.defaultConfig }, n), r._options = n, "/" === n.host && (n.host = window.location.hostname), "/" !== n.path[0] && (n.path = "/" + n.path), "/" !== n.path[n.path.length - 1] && (n.path += "/"), void 0 === n.secure && n.host !== s.util.CLOUD_HOST ? n.secure = s.util.isSecure() : n.host == s.util.CLOUD_HOST && (n.secure = !0), n.logFunction && c.default.setLogFunction(n.logFunction), c.default.logLevel = n.debug, s.util.supports.audioVideo || s.util.supports.data ? s.util.validateId(e) ? (r._api = new p.API(n), r._initializeServerConnection(), e ? r._initialize(e) : r._api.retrieveId().then(function(e) { return r._initialize(e) }).catch(function(e) { return r._abort(u.PeerErrorType.ServerError, e) }), r) : (r._delayedAbort(u.PeerErrorType.InvalidID, 'ID "' + e + '" is invalid'), r) : (r._delayedAbort(u.PeerErrorType.BrowserIncompatible, "The current browser does not support WebRTC"), r) }
                        return e(i, o), Object.defineProperty(i.prototype, "id", { get: function() { return this._id }, enumerable: !0, configurable: !0 }), Object.defineProperty(i.prototype, "options", { get: function() { return this._options }, enumerable: !0, configurable: !0 }), Object.defineProperty(i.prototype, "open", { get: function() { return this._open }, enumerable: !0, configurable: !0 }), Object.defineProperty(i.prototype, "socket", { get: function() { return this._socket }, enumerable: !0, configurable: !0 }), Object.defineProperty(i.prototype, "connections", {
                            get: function() {
                                var e, t, o = Object.create(null);
                                try {
                                    for (var i = n(this._connections), s = i.next(); !s.done; s = i.next()) {
                                        var c = r(s.value, 2),
                                            a = c[0],
                                            l = c[1];
                                        o[a] = l;
                                    }
                                } catch (d) { e = { error: d }; } finally { try { s && !s.done && (t = i.return) && t.call(i); } finally { if (e) throw e.error } }
                                return o
                            },
                            enumerable: !0,
                            configurable: !0
                        }), Object.defineProperty(i.prototype, "destroyed", { get: function() { return this._destroyed }, enumerable: !0, configurable: !0 }), Object.defineProperty(i.prototype, "disconnected", { get: function() { return this._disconnected }, enumerable: !0, configurable: !0 }), i.prototype._initializeServerConnection = function() {
                            var e = this;
                            this._socket = new a.Socket(this._options.secure, this._options.host, this._options.port, this._options.path, this._options.key), this.socket.on(u.SocketEventType.Message, function(t) { e._handleMessage(t); }), this.socket.on(u.SocketEventType.Error, function(t) { e._abort(u.PeerErrorType.SocketError, t); }), this.socket.on(u.SocketEventType.Disconnected, function() { e.disconnected || (e.emitError(u.PeerErrorType.Network, "Lost connection to server."), e.disconnect()); }), this.socket.on(u.SocketEventType.Close, function() { e.disconnected || e._abort(u.PeerErrorType.SocketClosed, "Underlying socket is already closed."); });
                        }, i.prototype._initialize = function(e) { this._id = e, this.socket.start(this.id, this._options.token); }, i.prototype._handleMessage = function(e) {
                            var t, r, o = e.type,
                                i = e.payload,
                                s = e.src;
                            switch (o) {
                                case u.ServerMessageType.Open:
                                    this.emit(u.PeerEventType.Open, this.id), this._open = !0;
                                    break;
                                case u.ServerMessageType.Error:
                                    this._abort(u.PeerErrorType.ServerError, i.msg);
                                    break;
                                case u.ServerMessageType.IdTaken:
                                    this._abort(u.PeerErrorType.UnavailableID, 'ID "' + this.id + '" is taken');
                                    break;
                                case u.ServerMessageType.InvalidKey:
                                    this._abort(u.PeerErrorType.InvalidKey, 'API KEY "' + this._options.key + '" is invalid');
                                    break;
                                case u.ServerMessageType.Leave:
                                    c.default.log("Received leave message from", s), this._cleanupPeer(s), this._connections.delete(s);
                                    break;
                                case u.ServerMessageType.Expire:
                                    this.emitError(u.PeerErrorType.PeerUnavailable, "Could not connect to peer " + s);
                                    break;
                                case u.ServerMessageType.Offer:
                                    var a = i.connectionId;
                                    if ((_ = this.getConnection(s, a)) && (_.close(), c.default.warn("Offer received for existing Connection ID:", a)), i.type === u.ConnectionType.Media) _ = new l.MediaConnection(s, this, { connectionId: a, _payload: i, metadata: i.metadata }), this._addConnection(s, _), this.emit(u.PeerEventType.Call, _);
                                    else {
                                        if (i.type !== u.ConnectionType.Data) return void c.default.warn("Received malformed connection type:", i.type);
                                        _ = new d.DataConnection(s, this, { connectionId: a, _payload: i, metadata: i.metadata, label: i.label, serialization: i.serialization, reliable: i.reliable }), this._addConnection(s, _), this.emit(u.PeerEventType.Connection, _);
                                    }
                                    var p = this._getMessages(a);
                                    try {
                                        for (var h = n(p), f = h.next(); !f.done; f = h.next()) {
                                            var y = f.value;
                                            _.handleMessage(y);
                                        }
                                    } catch (v) { t = { error: v }; } finally { try { f && !f.done && (r = h.return) && r.call(h); } finally { if (t) throw t.error } }
                                    break;
                                default:
                                    if (!i) return void c.default.warn("You received a malformed message from " + s + " of type " + o);
                                    var _;
                                    a = i.connectionId;
                                    (_ = this.getConnection(s, a)) && _.peerConnection ? _.handleMessage(e) : a ? this._storeMessage(a, e) : c.default.warn("You received an unrecognized message:", e);
                            }
                        }, i.prototype._storeMessage = function(e, t) { this._lostMessages.has(e) || this._lostMessages.set(e, []), this._lostMessages.get(e).push(t); }, i.prototype._getMessages = function(e) { var t = this._lostMessages.get(e); return t ? (this._lostMessages.delete(e), t) : [] }, i.prototype.connect = function(e, t) { if (void 0 === t && (t = {}), this.disconnected) return c.default.warn("You cannot connect to a new Peer because you called .disconnect() on this Peer and ended your connection with the server. You can create a new Peer to reconnect, or call reconnect on this peer if you believe its ID to still be available."), void this.emitError(u.PeerErrorType.Disconnected, "Cannot connect to new Peer after disconnecting from server."); var n = new d.DataConnection(e, this, t); return this._addConnection(e, n), n }, i.prototype.call = function(e, t, n) {
                            if (void 0 === n && (n = {}), this.disconnected) return c.default.warn("You cannot connect to a new Peer because you called .disconnect() on this Peer and ended your connection with the server. You can create a new Peer to reconnect."), void this.emitError(u.PeerErrorType.Disconnected, "Cannot connect to new Peer after disconnecting from server.");
                            if (t) { n._stream = t; var r = new l.MediaConnection(e, this, n); return this._addConnection(e, r), r }
                            c.default.error("To call a peer, you must provide a stream from your browser's `getUserMedia`.");
                        }, i.prototype._addConnection = function(e, t) { c.default.log("add connection " + t.type + ":" + t.connectionId + "\n       to peerId:" + e), this._connections.has(e) || this._connections.set(e, []), this._connections.get(e).push(t); }, i.prototype._removeConnection = function(e) {
                            var t = this._connections.get(e.peer);
                            if (t) { var n = t.indexOf(e); - 1 !== n && t.splice(n, 1); }
                            this._lostMessages.delete(e.connectionId);
                        }, i.prototype.getConnection = function(e, t) { var r, o, i = this._connections.get(e); if (!i) return null; try { for (var s = n(i), c = s.next(); !c.done; c = s.next()) { var a = c.value; if (a.connectionId === t) return a } } catch (l) { r = { error: l }; } finally { try { c && !c.done && (o = s.return) && o.call(s); } finally { if (r) throw r.error } } return null }, i.prototype._delayedAbort = function(e, t) {
                            var n = this;
                            setTimeout(function() { n._abort(e, t); }, 0);
                        }, i.prototype._abort = function(e, t) { c.default.error("Aborting!"), this._lastServerId ? this.disconnect() : this.destroy(), this.emitError(e, t); }, i.prototype.emitError = function(e, t) { c.default.error("Error:", t), "string" == typeof t && (t = new Error(t)), t.type = e, this.emit(u.PeerEventType.Error, t); }, i.prototype.destroy = function() { this.destroyed || (this._cleanup(), this.disconnect(), this._destroyed = !0); }, i.prototype._cleanup = function() {
                            var e, t;
                            try {
                                for (var r = n(this._connections.keys()), o = r.next(); !o.done; o = r.next()) {
                                    var i = o.value;
                                    this._cleanupPeer(i), this._connections.delete(i);
                                }
                            } catch (s) { e = { error: s }; } finally { try { o && !o.done && (t = r.return) && t.call(r); } finally { if (e) throw e.error } }
                            this.emit(u.PeerEventType.Close);
                        }, i.prototype._cleanupPeer = function(e) { var t, r, o = this._connections.get(e); if (o) try { for (var i = n(o), s = i.next(); !s.done; s = i.next()) { s.value.close(); } } catch (c) { t = { error: c }; } finally { try { s && !s.done && (r = i.return) && r.call(i); } finally { if (t) throw t.error } } }, i.prototype.disconnect = function() {
                            var e = this;
                            setTimeout(function() { e.disconnected || (e._disconnected = !0, e._open = !1, e.socket && e.socket.close(), e.emit(u.PeerEventType.Disconnected, e.id), e._lastServerId = e.id, e._id = null); }, 0);
                        }, i.prototype.reconnect = function() {
                            if (this.disconnected && !this.destroyed) c.default.log("Attempting reconnection to server with ID " + this._lastServerId), this._disconnected = !1, this._initializeServerConnection(), this._initialize(this._lastServerId);
                            else {
                                if (this.destroyed) throw new Error("This peer cannot reconnect to the server. It has already been destroyed.");
                                if (this.disconnected || this.open) throw new Error("Peer " + this.id + " cannot reconnect because it is not disconnected from the server!");
                                c.default.error("In a hurry? We're still trying to make the initial connection!");
                            }
                        }, i.prototype.listAllPeers = function(e) {
                            var t = this;
                            void 0 === e && (e = function(e) {}), this._api.listAllPeers().then(function(t) { return e(t) }).catch(function(e) { return t._abort(u.PeerErrorType.ServerError, e) });
                        }, i.DEFAULT_KEY = "peerjs", i
                    }(i.EventEmitter);
                exports.Peer = f;
            }, { "eventemitter3": "2JJl", "./util": "BHXf", "./logger": "8WOs", "./socket": "wJlv", "./mediaconnection": "dbHP", "./dataconnection": "GBTQ", "./enums": "9ZRY", "./api": "in7L" }],
            "iTK6": [function(require, module, exports) {
                Object.defineProperty(exports, "__esModule", { value: !0 });
                var e = require("./util"),
                    r = require("./peer");
                exports.peerjs = { Peer: r.Peer, util: e.util }, exports.default = r.Peer, window.peerjs = exports.peerjs, window.Peer = r.Peer;
            }, { "./util": "BHXf", "./peer": "Hxpd" }]
        }, {}, ["iTK6"]);
        
    });

    var Peer = /*@__PURE__*/getDefaultExportFromCjs(peerjs_min);

    const questions = [
        "What two words would passengers never want to hear a pilot say?",
        "You would never go on a roller coaster called \"BLANK\"",
        "The secret to a happy life",
        "If a winning coach gets Gatorade dumped on his head, what should get dumped on the losing coach?",
        "Name a candle scent designed specifically for Kim Kardashian",
        "You should never give alcohol to \"BLANK\"",
        "Everyone knows that monkeys hate \"BLANK\"",
        "The biggest downside to living in Hell",
        "Jesus's REAL last words",
        "The worst thing for an evil witch to turn you into",
        "The Skittles flavor that just missed the cut",
        "On your wedding night, it would be horrible to find out that the person you married is \"BLANK\"",
        "A name for a really bad Broadway musical",
        "The first thing you would do after winning the lottery",
        "What's actually causing global warming?",
        "A name for a brand of designer adult diapers",
        "Name a TV drama that's about a vampire doctor",
        "Something squirrels probably do when no one is looking",
        "The crime you would commit if you could get away with it",
        "Come up with a great title for the next awkward teen sex movie",
        "What's the Mona Lisa smiling about?",
        "A terrible name for a cruise ship",
        "What FDR meant to say was We have nothing to fear, but \"BLANK\"",
        "Come up with a title for an adult version of any classic video game",
        "The name of a font nobody would ever use",
        "Something you should never put on an open wound",
        "Scientists say erosion, but we all know the Grand Canyon was actually made by \"BLANK\"",
        "The real reason the dinosaurs died",
        "Come up with the name of a country that doesn't exist",
        "The best way to keep warm on a cold winter night",
        "A college major you don't see at many universities",
        "What would make baseball more entertaining to watch?",
        "The best thing about going to prison",
        "The best title for a new national anthem for the USA",
        "Come up with the name of book that would sell a million copies, immediately",
        "What would you do if you were left alone in the White House for an hour?",
        "Invent a family-friendly replacement word that you could say instead of an actual curse word",
        "A better name for testicles",
        "The name of the reindeer Santa didn't pick to pull his sleigh",
        "What's the first thing you would do if you could time travel?",
        "The name of a pizza place you should never order from",
        "A not-very-scary name for a pirate",
        "Come up with a name for a beer made especially for monkeys",
        "The best thing about living in an igloo",
        "The worst way to be murdered",
        "Something you shouldn't get your significant other for Valentine's Day",
        "A dangerous thing to do while driving",
        "Something you shouldn't wear to a job interview",
        "The #1 reason penguins can't fly",
        "Using only two words, a new state motto for Texas",
        "The hardest thing about being Batman",
        "A great way to kill time at work",
        "Come up with a really bad TV show that starts with Baby",
        "Why does the Tower of Pisa lean?",
        "What's wrong with these kids today?",
        "A great new invention that starts with Automatic",
        "Come up with a really bad football penalty that begins with Intentional",
        "A Starbucks coffee that should never exist",
        "There's Gryffindor, Ravenclaw, Slytherin, and Hufflepuff, but what's the Hogwarts house few have ever heard of?",
        "The worst words to say for the opening of a eulogy at a funeral",
        "Something you should never use as a scarf",
        "Invent a holiday that you think everyone would enjoy",
        "The best news you could get today",
        "Usually, it's bacon,lettuce and tomato, but come up with a BLT you wouldn't want to eat",
        "The worst thing you could stuff a bed mattress with",
        "A great opening line to start a conversation with a stranger at a party",
        "Something you would like to fill a swimming pool with",
        "Miley Cyrus' Wi-Fi password, possibly",
        "If you were allowed to name someone else's baby any weird thing you wanted, what would you name it?",
        "A fun thing to think about during mediocre sex",
        "You know you're in for a bad taxi ride when \"BLANK\"",
        "Where do babies come from?",
        "The terrible fate of the snowman Olaf in a director's cut of \"Frozen\"",
        "Sometimes, after a long day, you just need to \"BLANK\"",
        "The worst way to spell Mississippi",
        "Give me one good reason why I shouldn't spank you right now",
        "The best pick-up line for an elderly singles mixer",
        "A good stage name for a chimpanzee stripper",
        "The best place to bury all those bodies",
        "One place a finger shouldn't go",
        "Come up with a name for the most difficult yoga pose known to mankind",
        "What's lurking under your bed when you sleep?",
        "The name of a canine comedy club with puppy stand-up comedians",
        "A great name for a nude beach in Alaska",
        "Make up the title of a movie that is based on the first time you had sex",
        "A vanity license plate a jerk in an expensive car would get",
        "A good fake name to use when checking into a hotel",
        "A good catchphrase to yell every time you finish pooping",
        "Your personal catchphrase if you were on one of those \"Real Housewives\" shows",
        "The Katy Perry Super Bowl halftime show would have been better with \"BLANK\"",
        "Okay... fine! What do YOU want to talk about then?!!!",
        "Miller Lite beer would make a lot of money if they came up with a beer called Miller Lite _____",
        "Something you should never stick up your butt",
        "A terrible name for a clown",
        "An inappropriate thing to do at a cemetery",
        "Like chicken fingers or chicken poppers, a new appetizer name for your fun, theme restaurant: chicken _____",
        "Thing you'd be most surprised to have a dentist a find in your mouth",
        "Rename Winnie-the-Pooh to something more appropriate/descriptive",
        "Name the sequel to \"Titanic\" if there were one. \"Titanic 2: \"BLANK\"\"",
        "An alternate use for a banana",
        "What you'd guess is an unadvertised ingredient in most hot dogs",
        "Name your new haircutting establishment",
        "Something that would make an awful hat",
        "How many monkeys is too many monkeys?",
        "Something you'd be surprised to see a donkey do",
        "The title you'd come up with if you were writing the Olympics theme song",
        "Something you should never say to your mother",
        "Come up with a name for a new, very manly cocktail",
        "Where's the best place to hide from the shadow monsters?",
        "The three ingredients in the worst smoothie ever",
        "The best thing to use when you're out of toilet paper",
        "Come up with a catchier, more marketable name for the Bible",
        "The most presidential name you can think of (that isn't already the name of a president)",
        "A good way to get fired",
        "If we can't afford to bury or cremate you, what should we do with your body?",
        "Name the eighth dwarf, who got cut at the last minute",
        "A good place to hide boogers",
        "Come up with the name for a new TV show with the word Spanky in it",
        "A fun trick to play on the Pope",
        "Where do you think the beef really is?",
        "Something it'd be fun to throw off the Eiffel Tower",
        "Write a newspaper headline that will really catch people's attention",
        "The worst job title that starts with Assistant",
        "The last person you'd consider inviting to your birthday party",
        "The grossest thing you'd put in your mouth for $18",
        "What John Goodman's belches smell like",
        "The name of a new perfume by Betty White",
        "The worst name for a robot",
        "The first names of each of your nipples",
        "The most embarrassing name for a dog",
        "The worst thing you could discover in your burrito",
        "One thing never to do on a first date",
        "Ozzy Osbourne's Twitter password, probably",
        "Who let the dogs out?",
        "What do vegans taste like?",
        "An item NOT found in Taylor Swift's purse",
        "Name a new reggae band made up entirely of chickens",
        "Name a children's book by someone who hates children",
        "The name of your new plumbing company",
        "Make up a word that describes the sound of farting into a bowl of mac & cheese",
        "A new ice cream flavor that no one would ever order",
        "Name a new movie starring a talking goat who is president of the United States",
        "Something that would not work well as a dip for tortilla chips",
        "If God has a sense of humor, he welcomes people to heaven by saying, \"BLANK\"",
        "The name of a clothing store for overweight leprechauns",
        "Something upsetting you could say to the cable guy as he installs your television service",
        "The worst thing that could jump out of a bachelor party cake",
        "Come up with a name for a new beer marketed toward babies",
        "A terrible theme for a high school prom",
        "Make up a name for a silent-film porno from the 1920s",
        "Something you should not whisper to your grandmother",
        "A terrible name for a 1930s gangster",
        "Brand name of a bottled water sold in the land of Oz",
        "A fun thing to yell as a baby is being born",
        "The worst family secret that could come out over Thanksgiving dinner",
        "The name of a toilet paper specifically designed for the Queen of England",
        "Something you'd probably find a lot of in God's refrigerator",
        "The worst person to narrate the audiobook of \"Fifty Shades of Grey\"",
        "A lawn decoration sure to make the neighbors mad",
        "The worst thing to say when trying to adopt a pet",
        "A good name for an erotic bakery",
        "People wouldn't respect He-Man as much if, to gain his power, he held up his sword and shouted ____________________",
        "Fun thing to do if locked in the mall overnight",
        "The worst person to receive a sponge bath from",
        "Pants would be a whole lot better if they \"BLANK\"",
        "The most awesome Guinness World Record to break",
        "A little-known way to get gum out of your hair",
        "It's bad to be buried alive. It's worse to be buried alive with \"BLANK\".",
        "Something that would not work as well as skis",
        "A rejected title for \"The Good, The Bad and the Ugly\" was \"The Good, the Bad and the \"BLANK\"\"",
        "A rejected name for a ship in the U.S. Naval Fleet: the USS \"BLANK\"",
        "What to say to get out of jury duty",
        "What the Statue of Liberty is hiding beneath that robe",
        "There's only one time that murder is acceptable and that is when \"BLANK\"",
        "Take any well-known restaurant and slightly change its name to something inappropriate",
        "Little-known fact: The government allows peanut butter to contain up to 10% \"BLANK\"",
        "A good sign that your house is haunted",
        "A catchy name for a sperm bank",
        "A bad occupation for a robot to have",
        "A sequel to the painting Dogs Playing Poker",
        "The Tooth Fairy's other job",
        "Little-known fact: A secret area in the White House is the \"BLANK\" room",
        "An invention by Thomas Edison that never caught on",
        "A bad place to skinny-dip",
        "What time is it?",
        "A birthday present you shouldn't get for your grandmother",
        "A short motto everyone should live by",
        "Invent a Christmas tradition sure to catch on",
        "A bad thing to yell during church",
        "The unsexiest thought you can have",
        "A good improvement to make to Mt. Rushmore",
        "The best way to start your day",
        "The worst name for a summer camp",
        "Something that's made worse by adding cheese",
        "Three things are certain in life: Death, Taxes, and \"BLANK\"",
        "A faster way to get home from the Land of Oz is to click your heels three times and say \"BLANK\".",
        "The first commandment in the new religion you started",
        "Come up with a name for a rock band made up entirely of baby ducks",
        "Something that is currently legal that should be banned",
        "A word that should never follow Beef",
        "The perfect song to hum on the toilet",
        "A bad thing to say to a cop as he writes you a speeding ticket",
        "Something you shouldn't buy off of Craigslist",
        "Take any U.S. president's name and turn it into something inappropriate",
        "We can all agree that \"BLANK\"",
        "The name you would give to a really mopey pig",
        "A great name to have on a fake I.D.",
        "What robots dream about",
        "What really happened to Amelia Earhart",
        "How far is too far?",
        "If at first you don't succeed...",
        "Finish this sentence: When I'm rich, my mansion will have a room called The \"BLANK\" Room.",
        "Something you'd be surprised to see come out of a pimple you pop",
        "Today's music needs more \"BLANK\"",
        "A fun trick to play on your doctor",
        "A bad place for your rocket ship to crash would be The Planet of the \"BLANK\"",
        "A bad campaign slogan for a congressperson",
        "The coolest way to die",
        "Two people from history that should definitely have sex",
        "The name of an all-male version of Hooters",
        "A little-known nickname for New Orleans",
        "The next product for Matthew McConaughey to endorse",
        "A unique way to escape from prison",
        "The title of a new YouTube cat video that's sure to go viral",
        "A gift nobody would want: The \"BLANK\" of the Month Club",
        "A just-so-crazy-it's-brilliant business idea to pitch on \"Shark Tank\"",
        "A terrifying fortune cookie fortune",
        "It would be scary to read on a food package, May contain trace elements of \"BLANK\".",
        "What a dog sext message might say",
        "Something the devil is afraid of",
        "CBS should air a TV show about lawyers who are also \"BLANK\"",
        "A great thing to yell before jumping out of an airplane",
        "What you hope the Mars Rover finds",
        "A TMZ headline you really want to see",
        "Something that will get you thrown out of a Wendy's",
        "A rejected phrase for one of those Valentine heart candies",
        "Where missing socks go",
        "The first sign that you're old",
        "The name of a cocktail for hillbillies",
        "Graffiti you might find in a kindergarten",
        "The worst thing to wear to your court trial",
        "A rejected crayon color",
        "An angry review you'd give this game (Quiplash)",
        "Bad advice for new graduates",
        "The best way to tell if someone is dead",
        "A terrible talent to have for the Miss America Pageant",
        "The worst",
        "Tomorrow's news headline: Scientists Are Shocked to Discover That \"BLANK\"",
        "The worst material with which to make a snowman",
        "A terrible sportscaster catchphrase for when somebody dunks a basketball",
        "The first thing a pig would say if it could talk",
        "A surprising job entry on Abraham Lincoln's resume",
        "The worst shape for an animal cracker",
        "A weird thing to find in your grandparents' bedside table",
        "The worst name for a big and tall store",
        "Something you'd yell to heckle the performing dolphins at Sea World",
        "A new name for kumquats",
        "The name of a shampoo for hippies",
        "The real secret to living to age 100",
        "What really happens if you tear off that mattress tag",
        "A bad first line for your presidential inauguration speech",
        "A fun thing to do with a bowl of pudding",
        "Another use for cooked spaghetti",
        "A weird physical way to greet someone",
        "The worst name for a tanning salon",
        "The worst word that can come before fart",
        "A bad substitute for a toothbrush",
        "A trick you shouldn't teach your dog",
        "Something you can only do in a Walmart if no one's looking",
        "A name for a really cheap hotel",
        "The second thing said on the moon",
        "Why so serious?",
        "A tourist attraction in Hell",
        "The worst name for a mountain",
        "A thought that keeps Santa Claus awake at night",
        "The best thing about being really dumb",
        "Come up with a name for a salad dressing by Lindsay Lohan",
        "What they call pooping in the Land of Oz",
        "A completely wrong way to spell Jennifer Aniston",
        "The worst way to remove pubic hair",
        "You know you're really drunk when...",
        "The best way to defeat terrorism is...",
        "An animal Noah shouldn't have saved",
        "The biggest secret the government keeps",
        "The password to the secret, high-society sex club down the street",
        "Another use for gravy",
        "The worst name for a rap artist",
        "An angry internet comment on a pet store's website",
        "A rejected shape for Marshmallow Peeps",
        "Something that should never be homemade",
        "The worst name for a funeral home",
        "What Chewbacca has really been yelling all these years",
        "An item on every pervert's grocery list",
        "The worst car feature that ends with holder",
        "A Tweet from a caveman",
        "Knock, knock! Who's there? \"BLANK\"",
        "A great nickname for your armpit hair",
        "Pick any city name and make it sound dirty",
        "What you want your gravestone to read",
        "A slogan to get everyone excited about corn",
        "It never ends well when you mix \"BLANK\" and \"BLANK\"",
        "The best reason to go to Australia",
        "The beauty pageant no one wants to see: Miss \"BLANK\"",
        "The perfect meal would be a \"BLANK\" stuffed in a \"BLANK\" stuffed in a \"BLANK\"",
        "What's black and white and red all over?",
        "A little-known fact about the Jolly Green Giant",
        "The worst thing to find growing on your neck",
        "USA! USA! America is still number one in...",
        "A good name for an elderly nudist colony",
        "You should never \"BLANK\" and \"BLANK\" at the same time",
        "What is a tree thinking all day?",
        "What you call a baby sasquatch",
        "A good name for a sex robot",
        "A bad reason to call 911",
        "Name the next big sexually transmitted disease",
        "The worst thing about Canada",
        "A strange thing to keep as a pet",
        "What kittens would say if they could talk",
        "A sign you probably shouldn't put up in your yard",
        "What dogs think when they see people naked",
        "The sound a tree actually makes when it falls and no one is around to hear it",
        "The grossest thing you could find at the bottom of a swimming pool",
        "What happens to circumcision skin",
        "The worst name for an SUV",
        "A good use for toenail clippings",
        "The title of the most boring porno ever",
        "Something you shouldn't stuff with cheese",
        "Something Godzilla does when he's drunk",
        "Trash talk you would hear at a chess meet",
        "A kinky weird thing that does NOT happen in 50 Shades of Grey (as far as you know)",
        "The best part about being Donald Trump",
        "Tip: Never eat at a place called Kentucky Fried \"BLANK\"",
        "Something overheard at the Last Supper",
        "The name of a new species",
        "It's not \"Stinky Pete\" anymore - he's now called...",
        "You found WHO'S PUBE WHERE!?",
        "I knew I shouldn't have booked with \"BLANK\" airlines!",
        "I know it's wrong but right now I could murder...",
        "Quincunx is an actual word with a boring definition what should quincunx mean?",
        "Your surgeon comes to see you just after your operation and begins with the word \"Unfotunately\". What she says next is...",
        "The last thing you would expect your mother to be knitting",
        "The reason I will get arrested",
        "Say good bye to peanut butter and jelly the new sandwich craze is \"BLANK\" and \"BLANK\"",
        "The real reason you didn't do you homework",
        "What's really living in Boris Johnsons Hair",
        "The ultimate reason Donald Trump will go down in history",
    ];

    const colors = [
        'rgb(255,51,51)',
        'rgb(255,167,51)',
        'rgb(255,234,51)',
        'rgb(92,255,51)',
        'rgb(51,255,255)',
        'rgb(255,51,255)',
        'rgb(167,167,255)',
        'rgb(255,255,167)',
        'rgb(255,167,255)',
        'rgb(255,255,255)',
        'rgb(255,255,255)',
        'rgb(217,217,217)',
    ];

    const shuffled = questions.reduce((a, v) => a.splice(Math.floor(Math.random() * a.length), 0, v) && a, []);
    const rainbow = colors.reduce((a, v) => a.splice(Math.floor(Math.random() * a.length), 0, v) && a, []);

    let peer$1, gamecode$1, playerKeys, roundQuestions$1 = [],
        connections = {},
        allAnswers = {},
        votingOn = 0,
        colorIndex = 0,
        numberOfRounds;

    function reset$1() {
        peer$1 = null;
        gamecode$1 = null;
        playerKeys = null;
        roundQuestions$1 = [];
        connections = {};
        allAnswers = {};
        votingOn = 0;
        colorIndex = 0;
    }


    function randColor() {
        const c = rainbow[colorIndex];
        colorIndex += 1;
        console.log('rand color', colorIndex, c);
        if (colorIndex >= rainbow.length) colorIndex = 0;
        return c
    }

    function cleanup() {
        if (peer$1) {
            peer$1.disconnect();
            peer$1.destroy();
        }
    }

    function tellAll(kind, data) {
        console.log('in tell all');
        for (let key in connections) {
            const conn = connections[key];
            conn.send({ kind: kind, data: data });
        }
    }

    function addPlayer(peer, data) {
        data.color = randColor();
        console.log('color:', data.color, peer);
        players.update(function (p) {
            p[peer] = data;
            return p;
        });
    }

    function answersByIndex() {
        let aHash = {};
        let result = [];
        for (var key in allAnswers) {
            var quests = allAnswers[key];
            quests.forEach(function (q) {
                aHash[q[0]] = aHash[q[0]] || [q[0], q[1],
                []
                ];
                aHash[q[0]][2].push([key, q[2],
                    []
                ]);
            });
        }
        for (var key in aHash) {
            result.push(aHash[key]);
        }
        return result;
    }

    function voteNextAnswer() {
        const thisOne = allAnswers[votingOn];
        votingOn += 1;
        if (thisOne) {
            tellAll('vote', thisOne);
            vote.set(null);
            ballot.set([...thisOne, []]);
            state.set('vote');
            return true;
        } else {
            console.log('time to tally votes');
            return false;
        }
    }

    function showScores() {
        tellAll('scores', null);
        state.set('scores');
    }

    function tallyVotes(answers) {
        // work out scores add them into the players list
        console.log('all answers is:', allAnswers);
        players.update(function (p) {
            answers.forEach(function (answer) {
                const submitter = answer[0];
                p[submitter]['score'] += answer[3];
            });
            console.log('players is:', p);
            return p
        });
        // update of players will automatically send details to clients through player subscribe below
    }

    function recordAnswer(peer, data) {
        allAnswers[peer] = data;
        if (Object.keys(allAnswers).length == playerKeys.length) {
            // index by question
            // semd questions one by one to clients for voting
            // all clients see all questions but you can vote for you own answer and if there is only one other answer
            allAnswers = answersByIndex();
            votingOn = 0;
            console.log('answers indexed', allAnswers);
            voteNextAnswer();
        }
    }

    function recordVote(peer, data) {
        // vote comes in with data of [question number, peer of answer liked]
        // need to find matching peer in first element of b[2] array and add incoming peer to list of voters
        console.log('recording vote', peer, data);
        ballot.update(function (b) {
            const ans = b[2].find(function (i) { return i[0] == data[1] });
            console.log('matching answer', ans);
            if (ans) ans[2].push(peer);
            b[3].push(peer);
            console.log('All answers now', allAnswers);
            return b;
        });
    }

    function processMessage$1(peer, msg) {
        console.log('process: ', msg);
        const { kind, data } = msg;
        switch (kind) {
            case 'addPlayer':
                console.log('adding player', peer, data);
                addPlayer(peer, data);
                break;
            case 'answers':
                recordAnswer(peer, data);
                console.log('received answers', data);
                break;
            case 'vote':
                recordVote(peer, data);
                break;
        }
    }

    function assignQuestions(round) {
        // shuffle members
        allAnswers = {};
        const keys = playerKeys.reduce((a, v) => a.splice(Math.floor(Math.random() * a.length), 0, v) && a, []);
        const nextQuestion = (round - 1) * playerKeys.length;
        let start = nextQuestion;
        const quests = keys.map(function (key, i) {
            const end = i == playerKeys.length - 1 ? nextQuestion : start + 1;
            let result = [key, [
                [start, shuffled[start]],
                [end, shuffled[end]]
            ]];
            start += 1;
            return result
        });
        quests.forEach(function (q) {
            const key = q[0];
            if (key == gamecode$1) {
                // set questions for server (shared function?)
                roundQuestions$1 = q[1];
                question.set(q[1][0][1]);
            } else {
                const conn = connections[key];
                console.log('sending msg:', conn.peer, q[1]);
                conn.send({ kind: 'questions', data: q[1] });
            }
        });
        // state.set('question');
        console.log(quests);
    }

    function start$1(code) {
        cleanup();
        reset$1();
        console.log('starting server with code', code);
        gamecode$1 = code;
        console.log('status: ', status);
        console.log('Gamecode: ', gamecode$1);
        console.log('b4 cleanup');
          console.log('b4 players set');
        players.set({});
        console.log('b4 new peer');
        peer$1 = new Peer(gamecode$1);
        console.log('server peer', peer$1);
        peer$1.on('open', function (id) {
            myPeerId.set(id);
            console.log('Sever peer ID is: ' + id);

            numrounds.subscribe(function (n) {
                numberOfRounds = n;
                tellAll('numrounds', numberOfRounds);
            });

            players.subscribe(function (ps) {
                console.log('players changed:', ps);
                playerKeys = Object.keys(ps);
                tellAll('players', ps);
            });

            player.subscribe(function (player) {
                if (player) {
                    addPlayer(gamecode$1, player);
                }
            });

            round.subscribe(function (r) {
                if (r > 0) {
                    console.log('setting questions for round ', r);
                    assignQuestions(r);
                }
            });

            answer.subscribe(function (a) {
                console.log('answer is:', a, a ? true : false);
                if (a) {
                    var i = roundQuestions$1.findIndex(function (i) { return i.length < 3 });
                    roundQuestions$1[i].push(a);
                    if (i >= roundQuestions$1.length - 1) {
                        state.set('waiting');
                        recordAnswer(gamecode$1, roundQuestions$1);
                    } else {
                        question.set(roundQuestions$1[i + 1][1]);
                    }
                }
            });


            vote.subscribe(function (v) {
                if (v) {
                    recordVote(gamecode$1, v);
                }
            });

            allVoted.subscribe(function (b) {
                console.log('abv', b);
                if (b) {
                    console.log('everyone has voted send results', b);
                    tellAll('voted', b);
                    state.set('voted');
                } else {
                    console.log('not all voted');
                }
            });

            serverCommand.subscribe(function (sc) {
                if (sc) {
                    const { command, data } = sc;
                    switch (command) {
                        case "tally":
                            console.log('data is:', data);
                            tallyVotes(data);
                            if (!voteNextAnswer()) {
                                showScores();
                            }                        break;
                        default:
                            console.log("cant respond to server command", cmd);
                    }
                }
            });

        });
        peer$1.on('error', function (err) {
            console.log('server peer err:', err);
        });
        peer$1.on('connection', function (c) {
            connections[c.peer] = c;
            console.log('have connection', c);
            c.on('open', function () {
                console.log('client conn open');
                c.send({ kind: 'numrounds', data: numberOfRounds });
            });
            c.on('data', function (data) {
                console.log('Received client data', data);
                processMessage$1(c.peer, data);
            });
            c.on('close', function () {
                players.update(function (p) {
                    delete p[c.peer];
                    return p
                });
            });

        });

        peer$1.on('close', function () {
            // peer.destroy();
        });



    }

    servercode.subscribe(function (code) {
        if ((code) && (code != '')) {
            start$1('squid-splash-' + code);
        }
    });

    let peer, connection, gamecode, roundQuestions = [];

    function reset() {
        peer = null;
        connection = null;
        gamecode = null;
        roundQuestions = [];
    }

    function processMessage(msg) {
        const { kind, data } = msg;
        switch (kind) {
            case 'players':
                players.update(function(p) {
                    return data;
                });
                break;
            case 'questions':
                roundQuestions = data;
                question.set(data[0][1]);
                state.set('question');
                    // round.update(function(r) {return r+1})
                break;
            case 'vote':
                vote.set(null);
                ballot.set(data);
                state.set('vote');
                break;
            case 'voted':
                ballot.set(data);
                state.set('voted');
                break;
            case 'scores':
                state.set('scores');
                break;
            case 'numrounds':
                numrounds.set(data);
                break;
            default:
                console.log('Recived unprocessable message', kind);
        }
    }

    function start(code) {
        reset();
        gamecode = code;
        peer = new Peer();
        peer.on('open', function(id) {
            myPeerId.set(id);
        });

        connection = peer.connect(gamecode);
        window.conn = connection;
        connection.on('open', function() {

            //window.conn = connection;
            // update player to force send;

            console.log('client connection got open');
            player.subscribe(function(player) {
                connection.send({ kind: 'addPlayer', data: player });
            });


            answer.subscribe(function(a) {
                if (a) {
                    var i = roundQuestions.findIndex(function(i) { return i.length < 3 });
                    roundQuestions[i].push(a);
                    if (i >= roundQuestions.length - 1) {
                        connection.send({ kind: 'answers', data: roundQuestions });
                        state.set('waiting');
                        console.log('submitted answers', roundQuestions);
                    } else {
                        question.set(roundQuestions[i + 1][1]);
                    }
                }
            });

            vote.subscribe(function(v) {
                if (v) {
                    console.log('sending vote');
                    connection.send({ kind: 'vote', data: v });
                }
            });

            /*
                    player.update(function(p) {
                        p['peer'] = connid;
                        return p;
                    });
            */

        });
        connection.on('data', function(data) {
            processMessage(data);
        });

        connection.on('close', function() {
            alert("Sorry the connection to the game host was lost.");
            state.set('back');
        });
        /*
            peer.on('disconnected',function() {
                alert("Sorry the connection to the game host was lost.");
                $state = 'back';
            });
        */
        peer.on('close', function() {
            peer.destroy();
        });


    }

    clientcode.subscribe(function(code) {
        if ((code) && (code != '')) {
            start('squid-splash-' + code);
        }
    });

    /* src/Logo.svelte generated by Svelte v3.49.0 */

    const file$i = "src/Logo.svelte";

    function create_fragment$j(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t0;
    	let h2;
    	let span0;
    	let span1;
    	let span2;
    	let span3;
    	let span4;
    	let br;
    	let t6;
    	let p;

    	const block = {
    		c: function create() {
    			div = element("div");
    			img = element("img");
    			t0 = space();
    			h2 = element("h2");
    			span0 = element("span");
    			span0.textContent = "S";
    			span1 = element("span");
    			span1.textContent = "QUI";
    			span2 = element("span");
    			span2.textContent = "DS";
    			span3 = element("span");
    			span3.textContent = "P";
    			span4 = element("span");
    			span4.textContent = "LASH";
    			br = element("br");
    			t6 = space();
    			p = element("p");
    			p.textContent = "Not exactly Quiplash";
    			if (!src_url_equal(img.src, img_src_value = "/sslogo2.png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "class", "svelte-8h6yak");
    			add_location(img, file$i, 24, 1, 473);
    			attr_dev(span0, "class", "grey svelte-8h6yak");
    			add_location(span0, file$i, 26, 2, 506);
    			attr_dev(span1, "class", "green svelte-8h6yak");
    			add_location(span1, file$i, 26, 29, 533);
    			attr_dev(span2, "class", "grey svelte-8h6yak");
    			add_location(span2, file$i, 26, 59, 563);
    			attr_dev(span3, "class", "green svelte-8h6yak");
    			add_location(span3, file$i, 26, 87, 591);
    			attr_dev(span4, "class", "blue svelte-8h6yak");
    			add_location(span4, file$i, 26, 115, 619);
    			add_location(br, file$i, 26, 145, 649);
    			attr_dev(h2, "class", "svelte-8h6yak");
    			add_location(h2, file$i, 25, 0, 499);
    			attr_dev(p, "class", "tagline svelte-8h6yak");
    			add_location(p, file$i, 28, 0, 661);
    			attr_dev(div, "id", "logo");
    			attr_dev(div, "class", "svelte-8h6yak");
    			add_location(div, file$i, 23, 0, 456);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, img);
    			append_dev(div, t0);
    			append_dev(div, h2);
    			append_dev(h2, span0);
    			append_dev(h2, span1);
    			append_dev(h2, span2);
    			append_dev(h2, span3);
    			append_dev(h2, span4);
    			append_dev(h2, br);
    			append_dev(div, t6);
    			append_dev(div, p);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$j.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$j($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Logo', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Logo> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Logo extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$j, create_fragment$j, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Logo",
    			options,
    			id: create_fragment$j.name
    		});
    	}
    }

    /* src/Button.svelte generated by Svelte v3.49.0 */
    const file$h = "src/Button.svelte";

    function create_fragment$i(ctx) {
    	let button;
    	let current;
    	let mounted;
    	let dispose;
    	const default_slot_template = /*#slots*/ ctx[4].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[3], null);

    	const block = {
    		c: function create() {
    			button = element("button");
    			if (default_slot) default_slot.c();
    			add_location(button, file$h, 16, 0, 290);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);

    			if (default_slot) {
    				default_slot.m(button, null);
    			}

    			current = true;

    			if (!mounted) {
    				dispose = listen_dev(button, "click", /*click*/ ctx[0], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (default_slot) {
    				if (default_slot.p && (!current || dirty & /*$$scope*/ 8)) {
    					update_slot_base(
    						default_slot,
    						default_slot_template,
    						ctx,
    						/*$$scope*/ ctx[3],
    						!current
    						? get_all_dirty_from_scope(/*$$scope*/ ctx[3])
    						: get_slot_changes(default_slot_template, /*$$scope*/ ctx[3], dirty, null),
    						null
    					);
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(default_slot, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(default_slot, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			if (default_slot) default_slot.d(detaching);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$i.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$i($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Button', slots, ['default']);
    	const dispatch = createEventDispatcher();

    	function click() {
    		dispatch('button', { text: action, attributes });
    	}

    	let { action } = $$props;
    	let { attributes = {} } = $$props;
    	const writable_props = ['action', 'attributes'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Button> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('action' in $$props) $$invalidate(1, action = $$props.action);
    		if ('attributes' in $$props) $$invalidate(2, attributes = $$props.attributes);
    		if ('$$scope' in $$props) $$invalidate(3, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		dispatch,
    		click,
    		action,
    		attributes
    	});

    	$$self.$inject_state = $$props => {
    		if ('action' in $$props) $$invalidate(1, action = $$props.action);
    		if ('attributes' in $$props) $$invalidate(2, attributes = $$props.attributes);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [click, action, attributes, $$scope, slots];
    }

    class Button extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$i, create_fragment$i, safe_not_equal, { action: 1, attributes: 2 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Button",
    			options,
    			id: create_fragment$i.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*action*/ ctx[1] === undefined && !('action' in props)) {
    			console.warn("<Button> was created without expected prop 'action'");
    		}
    	}

    	get action() {
    		throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set action(value) {
    		throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get attributes() {
    		throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set attributes(value) {
    		throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Welcome.svelte generated by Svelte v3.49.0 */

    // (6:0) <Button action="start" on:button>
    function create_default_slot_1$3(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Start Game");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1$3.name,
    		type: "slot",
    		source: "(6:0) <Button action=\\\"start\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    // (7:0) <Button action="join" on:button>
    function create_default_slot$4(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Join Game");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$4.name,
    		type: "slot",
    		source: "(7:0) <Button action=\\\"join\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$h(ctx) {
    	let button0;
    	let t;
    	let button1;
    	let current;

    	button0 = new Button({
    			props: {
    				action: "start",
    				$$slots: { default: [create_default_slot_1$3] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button0.$on("button", /*button_handler*/ ctx[0]);

    	button1 = new Button({
    			props: {
    				action: "join",
    				$$slots: { default: [create_default_slot$4] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button1.$on("button", /*button_handler_1*/ ctx[1]);

    	const block = {
    		c: function create() {
    			create_component(button0.$$.fragment);
    			t = space();
    			create_component(button1.$$.fragment);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(button0, target, anchor);
    			insert_dev(target, t, anchor);
    			mount_component(button1, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			const button0_changes = {};

    			if (dirty & /*$$scope*/ 4) {
    				button0_changes.$$scope = { dirty, ctx };
    			}

    			button0.$set(button0_changes);
    			const button1_changes = {};

    			if (dirty & /*$$scope*/ 4) {
    				button1_changes.$$scope = { dirty, ctx };
    			}

    			button1.$set(button1_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(button0.$$.fragment, local);
    			transition_in(button1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(button0.$$.fragment, local);
    			transition_out(button1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(button0, detaching);
    			if (detaching) detach_dev(t);
    			destroy_component(button1, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$h.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$h($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Welcome', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Welcome> was created with unknown prop '${key}'`);
    	});

    	function button_handler(event) {
    		bubble.call(this, $$self, event);
    	}

    	function button_handler_1(event) {
    		bubble.call(this, $$self, event);
    	}

    	$$self.$capture_state = () => ({ Button });
    	return [button_handler, button_handler_1];
    }

    class Welcome extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$h, create_fragment$h, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Welcome",
    			options,
    			id: create_fragment$h.name
    		});
    	}
    }

    /* src/Join.svelte generated by Svelte v3.49.0 */
    const file$g = "src/Join.svelte";

    // (18:0) {:else}
    function create_else_block$4(ctx) {
    	let p;
    	let label;
    	let t1;
    	let input;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			p = element("p");
    			label = element("label");
    			label.textContent = "Game code";
    			t1 = space();
    			input = element("input");
    			add_location(label, file$g, 19, 4, 408);
    			attr_dev(input, "type", "text");
    			add_location(input, file$g, 20, 4, 437);
    			add_location(p, file$g, 18, 2, 400);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    			append_dev(p, label);
    			append_dev(p, t1);
    			append_dev(p, input);
    			set_input_value(input, /*ccode*/ ctx[1]);

    			if (!mounted) {
    				dispose = listen_dev(input, "input", /*input_input_handler*/ ctx[7]);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*ccode*/ 2 && input.value !== /*ccode*/ ctx[1]) {
    				set_input_value(input, /*ccode*/ ctx[1]);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$4.name,
    		type: "else",
    		source: "(18:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (11:0) {#if $isServer}
    function create_if_block_1$1(ctx) {
    	let ol;
    	let li0;
    	let t2;
    	let li1;
    	let t4;
    	let li2;
    	let t6;
    	let h1;
    	let t7;

    	const block = {
    		c: function create() {
    			ol = element("ol");
    			li0 = element("li");
    			li0.textContent = `Get your friends to visit ${/*address*/ ctx[6]}`;
    			t2 = space();
    			li1 = element("li");
    			li1.textContent = "click Join Game";
    			t4 = space();
    			li2 = element("li");
    			li2.textContent = "enter this game code:";
    			t6 = space();
    			h1 = element("h1");
    			t7 = text(/*$servercode*/ ctx[3]);
    			add_location(li0, file$g, 12, 4, 248);
    			add_location(li1, file$g, 13, 4, 297);
    			add_location(li2, file$g, 14, 4, 326);
    			attr_dev(ol, "class", "svelte-1pf0yc9");
    			add_location(ol, file$g, 11, 2, 239);
    			add_location(h1, file$g, 16, 2, 367);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, ol, anchor);
    			append_dev(ol, li0);
    			append_dev(ol, t2);
    			append_dev(ol, li1);
    			append_dev(ol, t4);
    			append_dev(ol, li2);
    			insert_dev(target, t6, anchor);
    			insert_dev(target, h1, anchor);
    			append_dev(h1, t7);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*$servercode*/ 8) set_data_dev(t7, /*$servercode*/ ctx[3]);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(ol);
    			if (detaching) detach_dev(t6);
    			if (detaching) detach_dev(h1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$1.name,
    		type: "if",
    		source: "(11:0) {#if $isServer}",
    		ctx
    	});

    	return block;
    }

    // (30:0) {#if $isServer}
    function create_if_block$4(ctx) {
    	let p0;
    	let label0;
    	let t1;
    	let input0;
    	let t2;
    	let p1;
    	let label1;
    	let input1;
    	let t3;
    	let span;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			p0 = element("p");
    			label0 = element("label");
    			label0.textContent = "Choose a number of rounds";
    			t1 = space();
    			input0 = element("input");
    			t2 = space();
    			p1 = element("p");
    			label1 = element("label");
    			input1 = element("input");
    			t3 = space();
    			span = element("span");
    			span.textContent = "Let players vote on all answers (including their own)";
    			add_location(label0, file$g, 31, 4, 611);
    			attr_dev(input0, "type", "text");
    			add_location(input0, file$g, 32, 4, 656);
    			add_location(p0, file$g, 30, 2, 603);
    			attr_dev(input1, "type", "checkbox");
    			add_location(input1, file$g, 36, 4, 770);
    			add_location(span, file$g, 37, 4, 843);
    			attr_dev(label1, "class", "paper-check");
    			add_location(label1, file$g, 35, 4, 738);
    			attr_dev(p1, "class", "form-group");
    			add_location(p1, file$g, 34, 2, 711);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p0, anchor);
    			append_dev(p0, label0);
    			append_dev(p0, t1);
    			append_dev(p0, input0);
    			set_input_value(input0, /*$numrounds*/ ctx[4]);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, p1, anchor);
    			append_dev(p1, label1);
    			append_dev(label1, input1);
    			input1.checked = /*$playersCanVoteOnOwnAnswers*/ ctx[5];
    			append_dev(label1, t3);
    			append_dev(label1, span);

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[9]),
    					listen_dev(input1, "change", /*input1_change_handler*/ ctx[10])
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*$numrounds*/ 16 && input0.value !== /*$numrounds*/ ctx[4]) {
    				set_input_value(input0, /*$numrounds*/ ctx[4]);
    			}

    			if (dirty & /*$playersCanVoteOnOwnAnswers*/ 32) {
    				input1.checked = /*$playersCanVoteOnOwnAnswers*/ ctx[5];
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p0);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(p1);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$4.name,
    		type: "if",
    		source: "(30:0) {#if $isServer}",
    		ctx
    	});

    	return block;
    }

    // (43:0) <Button   action="joining"   attributes={{ clientcode: ccode, player: pname }}   on:button>
    function create_default_slot_1$2(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Join");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1$2.name,
    		type: "slot",
    		source: "(43:0) <Button   action=\\\"joining\\\"   attributes={{ clientcode: ccode, player: pname }}   on:button>",
    		ctx
    	});

    	return block;
    }

    // (49:0) <Button action="back" on:button>
    function create_default_slot$3(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Back");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$3.name,
    		type: "slot",
    		source: "(49:0) <Button action=\\\"back\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$g(ctx) {
    	let t0;
    	let p;
    	let label;
    	let t2;
    	let input;
    	let t3;
    	let t4;
    	let button0;
    	let t5;
    	let button1;
    	let current;
    	let mounted;
    	let dispose;

    	function select_block_type(ctx, dirty) {
    		if (/*$isServer*/ ctx[2]) return create_if_block_1$1;
    		return create_else_block$4;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = /*$isServer*/ ctx[2] && create_if_block$4(ctx);

    	button0 = new Button({
    			props: {
    				action: "joining",
    				attributes: {
    					clientcode: /*ccode*/ ctx[1],
    					player: /*pname*/ ctx[0]
    				},
    				$$slots: { default: [create_default_slot_1$2] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button0.$on("button", /*button_handler*/ ctx[11]);

    	button1 = new Button({
    			props: {
    				action: "back",
    				$$slots: { default: [create_default_slot$3] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button1.$on("button", /*button_handler_1*/ ctx[12]);

    	const block = {
    		c: function create() {
    			if_block0.c();
    			t0 = space();
    			p = element("p");
    			label = element("label");
    			label.textContent = "Enter your player name";
    			t2 = space();
    			input = element("input");
    			t3 = space();
    			if (if_block1) if_block1.c();
    			t4 = space();
    			create_component(button0.$$.fragment);
    			t5 = space();
    			create_component(button1.$$.fragment);
    			add_location(label, file$g, 25, 2, 498);
    			attr_dev(input, "type", "text");
    			add_location(input, file$g, 26, 2, 538);
    			add_location(p, file$g, 24, 0, 492);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if_block0.m(target, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, p, anchor);
    			append_dev(p, label);
    			append_dev(p, t2);
    			append_dev(p, input);
    			set_input_value(input, /*pname*/ ctx[0]);
    			insert_dev(target, t3, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, t4, anchor);
    			mount_component(button0, target, anchor);
    			insert_dev(target, t5, anchor);
    			mount_component(button1, target, anchor);
    			current = true;

    			if (!mounted) {
    				dispose = listen_dev(input, "input", /*input_input_handler_1*/ ctx[8]);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block0) {
    				if_block0.p(ctx, dirty);
    			} else {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(t0.parentNode, t0);
    				}
    			}

    			if (dirty & /*pname*/ 1 && input.value !== /*pname*/ ctx[0]) {
    				set_input_value(input, /*pname*/ ctx[0]);
    			}

    			if (/*$isServer*/ ctx[2]) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block$4(ctx);
    					if_block1.c();
    					if_block1.m(t4.parentNode, t4);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			const button0_changes = {};

    			if (dirty & /*ccode, pname*/ 3) button0_changes.attributes = {
    				clientcode: /*ccode*/ ctx[1],
    				player: /*pname*/ ctx[0]
    			};

    			if (dirty & /*$$scope*/ 8192) {
    				button0_changes.$$scope = { dirty, ctx };
    			}

    			button0.$set(button0_changes);
    			const button1_changes = {};

    			if (dirty & /*$$scope*/ 8192) {
    				button1_changes.$$scope = { dirty, ctx };
    			}

    			button1.$set(button1_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(button0.$$.fragment, local);
    			transition_in(button1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(button0.$$.fragment, local);
    			transition_out(button1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if_block0.d(detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(p);
    			if (detaching) detach_dev(t3);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(t4);
    			destroy_component(button0, detaching);
    			if (detaching) detach_dev(t5);
    			destroy_component(button1, detaching);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$g.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$g($$self, $$props, $$invalidate) {
    	let $isServer;
    	let $servercode;
    	let $numrounds;
    	let $playersCanVoteOnOwnAnswers;
    	validate_store(isServer, 'isServer');
    	component_subscribe($$self, isServer, $$value => $$invalidate(2, $isServer = $$value));
    	validate_store(servercode, 'servercode');
    	component_subscribe($$self, servercode, $$value => $$invalidate(3, $servercode = $$value));
    	validate_store(numrounds, 'numrounds');
    	component_subscribe($$self, numrounds, $$value => $$invalidate(4, $numrounds = $$value));
    	validate_store(playersCanVoteOnOwnAnswers, 'playersCanVoteOnOwnAnswers');
    	component_subscribe($$self, playersCanVoteOnOwnAnswers, $$value => $$invalidate(5, $playersCanVoteOnOwnAnswers = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Join', slots, []);
    	let pname = "", ccode = "", address = document.location;
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Join> was created with unknown prop '${key}'`);
    	});

    	function input_input_handler() {
    		ccode = this.value;
    		$$invalidate(1, ccode);
    	}

    	function input_input_handler_1() {
    		pname = this.value;
    		$$invalidate(0, pname);
    	}

    	function input0_input_handler() {
    		$numrounds = this.value;
    		numrounds.set($numrounds);
    	}

    	function input1_change_handler() {
    		$playersCanVoteOnOwnAnswers = this.checked;
    		playersCanVoteOnOwnAnswers.set($playersCanVoteOnOwnAnswers);
    	}

    	function button_handler(event) {
    		bubble.call(this, $$self, event);
    	}

    	function button_handler_1(event) {
    		bubble.call(this, $$self, event);
    	}

    	$$self.$capture_state = () => ({
    		servercode,
    		isServer,
    		numrounds,
    		playersCanVoteOnOwnAnswers,
    		Button,
    		pname,
    		ccode,
    		address,
    		$isServer,
    		$servercode,
    		$numrounds,
    		$playersCanVoteOnOwnAnswers
    	});

    	$$self.$inject_state = $$props => {
    		if ('pname' in $$props) $$invalidate(0, pname = $$props.pname);
    		if ('ccode' in $$props) $$invalidate(1, ccode = $$props.ccode);
    		if ('address' in $$props) $$invalidate(6, address = $$props.address);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		pname,
    		ccode,
    		$isServer,
    		$servercode,
    		$numrounds,
    		$playersCanVoteOnOwnAnswers,
    		address,
    		input_input_handler,
    		input_input_handler_1,
    		input0_input_handler,
    		input1_change_handler,
    		button_handler,
    		button_handler_1
    	];
    }

    class Join extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$g, create_fragment$g, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Join",
    			options,
    			id: create_fragment$g.name
    		});
    	}
    }

    /* src/BlobSmile.svelte generated by Svelte v3.49.0 */

    const file$f = "src/BlobSmile.svelte";

    function create_fragment$f(ctx) {
    	let path;

    	const block = {
    		c: function create() {
    			path = svg_element("path");
    			attr_dev(path, "id", "smile");
    			attr_dev(path, "class", "smile");
    			attr_dev(path, "stroke", "black");
    			attr_dev(path, "d", "M30 75 C 40 80, 60 80, 70 75");
    			attr_dev(path, "fill", "transparent");
    			add_location(path, file$f, 0, 2, 2);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, path, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(path);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$f.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$f($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('BlobSmile', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<BlobSmile> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class BlobSmile extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$f, create_fragment$f, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "BlobSmile",
    			options,
    			id: create_fragment$f.name
    		});
    	}
    }

    /* src/BlobFrown.svelte generated by Svelte v3.49.0 */

    const file$e = "src/BlobFrown.svelte";

    function create_fragment$e(ctx) {
    	let path;

    	const block = {
    		c: function create() {
    			path = svg_element("path");
    			attr_dev(path, "class", "frown");
    			attr_dev(path, "stroke", "black");
    			attr_dev(path, "d", "M30 80 C 40 75, 60 75, 70 80");
    			attr_dev(path, "fill", "transparent");
    			add_location(path, file$e, 0, 2, 2);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, path, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(path);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$e.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$e($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('BlobFrown', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<BlobFrown> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class BlobFrown extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$e, create_fragment$e, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "BlobFrown",
    			options,
    			id: create_fragment$e.name
    		});
    	}
    }

    /* src/Blob.svelte generated by Svelte v3.49.0 */
    const file$d = "src/Blob.svelte";

    function create_fragment$d(ctx) {
    	let svg;
    	let defs;
    	let radialGradient;
    	let stop0;
    	let stop1;
    	let g0;
    	let path0;
    	let animate0;
    	let path0_fill_value;
    	let g3;
    	let g1;
    	let circle0;
    	let circle1;
    	let g2;
    	let path1;
    	let animate1;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			defs = svg_element("defs");
    			radialGradient = svg_element("radialGradient");
    			stop0 = svg_element("stop");
    			stop1 = svg_element("stop");
    			g0 = svg_element("g");
    			path0 = svg_element("path");
    			animate0 = svg_element("animate");
    			g3 = svg_element("g");
    			g1 = svg_element("g");
    			circle0 = svg_element("circle");
    			circle1 = svg_element("circle");
    			g2 = svg_element("g");
    			path1 = svg_element("path");
    			animate1 = svg_element("animate");
    			attr_dev(stop0, "offset", "40%");
    			set_style(stop0, "stop-color", /*color*/ ctx[1]);
    			set_style(stop0, "stop-opacity", "1");
    			add_location(stop0, file$d, 44, 6, 1058);
    			attr_dev(stop1, "offset", "90%");
    			set_style(stop1, "stop-color", "rgb(0,0,0)");
    			set_style(stop1, "stop-opacity", "0.5");
    			add_location(stop1, file$d, 45, 6, 1128);
    			attr_dev(radialGradient, "id", /*id*/ ctx[0]);
    			attr_dev(radialGradient, "cx", "30%");
    			attr_dev(radialGradient, "cy", "30%");
    			attr_dev(radialGradient, "r", "120%");
    			attr_dev(radialGradient, "fx", "60%");
    			attr_dev(radialGradient, "fy", "50%");
    			add_location(radialGradient, file$d, 43, 4, 985);
    			add_location(defs, file$d, 42, 2, 974);
    			attr_dev(animate0, "id", "breathe");
    			attr_dev(animate0, "begin", "" + (/*breathvar*/ ctx[4] + "s"));
    			attr_dev(animate0, "fill", "remove");
    			attr_dev(animate0, "attributeName", "d");
    			attr_dev(animate0, "dur", "" + (1 + /*breathvar*/ ctx[4] + "s"));
    			attr_dev(animate0, "repeatCount", "indefinite");
    			attr_dev(animate0, "values", "M30 90 C 10 90, 20 50, 50 50 C 80 50, 90 90, 70 90 Z; M30 90 C 8 87, 20 50, 50 50 C 80 50, 92 87, 70 90 Z; M30 90 C 10 90, 20 50, 50 50 C 80 50, 90 90, 70 90 Z");
    			attr_dev(animate0, "keyTimes", "0;0.5;1");
    			add_location(animate0, file$d, 53, 6, 1373);
    			attr_dev(path0, "stroke", "black");
    			attr_dev(path0, "d", "M30 90 C 10 90, 20 50, 50 50 C 80 50, 90 90, 70 90 Z");
    			attr_dev(path0, "fill", path0_fill_value = "url(#" + /*id*/ ctx[0] + ")");
    			add_location(path0, file$d, 49, 4, 1252);
    			attr_dev(g0, "class", "body");
    			add_location(g0, file$d, 48, 2, 1231);
    			attr_dev(circle0, "fill", "black");
    			attr_dev(circle0, "cx", "48");
    			attr_dev(circle0, "cy", "70");
    			attr_dev(circle0, "r", "1");
    			add_location(circle0, file$d, 66, 6, 1816);
    			attr_dev(circle1, "fill", "black");
    			attr_dev(circle1, "cx", "52");
    			attr_dev(circle1, "cy", "70");
    			attr_dev(circle1, "r", "1");
    			add_location(circle1, file$d, 67, 6, 1868);
    			attr_dev(g1, "class", "eyes");
    			add_location(g1, file$d, 65, 4, 1793);
    			attr_dev(animate1, "id", "sad-to-happy");
    			attr_dev(animate1, "begin", "indefinite");
    			attr_dev(animate1, "fill", "remove");
    			attr_dev(animate1, "attributeName", "d");
    			attr_dev(animate1, "dur", "2s");
    			attr_dev(animate1, "from", smile);
    			attr_dev(animate1, "to", frown);
    			add_location(animate1, file$d, 77, 8, 2101);
    			attr_dev(path1, "id", "mouth");
    			attr_dev(path1, "class", "smile");
    			attr_dev(path1, "stroke", "black");
    			attr_dev(path1, "fill", "transparent");
    			attr_dev(path1, "d", smile);
    			add_location(path1, file$d, 70, 6, 1951);
    			attr_dev(g2, "class", "mouth");
    			add_location(g2, file$d, 69, 4, 1927);
    			attr_dev(g3, "class", "face");
    			add_location(g3, file$d, 64, 2, 1772);
    			attr_dev(svg, "viewBox", "0 0 100 100");
    			set_style(svg, "width", "10em");
    			set_style(svg, "height", "10em");
    			add_location(svg, file$d, 41, 0, 909);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, defs);
    			append_dev(defs, radialGradient);
    			append_dev(radialGradient, stop0);
    			append_dev(radialGradient, stop1);
    			append_dev(svg, g0);
    			append_dev(g0, path0);
    			append_dev(path0, animate0);
    			append_dev(svg, g3);
    			append_dev(g3, g1);
    			append_dev(g1, circle0);
    			append_dev(g1, circle1);
    			append_dev(g3, g2);
    			append_dev(g2, path1);
    			append_dev(path1, animate1);
    			/*animate1_binding*/ ctx[6](animate1);
    			/*path1_binding*/ ctx[7](path1);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*color*/ 2) {
    				set_style(stop0, "stop-color", /*color*/ ctx[1]);
    			}

    			if (dirty & /*id*/ 1) {
    				attr_dev(radialGradient, "id", /*id*/ ctx[0]);
    			}

    			if (dirty & /*id*/ 1 && path0_fill_value !== (path0_fill_value = "url(#" + /*id*/ ctx[0] + ")")) {
    				attr_dev(path0, "fill", path0_fill_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    			/*animate1_binding*/ ctx[6](null);
    			/*path1_binding*/ ctx[7](null);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$d.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const frown = "M30 80 C 40 75, 60 75, 70 80";
    const smile = "M30 75 C 40 80, 60 80, 70 75";

    function instance$d($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Blob', slots, []);
    	const breathvar = Math.random() + Math.random();
    	let mouth;
    	let anim;

    	onMount(async () => {
    		anim.addEventListener("beginEvent", function () {
    			mouth.setAttribute("d", expression == "smile" ? smile : frown);
    		});

    		anim.addEventListener("endEvent", function () {
    			anim.setAttribute("from", mouth.getAttribute("d"));
    		});
    	});

    	let { id, color = "#ffffff", expression = "smile" } = $$props;
    	const writable_props = ['id', 'color', 'expression'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Blob> was created with unknown prop '${key}'`);
    	});

    	function animate1_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			anim = $$value;
    			$$invalidate(2, anim);
    		});
    	}

    	function path1_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			mouth = $$value;
    			$$invalidate(3, mouth);
    		});
    	}

    	$$self.$$set = $$props => {
    		if ('id' in $$props) $$invalidate(0, id = $$props.id);
    		if ('color' in $$props) $$invalidate(1, color = $$props.color);
    		if ('expression' in $$props) $$invalidate(5, expression = $$props.expression);
    	};

    	$$self.$capture_state = () => ({
    		BlobSmile,
    		BlobFrown,
    		onMount,
    		breathvar,
    		mouth,
    		anim,
    		frown,
    		smile,
    		id,
    		color,
    		expression
    	});

    	$$self.$inject_state = $$props => {
    		if ('mouth' in $$props) $$invalidate(3, mouth = $$props.mouth);
    		if ('anim' in $$props) $$invalidate(2, anim = $$props.anim);
    		if ('id' in $$props) $$invalidate(0, id = $$props.id);
    		if ('color' in $$props) $$invalidate(1, color = $$props.color);
    		if ('expression' in $$props) $$invalidate(5, expression = $$props.expression);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*anim, expression*/ 36) {
    			if (anim) {
    				if (expression == "smile") {
    					anim.setAttribute("to", smile);
    				} else {
    					anim.setAttribute("to", frown);
    				}

    				anim.beginElement();
    			}
    		}
    	};

    	return [id, color, anim, mouth, breathvar, expression, animate1_binding, path1_binding];
    }

    class Blob$1 extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$d, create_fragment$d, safe_not_equal, { id: 0, color: 1, expression: 5 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Blob",
    			options,
    			id: create_fragment$d.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*id*/ ctx[0] === undefined && !('id' in props)) {
    			console.warn("<Blob> was created without expected prop 'id'");
    		}
    	}

    	get id() {
    		throw new Error("<Blob>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set id(value) {
    		throw new Error("<Blob>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get color() {
    		throw new Error("<Blob>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set color(value) {
    		throw new Error("<Blob>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get expression() {
    		throw new Error("<Blob>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set expression(value) {
    		throw new Error("<Blob>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Player.svelte generated by Svelte v3.49.0 */
    const file$c = "src/Player.svelte";

    function create_fragment$c(ctx) {
    	let div;
    	let blob;
    	let t0;
    	let p;
    	let span;
    	let t1_value = /*player*/ ctx[0].name + "";
    	let t1;
    	let div_class_value;
    	let current;

    	blob = new Blob$1({
    			props: {
    				id: /*player*/ ctx[0].name.replace(/\W/g, '-'),
    				color: /*player*/ ctx[0].color
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(blob.$$.fragment);
    			t0 = space();
    			p = element("p");
    			span = element("span");
    			t1 = text(t1_value);
    			attr_dev(span, "class", "svelte-1jnef4a");
    			add_location(span, file$c, 15, 3, 439);
    			attr_dev(p, "class", "svelte-1jnef4a");
    			add_location(p, file$c, 15, 0, 436);
    			attr_dev(div, "class", div_class_value = "" + (null_to_empty(/*size*/ ctx[1]) + " svelte-1jnef4a"));
    			add_location(div, file$c, 13, 0, 346);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(blob, div, null);
    			append_dev(div, t0);
    			append_dev(div, p);
    			append_dev(p, span);
    			append_dev(span, t1);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			const blob_changes = {};
    			if (dirty & /*player*/ 1) blob_changes.id = /*player*/ ctx[0].name.replace(/\W/g, '-');
    			if (dirty & /*player*/ 1) blob_changes.color = /*player*/ ctx[0].color;
    			blob.$set(blob_changes);
    			if ((!current || dirty & /*player*/ 1) && t1_value !== (t1_value = /*player*/ ctx[0].name + "")) set_data_dev(t1, t1_value);

    			if (!current || dirty & /*size*/ 2 && div_class_value !== (div_class_value = "" + (null_to_empty(/*size*/ ctx[1]) + " svelte-1jnef4a"))) {
    				attr_dev(div, "class", div_class_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(blob.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(blob.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(blob);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$c.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$c($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Player', slots, []);
    	let { player, size = 'normal' } = $$props;
    	const writable_props = ['player', 'size'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Player> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('player' in $$props) $$invalidate(0, player = $$props.player);
    		if ('size' in $$props) $$invalidate(1, size = $$props.size);
    	};

    	$$self.$capture_state = () => ({ Blob: Blob$1, player, size });

    	$$self.$inject_state = $$props => {
    		if ('player' in $$props) $$invalidate(0, player = $$props.player);
    		if ('size' in $$props) $$invalidate(1, size = $$props.size);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [player, size];
    }

    class Player extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$c, create_fragment$c, safe_not_equal, { player: 0, size: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Player",
    			options,
    			id: create_fragment$c.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*player*/ ctx[0] === undefined && !('player' in props)) {
    			console.warn("<Player> was created without expected prop 'player'");
    		}
    	}

    	get player() {
    		throw new Error("<Player>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set player(value) {
    		throw new Error("<Player>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get size() {
    		throw new Error("<Player>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set size(value) {
    		throw new Error("<Player>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Sound.svelte generated by Svelte v3.49.0 */
    const file$b = "src/Sound.svelte";

    function create_fragment$b(ctx) {
    	let audio;
    	let audio_src_value;

    	const block = {
    		c: function create() {
    			audio = element("audio");
    			attr_dev(audio, "id", /*effect*/ ctx[0]);
    			if (!src_url_equal(audio.src, audio_src_value = "./" + /*effect*/ ctx[0] + ".mp3")) attr_dev(audio, "src", audio_src_value);
    			add_location(audio, file$b, 14, 0, 277);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, audio, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*effect*/ 1) {
    				attr_dev(audio, "id", /*effect*/ ctx[0]);
    			}

    			if (dirty & /*effect*/ 1 && !src_url_equal(audio.src, audio_src_value = "./" + /*effect*/ ctx[0] + ".mp3")) {
    				attr_dev(audio, "src", audio_src_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(audio);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$b.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$b($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Sound', slots, []);
    	let { effect, play = 0 } = $$props;

    	afterUpdate(() => {
    		if (play > 0) {
    			const el = document.getElementById(effect);
    			el.currentTime = 0;
    			el.play();
    		}
    	});

    	const writable_props = ['effect', 'play'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Sound> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('effect' in $$props) $$invalidate(0, effect = $$props.effect);
    		if ('play' in $$props) $$invalidate(1, play = $$props.play);
    	};

    	$$self.$capture_state = () => ({ effect, play, afterUpdate });

    	$$self.$inject_state = $$props => {
    		if ('effect' in $$props) $$invalidate(0, effect = $$props.effect);
    		if ('play' in $$props) $$invalidate(1, play = $$props.play);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [effect, play];
    }

    class Sound extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$b, create_fragment$b, safe_not_equal, { effect: 0, play: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Sound",
    			options,
    			id: create_fragment$b.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*effect*/ ctx[0] === undefined && !('effect' in props)) {
    			console.warn("<Sound> was created without expected prop 'effect'");
    		}
    	}

    	get effect() {
    		throw new Error("<Sound>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set effect(value) {
    		throw new Error("<Sound>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get play() {
    		throw new Error("<Sound>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set play(value) {
    		throw new Error("<Sound>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Joining.svelte generated by Svelte v3.49.0 */
    const file$a = "src/Joining.svelte";

    function get_each_context$4(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	return child_ctx;
    }

    // (35:0) {:else}
    function create_else_block_1(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Connecting to server";
    			add_location(p, file$a, 35, 2, 672);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block_1.name,
    		type: "else",
    		source: "(35:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (26:0) {#if $numPlayers > 0}
    function create_if_block_1(ctx) {
    	let p;
    	let t0;
    	let t1;
    	let t2_value = (/*$numPlayers*/ ctx[1] > 1 ? 's' : '') + "";
    	let t2;
    	let t3;
    	let t4;
    	let div;
    	let current;
    	let each_value = /*$playerKeys*/ ctx[2];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$4(get_each_context$4(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			p = element("p");
    			t0 = text(/*$numPlayers*/ ctx[1]);
    			t1 = text(" player");
    			t2 = text(t2_value);
    			t3 = text(" joined");
    			t4 = space();
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(p, "class", "marginless");
    			add_location(p, file$a, 26, 2, 456);
    			attr_dev(div, "class", "players svelte-1g1jlnr");
    			add_location(div, file$a, 29, 2, 548);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    			append_dev(p, t0);
    			append_dev(p, t1);
    			append_dev(p, t2);
    			append_dev(p, t3);
    			insert_dev(target, t4, anchor);
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (!current || dirty & /*$numPlayers*/ 2) set_data_dev(t0, /*$numPlayers*/ ctx[1]);
    			if ((!current || dirty & /*$numPlayers*/ 2) && t2_value !== (t2_value = (/*$numPlayers*/ ctx[1] > 1 ? 's' : '') + "")) set_data_dev(t2, t2_value);

    			if (dirty & /*$players, $playerKeys*/ 12) {
    				each_value = /*$playerKeys*/ ctx[2];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$4(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$4(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    			if (detaching) detach_dev(t4);
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(26:0) {#if $numPlayers > 0}",
    		ctx
    	});

    	return block;
    }

    // (31:4) {#each $playerKeys as key}
    function create_each_block$4(ctx) {
    	let player;
    	let current;

    	player = new Player({
    			props: {
    				player: /*$players*/ ctx[3][/*key*/ ctx[7]]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(player.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(player, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const player_changes = {};
    			if (dirty & /*$players, $playerKeys*/ 12) player_changes.player = /*$players*/ ctx[3][/*key*/ ctx[7]];
    			player.$set(player_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(player.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(player.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(player, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$4.name,
    		type: "each",
    		source: "(31:4) {#each $playerKeys as key}",
    		ctx
    	});

    	return block;
    }

    // (42:0) {:else}
    function create_else_block$3(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Your host will begin the game when all players have joined.";
    			add_location(p, file$a, 42, 2, 842);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$3.name,
    		type: "else",
    		source: "(42:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (39:0) {#if $isServer}
    function create_if_block$3(ctx) {
    	let p;
    	let t1;
    	let button;
    	let current;

    	button = new Button({
    			props: {
    				action: "question",
    				$$slots: { default: [create_default_slot_1$1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button.$on("button", /*button_handler*/ ctx[5]);

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Click Begin when all your friends have joined.";
    			t1 = space();
    			create_component(button.$$.fragment);
    			add_location(p, file$a, 39, 2, 725);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    			insert_dev(target, t1, anchor);
    			mount_component(button, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(button.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(button.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    			if (detaching) detach_dev(t1);
    			destroy_component(button, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(39:0) {#if $isServer}",
    		ctx
    	});

    	return block;
    }

    // (41:2) <Button action="question" on:button>
    function create_default_slot_1$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Begin");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1$1.name,
    		type: "slot",
    		source: "(41:2) <Button action=\\\"question\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    // (45:0) <Button action="back" on:button>
    function create_default_slot$2(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Back");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$2.name,
    		type: "slot",
    		source: "(45:0) <Button action=\\\"back\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$a(ctx) {
    	let p;
    	let t1;
    	let h1;
    	let t2;
    	let t3;
    	let current_block_type_index;
    	let if_block0;
    	let t4;
    	let current_block_type_index_1;
    	let if_block1;
    	let t5;
    	let button;
    	let current;
    	const if_block_creators = [create_if_block_1, create_else_block_1];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*$numPlayers*/ ctx[1] > 0) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block0 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	const if_block_creators_1 = [create_if_block$3, create_else_block$3];
    	const if_blocks_1 = [];

    	function select_block_type_1(ctx, dirty) {
    		if (/*$isServer*/ ctx[4]) return 0;
    		return 1;
    	}

    	current_block_type_index_1 = select_block_type_1(ctx);
    	if_block1 = if_blocks_1[current_block_type_index_1] = if_block_creators_1[current_block_type_index_1](ctx);

    	button = new Button({
    			props: {
    				action: "back",
    				$$slots: { default: [create_default_slot$2] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button.$on("button", /*button_handler_1*/ ctx[6]);

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Game code";
    			t1 = space();
    			h1 = element("h1");
    			t2 = text(/*$gamecode*/ ctx[0]);
    			t3 = space();
    			if_block0.c();
    			t4 = space();
    			if_block1.c();
    			t5 = space();
    			create_component(button.$$.fragment);
    			attr_dev(p, "class", "marginless");
    			add_location(p, file$a, 22, 0, 355);
    			attr_dev(h1, "class", "marginless");
    			add_location(h1, file$a, 23, 0, 391);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, h1, anchor);
    			append_dev(h1, t2);
    			insert_dev(target, t3, anchor);
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert_dev(target, t4, anchor);
    			if_blocks_1[current_block_type_index_1].m(target, anchor);
    			insert_dev(target, t5, anchor);
    			mount_component(button, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (!current || dirty & /*$gamecode*/ 1) set_data_dev(t2, /*$gamecode*/ ctx[0]);
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block0 = if_blocks[current_block_type_index];

    				if (!if_block0) {
    					if_block0 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block0.c();
    				} else {
    					if_block0.p(ctx, dirty);
    				}

    				transition_in(if_block0, 1);
    				if_block0.m(t4.parentNode, t4);
    			}

    			let previous_block_index_1 = current_block_type_index_1;
    			current_block_type_index_1 = select_block_type_1(ctx);

    			if (current_block_type_index_1 !== previous_block_index_1) {
    				group_outros();

    				transition_out(if_blocks_1[previous_block_index_1], 1, 1, () => {
    					if_blocks_1[previous_block_index_1] = null;
    				});

    				check_outros();
    				if_block1 = if_blocks_1[current_block_type_index_1];

    				if (!if_block1) {
    					if_block1 = if_blocks_1[current_block_type_index_1] = if_block_creators_1[current_block_type_index_1](ctx);
    					if_block1.c();
    				}

    				transition_in(if_block1, 1);
    				if_block1.m(t5.parentNode, t5);
    			}

    			const button_changes = {};

    			if (dirty & /*$$scope*/ 1024) {
    				button_changes.$$scope = { dirty, ctx };
    			}

    			button.$set(button_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(if_block1);
    			transition_in(button.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block0);
    			transition_out(if_block1);
    			transition_out(button.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(h1);
    			if (detaching) detach_dev(t3);
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach_dev(t4);
    			if_blocks_1[current_block_type_index_1].d(detaching);
    			if (detaching) detach_dev(t5);
    			destroy_component(button, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$a.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$a($$self, $$props, $$invalidate) {
    	let $gamecode;
    	let $numPlayers;
    	let $playerKeys;
    	let $players;
    	let $isServer;
    	validate_store(gamecode$2, 'gamecode');
    	component_subscribe($$self, gamecode$2, $$value => $$invalidate(0, $gamecode = $$value));
    	validate_store(numPlayers, 'numPlayers');
    	component_subscribe($$self, numPlayers, $$value => $$invalidate(1, $numPlayers = $$value));
    	validate_store(playerKeys$1, 'playerKeys');
    	component_subscribe($$self, playerKeys$1, $$value => $$invalidate(2, $playerKeys = $$value));
    	validate_store(players, 'players');
    	component_subscribe($$self, players, $$value => $$invalidate(3, $players = $$value));
    	validate_store(isServer, 'isServer');
    	component_subscribe($$self, isServer, $$value => $$invalidate(4, $isServer = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Joining', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Joining> was created with unknown prop '${key}'`);
    	});

    	function button_handler(event) {
    		bubble.call(this, $$self, event);
    	}

    	function button_handler_1(event) {
    		bubble.call(this, $$self, event);
    	}

    	$$self.$capture_state = () => ({
    		gamecode: gamecode$2,
    		isServer,
    		players,
    		playerKeys: playerKeys$1,
    		numPlayers,
    		Button,
    		Player,
    		Sound,
    		$gamecode,
    		$numPlayers,
    		$playerKeys,
    		$players,
    		$isServer
    	});

    	return [
    		$gamecode,
    		$numPlayers,
    		$playerKeys,
    		$players,
    		$isServer,
    		button_handler,
    		button_handler_1
    	];
    }

    class Joining extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$a, create_fragment$a, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Joining",
    			options,
    			id: create_fragment$a.name
    		});
    	}
    }

    /* src/Countdown.svelte generated by Svelte v3.49.0 */
    const file$9 = "src/Countdown.svelte";

    function create_fragment$9(ctx) {
    	let div;
    	let p;
    	let t0;
    	let t1;
    	let svg;
    	let circle0;
    	let circle1;
    	let div_class_value;

    	const block = {
    		c: function create() {
    			div = element("div");
    			p = element("p");
    			t0 = text(/*count*/ ctx[2]);
    			t1 = space();
    			svg = svg_element("svg");
    			circle0 = svg_element("circle");
    			circle1 = svg_element("circle");
    			attr_dev(p, "class", "svelte-1m70lv1");
    			add_location(p, file$9, 101, 2, 1655);
    			attr_dev(circle0, "class", "path svelte-1m70lv1");
    			attr_dev(circle0, "cx", "52");
    			attr_dev(circle0, "cy", "52");
    			attr_dev(circle0, "r", "45");
    			attr_dev(circle0, "stroke-width", "12");
    			add_location(circle0, file$9, 103, 4, 1739);
    			attr_dev(circle1, "class", "timer svelte-1m70lv1");
    			attr_dev(circle1, "cx", "52");
    			attr_dev(circle1, "cy", "52");
    			attr_dev(circle1, "r", "45");
    			attr_dev(circle1, "stroke-width", "10");
    			attr_dev(circle1, "stroke-dasharray", "283");
    			set_style(circle1, "animation-duration", /*time*/ ctx[0] + "s");
    			add_location(circle1, file$9, 104, 4, 1808);
    			attr_dev(svg, "viewBox", "0 0 104 104");
    			attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr_dev(svg, "class", "svelte-1m70lv1");
    			add_location(svg, file$9, 102, 2, 1672);
    			attr_dev(div, "class", div_class_value = "" + (null_to_empty(/*size*/ ctx[1]) + " svelte-1m70lv1"));
    			add_location(div, file$9, 100, 0, 1634);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, p);
    			append_dev(p, t0);
    			append_dev(div, t1);
    			append_dev(div, svg);
    			append_dev(svg, circle0);
    			append_dev(svg, circle1);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*count*/ 4) set_data_dev(t0, /*count*/ ctx[2]);

    			if (dirty & /*time*/ 1) {
    				set_style(circle1, "animation-duration", /*time*/ ctx[0] + "s");
    			}

    			if (dirty & /*size*/ 2 && div_class_value !== (div_class_value = "" + (null_to_empty(/*size*/ ctx[1]) + " svelte-1m70lv1"))) {
    				attr_dev(div, "class", div_class_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$9.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$9($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Countdown', slots, []);
    	const dispatch = createEventDispatcher();
    	let count, int;

    	onMount(async () => {
    		$$invalidate(2, count = time);

    		int = setInterval(
    			function () {
    				const wasOne = count === 1;

    				if (count > 0) {
    					$$invalidate(2, count = count - 1);
    				}

    				if (count == 0 && wasOne) {
    					setTimeout(
    						() => {
    							if (count === 0) dispatch("timesUp");
    						},
    						1000
    					);
    				}
    			},
    			1000
    		);
    	});

    	onDestroy(() => {
    		clearInterval(int);
    	});

    	function reset() {
    		$$invalidate(2, count = time);
    	}

    	let { time, size = "normal" } = $$props;
    	const writable_props = ['time', 'size'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Countdown> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('time' in $$props) $$invalidate(0, time = $$props.time);
    		if ('size' in $$props) $$invalidate(1, size = $$props.size);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		onDestroy,
    		createEventDispatcher,
    		dispatch,
    		count,
    		int,
    		reset,
    		time,
    		size
    	});

    	$$self.$inject_state = $$props => {
    		if ('count' in $$props) $$invalidate(2, count = $$props.count);
    		if ('int' in $$props) int = $$props.int;
    		if ('time' in $$props) $$invalidate(0, time = $$props.time);
    		if ('size' in $$props) $$invalidate(1, size = $$props.size);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [time, size, count, reset];
    }

    class Countdown extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$9, create_fragment$9, safe_not_equal, { reset: 3, time: 0, size: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Countdown",
    			options,
    			id: create_fragment$9.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*time*/ ctx[0] === undefined && !('time' in props)) {
    			console.warn("<Countdown> was created without expected prop 'time'");
    		}
    	}

    	get reset() {
    		return this.$$.ctx[3];
    	}

    	set reset(value) {
    		throw new Error("<Countdown>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get time() {
    		throw new Error("<Countdown>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set time(value) {
    		throw new Error("<Countdown>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get size() {
    		throw new Error("<Countdown>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set size(value) {
    		throw new Error("<Countdown>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Question.svelte generated by Svelte v3.49.0 */

    const { console: console_1$3 } = globals;
    const file$8 = "src/Question.svelte";

    // (35:0) <Button action="back" on:button>
    function create_default_slot$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Back");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$1.name,
    		type: "slot",
    		source: "(35:0) <Button action=\\\"back\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$8(ctx) {
    	let div;
    	let countdown;
    	let t0;
    	let h1;
    	let t1;
    	let t2;
    	let t3;
    	let p0;
    	let t4;
    	let t5;
    	let p1;
    	let label;
    	let t7;
    	let textarea;
    	let t8;
    	let button0;
    	let t10;
    	let button1;
    	let t11;
    	let sound;
    	let current;
    	let mounted;
    	let dispose;
    	let countdown_props = { time: "60" };
    	countdown = new Countdown({ props: countdown_props, $$inline: true });
    	/*countdown_binding*/ ctx[5](countdown);
    	countdown.$on("timesUp", /*submitAnswer*/ ctx[4]);

    	button1 = new Button({
    			props: {
    				action: "back",
    				$$slots: { default: [create_default_slot$1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button1.$on("button", /*button_handler*/ ctx[7]);

    	sound = new Sound({
    			props: { effect: "splash", play: "0" },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(countdown.$$.fragment);
    			t0 = space();
    			h1 = element("h1");
    			t1 = text("Round ");
    			t2 = text(/*$round*/ ctx[2]);
    			t3 = space();
    			p0 = element("p");
    			t4 = text(/*$question*/ ctx[3]);
    			t5 = space();
    			p1 = element("p");
    			label = element("label");
    			label.textContent = "Your answer";
    			t7 = space();
    			textarea = element("textarea");
    			t8 = space();
    			button0 = element("button");
    			button0.textContent = "Submit";
    			t10 = space();
    			create_component(button1.$$.fragment);
    			t11 = space();
    			create_component(sound.$$.fragment);
    			attr_dev(div, "class", "counter-top-right");
    			add_location(div, file$8, 20, 2, 439);
    			add_location(h1, file$8, 24, 0, 547);
    			attr_dev(p0, "class", "question");
    			add_location(p0, file$8, 26, 0, 572);
    			add_location(label, file$8, 29, 2, 615);
    			attr_dev(textarea, "class", "svelte-1rc8oa9");
    			add_location(textarea, file$8, 30, 2, 644);
    			add_location(p1, file$8, 28, 0, 609);
    			add_location(button0, file$8, 33, 0, 680);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(countdown, div, null);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, h1, anchor);
    			append_dev(h1, t1);
    			append_dev(h1, t2);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, p0, anchor);
    			append_dev(p0, t4);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, p1, anchor);
    			append_dev(p1, label);
    			append_dev(p1, t7);
    			append_dev(p1, textarea);
    			set_input_value(textarea, /*ans*/ ctx[1]);
    			insert_dev(target, t8, anchor);
    			insert_dev(target, button0, anchor);
    			insert_dev(target, t10, anchor);
    			mount_component(button1, target, anchor);
    			insert_dev(target, t11, anchor);
    			mount_component(sound, target, anchor);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(textarea, "input", /*textarea_input_handler*/ ctx[6]),
    					listen_dev(button0, "click", /*submitAnswer*/ ctx[4], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			const countdown_changes = {};
    			countdown.$set(countdown_changes);
    			if (!current || dirty & /*$round*/ 4) set_data_dev(t2, /*$round*/ ctx[2]);
    			if (!current || dirty & /*$question*/ 8) set_data_dev(t4, /*$question*/ ctx[3]);

    			if (dirty & /*ans*/ 2) {
    				set_input_value(textarea, /*ans*/ ctx[1]);
    			}

    			const button1_changes = {};

    			if (dirty & /*$$scope*/ 512) {
    				button1_changes.$$scope = { dirty, ctx };
    			}

    			button1.$set(button1_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(countdown.$$.fragment, local);
    			transition_in(button1.$$.fragment, local);
    			transition_in(sound.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(countdown.$$.fragment, local);
    			transition_out(button1.$$.fragment, local);
    			transition_out(sound.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			/*countdown_binding*/ ctx[5](null);
    			destroy_component(countdown);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(h1);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(p0);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(p1);
    			if (detaching) detach_dev(t8);
    			if (detaching) detach_dev(button0);
    			if (detaching) detach_dev(t10);
    			destroy_component(button1, detaching);
    			if (detaching) detach_dev(t11);
    			destroy_component(sound, detaching);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$8.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$8($$self, $$props, $$invalidate) {
    	let $answer;
    	let $round;
    	let $question;
    	validate_store(answer, 'answer');
    	component_subscribe($$self, answer, $$value => $$invalidate(8, $answer = $$value));
    	validate_store(round, 'round');
    	component_subscribe($$self, round, $$value => $$invalidate(2, $round = $$value));
    	validate_store(question, 'question');
    	component_subscribe($$self, question, $$value => $$invalidate(3, $question = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Question', slots, []);
    	let timer;
    	let ans = "";

    	function submitAnswer() {
    		console.log('anser submitted');
    		set_store_value(answer, $answer = ans || timeoutAnswer, $answer);
    		$$invalidate(1, ans = "");
    		set_store_value(answer, $answer = null, $answer);
    		timer.reset();
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1$3.warn(`<Question> was created with unknown prop '${key}'`);
    	});

    	function countdown_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			timer = $$value;
    			$$invalidate(0, timer);
    		});
    	}

    	function textarea_input_handler() {
    		ans = this.value;
    		$$invalidate(1, ans);
    	}

    	function button_handler(event) {
    		bubble.call(this, $$self, event);
    	}

    	$$self.$capture_state = () => ({
    		onMount,
    		round,
    		question,
    		answer,
    		timeoutAnswer,
    		Button,
    		Sound,
    		Countdown,
    		timer,
    		ans,
    		submitAnswer,
    		$answer,
    		$round,
    		$question
    	});

    	$$self.$inject_state = $$props => {
    		if ('timer' in $$props) $$invalidate(0, timer = $$props.timer);
    		if ('ans' in $$props) $$invalidate(1, ans = $$props.ans);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		timer,
    		ans,
    		$round,
    		$question,
    		submitAnswer,
    		countdown_binding,
    		textarea_input_handler,
    		button_handler
    	];
    }

    class Question extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$8, create_fragment$8, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Question",
    			options,
    			id: create_fragment$8.name
    		});
    	}
    }

    /* src/Waiting.svelte generated by Svelte v3.49.0 */

    const file$7 = "src/Waiting.svelte";

    function create_fragment$7(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Waiting for everyone else - as usual!";
    			add_location(p, file$7, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$7($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Waiting', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Waiting> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Waiting extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Waiting",
    			options,
    			id: create_fragment$7.name
    		});
    	}
    }

    /* src/Vote.svelte generated by Svelte v3.49.0 */

    const { console: console_1$2 } = globals;

    const file$6 = "src/Vote.svelte";

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[9] = list[i];
    	child_ctx[11] = i;
    	return child_ctx;
    }

    // (57:2) {#each answers as answer, i}
    function create_each_block$3(ctx) {
    	let button;
    	let span;
    	let t0_value = /*answer*/ ctx[9][1] + "";
    	let t0;
    	let span_class_value;
    	let t1;
    	let button_class_value;
    	let mounted;
    	let dispose;

    	function click_handler(...args) {
    		return /*click_handler*/ ctx[8](/*answer*/ ctx[9], ...args);
    	}

    	const block = {
    		c: function create() {
    			button = element("button");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			attr_dev(span, "class", span_class_value = "" + (null_to_empty(/*answer*/ ctx[9][1] === timeoutAnswer ? 'noanswer' : '') + " svelte-vn6vyw"));
    			add_location(span, file$6, 62, 6, 2002);

    			attr_dev(button, "class", button_class_value = "answer " + (/*$vote*/ ctx[2] ? 'disabled' : '') + " " + (/*$vote*/ ctx[2] && /*$vote*/ ctx[2][1] == /*answer*/ ctx[9][0]
    			? 'my-vote'
    			: '') + " svelte-vn6vyw");

    			add_location(button, file$6, 57, 4, 1834);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);
    			append_dev(button, span);
    			append_dev(span, t0);
    			append_dev(button, t1);

    			if (!mounted) {
    				dispose = listen_dev(button, "click", click_handler, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*answers*/ 1 && t0_value !== (t0_value = /*answer*/ ctx[9][1] + "")) set_data_dev(t0, t0_value);

    			if (dirty & /*answers*/ 1 && span_class_value !== (span_class_value = "" + (null_to_empty(/*answer*/ ctx[9][1] === timeoutAnswer ? 'noanswer' : '') + " svelte-vn6vyw"))) {
    				attr_dev(span, "class", span_class_value);
    			}

    			if (dirty & /*$vote, answers*/ 5 && button_class_value !== (button_class_value = "answer " + (/*$vote*/ ctx[2] ? 'disabled' : '') + " " + (/*$vote*/ ctx[2] && /*$vote*/ ctx[2][1] == /*answer*/ ctx[9][0]
    			? 'my-vote'
    			: '') + " svelte-vn6vyw")) {
    				attr_dev(button, "class", button_class_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$3.name,
    		type: "each",
    		source: "(57:2) {#each answers as answer, i}",
    		ctx
    	});

    	return block;
    }

    // (69:0) {:else}
    function create_else_block$2(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Click on your favourite answer";
    			add_location(p, file$6, 69, 2, 2194);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$2.name,
    		type: "else",
    		source: "(69:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (67:0) {#if $vote}
    function create_if_block$2(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "Now we're just waiting for everyone else to vote";
    			add_location(p, file$6, 67, 2, 2128);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(67:0) {#if $vote}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let div0;
    	let countdown;
    	let t0;
    	let p;
    	let t1;
    	let t2;
    	let div1;
    	let t3;
    	let if_block_anchor;
    	let current;
    	countdown = new Countdown({ props: { time: "15" }, $$inline: true });
    	countdown.$on("timesUp", /*nullVote*/ ctx[4]);
    	let each_value = /*answers*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
    	}

    	function select_block_type(ctx, dirty) {
    		if (/*$vote*/ ctx[2]) return create_if_block$2;
    		return create_else_block$2;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			create_component(countdown.$$.fragment);
    			t0 = space();
    			p = element("p");
    			t1 = text(/*question*/ ctx[1]);
    			t2 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t3 = space();
    			if_block.c();
    			if_block_anchor = empty();
    			attr_dev(div0, "class", "counter-top-right");
    			add_location(div0, file$6, 50, 0, 1652);
    			attr_dev(p, "class", "question");
    			add_location(p, file$6, 54, 0, 1742);
    			attr_dev(div1, "class", "answers svelte-vn6vyw");
    			add_location(div1, file$6, 55, 0, 1777);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			mount_component(countdown, div0, null);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, p, anchor);
    			append_dev(p, t1);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div1, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}

    			insert_dev(target, t3, anchor);
    			if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (!current || dirty & /*question*/ 2) set_data_dev(t1, /*question*/ ctx[1]);

    			if (dirty & /*$vote, answers, answerSelected, timeoutAnswer*/ 13) {
    				each_value = /*answers*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$3(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div1, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (current_block_type !== (current_block_type = select_block_type(ctx))) {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(countdown.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(countdown.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			destroy_component(countdown);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(p);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div1);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t3);
    			if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let question;
    	let answers;
    	let $ballot;
    	let $myPeerId;
    	let $vote;
    	let $playersCanVoteOnOwnAnswers;
    	validate_store(ballot, 'ballot');
    	component_subscribe($$self, ballot, $$value => $$invalidate(5, $ballot = $$value));
    	validate_store(myPeerId, 'myPeerId');
    	component_subscribe($$self, myPeerId, $$value => $$invalidate(6, $myPeerId = $$value));
    	validate_store(vote, 'vote');
    	component_subscribe($$self, vote, $$value => $$invalidate(2, $vote = $$value));
    	validate_store(playersCanVoteOnOwnAnswers, 'playersCanVoteOnOwnAnswers');
    	component_subscribe($$self, playersCanVoteOnOwnAnswers, $$value => $$invalidate(7, $playersCanVoteOnOwnAnswers = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Vote', slots, []);

    	ballot.subscribe(function (b) {
    		console.log("ballot changed", b);
    	});

    	function answerSelected(answer) {
    		const votedFor = answer;
    		set_store_value(vote, $vote = [$ballot[0], votedFor[0]], $vote);
    		console.log("Vote is: ", $vote);
    	} // once I've vote show my vote and any incoming votes from server.
    	// server could broadcast my vote out again

    	// or broadcase my vote and any others.
    	// then when vote received would also need to broadcast to others that have answered
    	// need to allow for questions I can't vote on - should see other votes immediately.
    	// but only show other votes once I've voted.
    	// or do we just wait for everyone then animate???
    	// could send null vote if I'm not allow to vote or server could just choose to ignore.
    	// or do we just let people vote on their own in case they actually prefer someone elses
    	// if we want to have a timeout on votes however we need another option to all voted
    	// maybe add a vote to the ballot record
    	function nullVote() {
    		console.log("Null vote made");
    		set_store_value(vote, $vote = [$ballot[0], null], $vote);
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1$2.warn(`<Vote> was created with unknown prop '${key}'`);
    	});

    	const click_handler = (answer, e) => answerSelected(answer);

    	$$self.$capture_state = () => ({
    		onMount,
    		Countdown,
    		ballot,
    		vote,
    		allVoted,
    		myPeerId,
    		playersCanVoteOnOwnAnswers,
    		timeoutAnswer,
    		answerSelected,
    		nullVote,
    		answers,
    		question,
    		$ballot,
    		$myPeerId,
    		$vote,
    		$playersCanVoteOnOwnAnswers
    	});

    	$$self.$inject_state = $$props => {
    		if ('answers' in $$props) $$invalidate(0, answers = $$props.answers);
    		if ('question' in $$props) $$invalidate(1, question = $$props.question);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*$ballot*/ 32) {
    			$$invalidate(1, question = $ballot[1]);
    		}

    		if ($$self.$$.dirty & /*$ballot*/ 32) {
    			$$invalidate(0, answers = $ballot[2]);
    		}

    		if ($$self.$$.dirty & /*$playersCanVoteOnOwnAnswers, $ballot, $myPeerId*/ 224) {
    			{
    				if (!$playersCanVoteOnOwnAnswers) set_store_value(
    					vote,
    					$vote = $ballot[2].find(answer => answer[0] === $myPeerId)
    					? [$ballot[0], null]
    					: null,
    					$vote
    				); // this line stops people voting on rounds they are in by casting a null vote.
    			}
    		}
    	};

    	return [
    		answers,
    		question,
    		$vote,
    		answerSelected,
    		nullVote,
    		$ballot,
    		$myPeerId,
    		$playersCanVoteOnOwnAnswers,
    		click_handler
    	];
    }

    class Vote extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Vote",
    			options,
    			id: create_fragment$6.name
    		});
    	}
    }

    /* src/Voted.svelte generated by Svelte v3.49.0 */

    const { console: console_1$1 } = globals;
    const file$5 = "src/Voted.svelte";

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[12] = list[i];
    	child_ctx[14] = i;
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[15] = list[i];
    	child_ctx[17] = i;
    	return child_ctx;
    }

    // (135:8) {:else}
    function create_else_block$1(ctx) {
    	let div;
    	let t_value = /*answer*/ ctx[12][3].length + "";
    	let t;

    	const block = {
    		c: function create() {
    			div = element("div");
    			t = text(t_value);
    			attr_dev(div, "class", "vote-count badge svelte-1kzdrbb");
    			add_location(div, file$5, 135, 10, 4142);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*answers*/ 4 && t_value !== (t_value = /*answer*/ ctx[12][3].length + "")) set_data_dev(t, t_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(135:8) {:else}",
    		ctx
    	});

    	return block;
    }

    // (133:8) {#if answer[5]}
    function create_if_block$1(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			div.textContent = "SquidSplash!";
    			attr_dev(div, "class", "vote-count badge secondary squidsplash svelte-1kzdrbb");
    			add_location(div, file$5, 133, 10, 4045);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(133:8) {#if answer[5]}",
    		ctx
    	});

    	return block;
    }

    // (141:10) {#each answer[3] as vote, x}
    function create_each_block_1(ctx) {
    	let player;
    	let current;

    	player = new Player({
    			props: {
    				player: /*$players*/ ctx[4][/*vote*/ ctx[15]],
    				size: "small"
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(player.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(player, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const player_changes = {};
    			if (dirty & /*$players, answers*/ 20) player_changes.player = /*$players*/ ctx[4][/*vote*/ ctx[15]];
    			player.$set(player_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(player.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(player.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(player, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1.name,
    		type: "each",
    		source: "(141:10) {#each answer[3] as vote, x}",
    		ctx
    	});

    	return block;
    }

    // (130:2) {#each answers as answer, i}
    function create_each_block$2(ctx) {
    	let div4;
    	let div1;
    	let t0;
    	let div0;
    	let t1;
    	let button;
    	let t2_value = /*answer*/ ctx[12][1] + "";
    	let t2;
    	let t3;
    	let div3;
    	let div2;
    	let t4;
    	let t5_value = /*answer*/ ctx[12][4] + "";
    	let t5;
    	let t6;
    	let player;
    	let t7;
    	let current;

    	function select_block_type(ctx, dirty) {
    		if (/*answer*/ ctx[12][5]) return create_if_block$1;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);
    	let each_value_1 = /*answer*/ ctx[12][3];
    	validate_each_argument(each_value_1);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	player = new Player({
    			props: {
    				player: /*$players*/ ctx[4][/*answer*/ ctx[12][0]]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div4 = element("div");
    			div1 = element("div");
    			if_block.c();
    			t0 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t1 = space();
    			button = element("button");
    			t2 = text(t2_value);
    			t3 = space();
    			div3 = element("div");
    			div2 = element("div");
    			t4 = text("+");
    			t5 = text(t5_value);
    			t6 = space();
    			create_component(player.$$.fragment);
    			t7 = space();
    			attr_dev(div0, "class", "voters svelte-1kzdrbb");
    			add_location(div0, file$5, 139, 8, 4243);
    			attr_dev(div1, "class", "votes svelte-1kzdrbb");
    			add_location(div1, file$5, 131, 6, 3991);
    			attr_dev(button, "class", "disabled speech " + (/*i*/ ctx[14] % 2 == 0 ? 'left' : 'right') + " svelte-1kzdrbb");
    			add_location(button, file$5, 145, 6, 4415);
    			attr_dev(div2, "class", "badge svelte-1kzdrbb");
    			add_location(div2, file$5, 149, 8, 4550);
    			attr_dev(div3, "class", "tally svelte-1kzdrbb");
    			add_location(div3, file$5, 148, 6, 4522);
    			attr_dev(div4, "class", "answer svelte-1kzdrbb");
    			add_location(div4, file$5, 130, 4, 3964);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div4, anchor);
    			append_dev(div4, div1);
    			if_block.m(div1, null);
    			append_dev(div1, t0);
    			append_dev(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			append_dev(div4, t1);
    			append_dev(div4, button);
    			append_dev(button, t2);
    			append_dev(div4, t3);
    			append_dev(div4, div3);
    			append_dev(div3, div2);
    			append_dev(div2, t4);
    			append_dev(div2, t5);
    			append_dev(div4, t6);
    			mount_component(player, div4, null);
    			append_dev(div4, t7);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div1, t0);
    				}
    			}

    			if (dirty & /*$players, answers*/ 20) {
    				each_value_1 = /*answer*/ ctx[12][3];
    				validate_each_argument(each_value_1);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div0, null);
    					}
    				}

    				group_outros();

    				for (i = each_value_1.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}

    			if ((!current || dirty & /*answers*/ 4) && t2_value !== (t2_value = /*answer*/ ctx[12][1] + "")) set_data_dev(t2, t2_value);
    			if ((!current || dirty & /*answers*/ 4) && t5_value !== (t5_value = /*answer*/ ctx[12][4] + "")) set_data_dev(t5, t5_value);
    			const player_changes = {};
    			if (dirty & /*$players, answers*/ 20) player_changes.player = /*$players*/ ctx[4][/*answer*/ ctx[12][0]];
    			player.$set(player_changes);
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(player.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(player.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div4);
    			if_block.d();
    			destroy_each(each_blocks, detaching);
    			destroy_component(player);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$2.name,
    		type: "each",
    		source: "(130:2) {#each answers as answer, i}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$5(ctx) {
    	let p0;
    	let t1;
    	let p1;
    	let t2;
    	let t3;
    	let div;
    	let t4;
    	let sound0;
    	let t5;
    	let sound1;
    	let current;
    	let each_value = /*answers*/ ctx[2];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	sound0 = new Sound({
    			props: { effect: "horn", play: /*sound*/ ctx[0] },
    			$$inline: true
    		});

    	sound1 = new Sound({
    			props: { effect: "splash", play: /*bonus*/ ctx[1] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			p0 = element("p");
    			p0.textContent = "Everyone's done - let's take a look";
    			t1 = space();
    			p1 = element("p");
    			t2 = text(/*question*/ ctx[3]);
    			t3 = space();
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t4 = space();
    			create_component(sound0.$$.fragment);
    			t5 = space();
    			create_component(sound1.$$.fragment);
    			add_location(p0, file$5, 126, 0, 3829);
    			attr_dev(p1, "class", "question");
    			add_location(p1, file$5, 127, 0, 3872);
    			attr_dev(div, "class", "answers svelte-1kzdrbb");
    			add_location(div, file$5, 128, 0, 3907);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p0, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, p1, anchor);
    			append_dev(p1, t2);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			insert_dev(target, t4, anchor);
    			mount_component(sound0, target, anchor);
    			insert_dev(target, t5, anchor);
    			mount_component(sound1, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (!current || dirty & /*question*/ 8) set_data_dev(t2, /*question*/ ctx[3]);

    			if (dirty & /*$players, answers*/ 20) {
    				each_value = /*answers*/ ctx[2];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}

    			const sound0_changes = {};
    			if (dirty & /*sound*/ 1) sound0_changes.play = /*sound*/ ctx[0];
    			sound0.$set(sound0_changes);
    			const sound1_changes = {};
    			if (dirty & /*bonus*/ 2) sound1_changes.play = /*bonus*/ ctx[1];
    			sound1.$set(sound1_changes);
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(sound0.$$.fragment, local);
    			transition_in(sound1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(sound0.$$.fragment, local);
    			transition_out(sound1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(p1);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t4);
    			destroy_component(sound0, detaching);
    			if (detaching) detach_dev(t5);
    			destroy_component(sound1, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const displayDelay = 1000, finishDelay = 3000; // finished delay must be greater than displayDelay otherwise finish will fire before shutout notification

    function shutoutBonus() {
    	return 100;
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let question;
    	let answers;
    	let $ballot;
    	let $serverCommand;
    	let $numPlayers;
    	let $playersCanVoteOnOwnAnswers;
    	let $round;
    	let $players;
    	validate_store(ballot, 'ballot');
    	component_subscribe($$self, ballot, $$value => $$invalidate(5, $ballot = $$value));
    	validate_store(serverCommand, 'serverCommand');
    	component_subscribe($$self, serverCommand, $$value => $$invalidate(6, $serverCommand = $$value));
    	validate_store(numPlayers, 'numPlayers');
    	component_subscribe($$self, numPlayers, $$value => $$invalidate(7, $numPlayers = $$value));
    	validate_store(playersCanVoteOnOwnAnswers, 'playersCanVoteOnOwnAnswers');
    	component_subscribe($$self, playersCanVoteOnOwnAnswers, $$value => $$invalidate(8, $playersCanVoteOnOwnAnswers = $$value));
    	validate_store(round, 'round');
    	component_subscribe($$self, round, $$value => $$invalidate(9, $round = $$value));
    	validate_store(players, 'players');
    	component_subscribe($$self, players, $$value => $$invalidate(4, $players = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Voted', slots, []);
    	let sound = 0, bonus = 0;

    	function voteScore() {
    		return 20 * $round; // add round bonus in here
    	}

    	const sleep = t => new Promise(s => setTimeout(s, t));

    	onMount(async () => {
    		// answers is an array of all users answers to the question
    		// each individual answer is an array with 6 elements:
    		// 0: The peer id of the answering user
    		// 1: The answer
    		// 2: An array of peer ids of the players who voted for this answer
    		// 3: An array of peer ids of votes that have been counted for this answer (moved from 2)
    		// 4: The totaled score for this answer once votes have been counted
    		// 5: Boolean for whether the answer was a shutout (all players voted for this answer)
    		console.log("ansers are: ", answers);

    		const shutoutAt = $playersCanVoteOnOwnAnswers
    		? $numPlayers
    		: $numPlayers - 2;

    		for (var i = 0; i < answers.length; i++) {
    			const answer = answers[i];
    			const votes = answer[2];

    			for (var j = votes.length; j > 0; j--) {
    				const vote = votes.shift();
    				await sleep(displayDelay);
    				$$invalidate(0, sound += 1);
    				answer[3] = [...answer[3], vote];
    				answer[4] += voteScore();
    				$$invalidate(2, answers = [...answers]);
    			}

    			if (answer[3].length >= shutoutAt) {
    				await sleep(displayDelay);
    				answer[4] += shutoutBonus();
    				$$invalidate(1, bonus += 1);
    				answer[5] = true;
    				$$invalidate(2, answers = [...answers]);
    			}
    		}

    		await sleep(finishDelay);

    		$ballot[2].forEach(function (a, i) {
    			a.push(answers[i][4]);
    		});

    		set_store_value(serverCommand, $serverCommand = { command: "tally", data: $ballot[2] }, $serverCommand);
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1$1.warn(`<Voted> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({
    		onMount,
    		onDestroy,
    		ballot,
    		vote,
    		allVoted,
    		round,
    		players,
    		numPlayers,
    		serverCommand,
    		playersCanVoteOnOwnAnswers,
    		Player,
    		Sound,
    		displayDelay,
    		finishDelay,
    		sound,
    		bonus,
    		voteScore,
    		shutoutBonus,
    		sleep,
    		answers,
    		question,
    		$ballot,
    		$serverCommand,
    		$numPlayers,
    		$playersCanVoteOnOwnAnswers,
    		$round,
    		$players
    	});

    	$$self.$inject_state = $$props => {
    		if ('sound' in $$props) $$invalidate(0, sound = $$props.sound);
    		if ('bonus' in $$props) $$invalidate(1, bonus = $$props.bonus);
    		if ('answers' in $$props) $$invalidate(2, answers = $$props.answers);
    		if ('question' in $$props) $$invalidate(3, question = $$props.question);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*$ballot*/ 32) {
    			$$invalidate(3, question = $ballot[1]);
    		}

    		if ($$self.$$.dirty & /*$ballot*/ 32) {
    			$$invalidate(2, answers = $ballot[2].map(function (a) {
    				let el = JSON.parse(JSON.stringify(a)); // deep clone array so we dont change ballot
    				el.push([]);
    				el.push(0);
    				el.push(false);
    				return el;
    			}));
    		}
    	};

    	return [sound, bonus, answers, question, $players, $ballot];
    }

    class Voted extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Voted",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    /* src/Score.svelte generated by Svelte v3.49.0 */

    const file$4 = "src/Score.svelte";

    function create_fragment$4(ctx) {
    	let div;
    	let t;

    	const block = {
    		c: function create() {
    			div = element("div");
    			t = text(/*score*/ ctx[0]);
    			attr_dev(div, "class", "badge svelte-qeg82q");
    			add_location(div, file$4, 12, 0, 130);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, t);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*score*/ 1) set_data_dev(t, /*score*/ ctx[0]);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Score', slots, []);
    	let { score } = $$props;
    	const writable_props = ['score'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Score> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('score' in $$props) $$invalidate(0, score = $$props.score);
    	};

    	$$self.$capture_state = () => ({ score });

    	$$self.$inject_state = $$props => {
    		if ('score' in $$props) $$invalidate(0, score = $$props.score);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [score];
    }

    class Score extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { score: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Score",
    			options,
    			id: create_fragment$4.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*score*/ ctx[0] === undefined && !('score' in props)) {
    			console.warn("<Score> was created without expected prop 'score'");
    		}
    	}

    	get score() {
    		throw new Error("<Score>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set score(value) {
    		throw new Error("<Score>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/Scores.svelte generated by Svelte v3.49.0 */
    const file$3 = "src/Scores.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[9] = list[i];
    	return child_ctx;
    }

    // (57:2) {#each $playerKeys as key}
    function create_each_block$1(ctx) {
    	let div;
    	let score;
    	let t0;
    	let player;
    	let t1;
    	let current;

    	score = new Score({
    			props: {
    				score: /*$players*/ ctx[1][/*key*/ ctx[9]].score
    			},
    			$$inline: true
    		});

    	player = new Player({
    			props: {
    				player: /*$players*/ ctx[1][/*key*/ ctx[9]]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(score.$$.fragment);
    			t0 = space();
    			create_component(player.$$.fragment);
    			t1 = space();
    			attr_dev(div, "class", "score svelte-t47xaj");
    			add_location(div, file$3, 57, 4, 1036);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(score, div, null);
    			append_dev(div, t0);
    			mount_component(player, div, null);
    			append_dev(div, t1);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const score_changes = {};
    			if (dirty & /*$players, $playerKeys*/ 3) score_changes.score = /*$players*/ ctx[1][/*key*/ ctx[9]].score;
    			score.$set(score_changes);
    			const player_changes = {};
    			if (dirty & /*$players, $playerKeys*/ 3) player_changes.player = /*$players*/ ctx[1][/*key*/ ctx[9]];
    			player.$set(player_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(score.$$.fragment, local);
    			transition_in(player.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(score.$$.fragment, local);
    			transition_out(player.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(score);
    			destroy_component(player);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(57:2) {#each $playerKeys as key}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let div0;
    	let countdown;
    	let t0;
    	let p;
    	let t2;
    	let div1;
    	let t3;
    	let if_block_anchor;
    	let current;
    	countdown = new Countdown({ props: { time: "20" }, $$inline: true });
    	countdown.$on("timesUp", /*nextRound*/ ctx[4]);
    	let each_value = /*$playerKeys*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	let if_block = false  ;

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			create_component(countdown.$$.fragment);
    			t0 = space();
    			p = element("p");
    			p.textContent = `${/*getStatement*/ ctx[3]()}`;
    			t2 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t3 = space();
    			if_block_anchor = empty();
    			attr_dev(div0, "class", "counter-top-right");
    			add_location(div0, file$3, 49, 0, 853);
    			attr_dev(p, "class", "question");
    			add_location(p, file$3, 53, 0, 940);
    			attr_dev(div1, "class", "scores svelte-t47xaj");
    			add_location(div1, file$3, 55, 0, 982);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			mount_component(countdown, div0, null);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, p, anchor);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div1, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}

    			insert_dev(target, t3, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*$players, $playerKeys*/ 3) {
    				each_value = /*$playerKeys*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div1, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(countdown.$$.fragment, local);

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(countdown.$$.fragment, local);
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			destroy_component(countdown);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(p);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div1);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let $state;
    	let $numrounds;
    	let $round;
    	let $playerKeys;
    	let $players;
    	let $isServer;
    	validate_store(state, 'state');
    	component_subscribe($$self, state, $$value => $$invalidate(5, $state = $$value));
    	validate_store(numrounds, 'numrounds');
    	component_subscribe($$self, numrounds, $$value => $$invalidate(6, $numrounds = $$value));
    	validate_store(round, 'round');
    	component_subscribe($$self, round, $$value => $$invalidate(7, $round = $$value));
    	validate_store(playerKeys$1, 'playerKeys');
    	component_subscribe($$self, playerKeys$1, $$value => $$invalidate(0, $playerKeys = $$value));
    	validate_store(players, 'players');
    	component_subscribe($$self, players, $$value => $$invalidate(1, $players = $$value));
    	validate_store(isServer, 'isServer');
    	component_subscribe($$self, isServer, $$value => $$invalidate(2, $isServer = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Scores', slots, []);

    	const statements = [
    		`Let's have a look at the ol' scoreboard`,
    		`Here's how it's shaping up after round ${$round}`
    	];

    	function getStatement() {
    		return statements[Math.floor(Math.random() * statements.length)];
    	}

    	function nextRound() {
    		if ($round < $numrounds) {
    			set_store_value(state, $state = "question", $state);
    		} else {
    			set_store_value(state, $state = "gameover", $state);
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Scores> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({
    		players,
    		playerKeys: playerKeys$1,
    		isServer,
    		round,
    		numrounds,
    		state,
    		Player,
    		Score,
    		Countdown,
    		Button,
    		statements,
    		getStatement,
    		nextRound,
    		$state,
    		$numrounds,
    		$round,
    		$playerKeys,
    		$players,
    		$isServer
    	});

    	return [$playerKeys, $players, $isServer, getStatement, nextRound];
    }

    class Scores extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Scores",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src/Gameover.svelte generated by Svelte v3.49.0 */
    const file$2 = "src/Gameover.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	return child_ctx;
    }

    // (21:0) {#each winners as winner}
    function create_each_block(ctx) {
    	let li;
    	let player;
    	let t;
    	let current;

    	player = new Player({
    			props: { player: /*winner*/ ctx[7] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			li = element("li");
    			create_component(player.$$.fragment);
    			t = space();
    			attr_dev(li, "class", "svelte-1iyue8n");
    			add_location(li, file$2, 21, 2, 505);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, li, anchor);
    			mount_component(player, li, null);
    			append_dev(li, t);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(player.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(player.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(li);
    			destroy_component(player);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(21:0) {#each winners as winner}",
    		ctx
    	});

    	return block;
    }

    // (29:0) {:else}
    function create_else_block(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "First Equal!";
    			add_location(p, file$2, 29, 2, 626);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(29:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (27:0) {#if winners.length < 2}
    function create_if_block(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "For taking the Win";
    			add_location(p, file$2, 27, 2, 590);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(27:0) {#if winners.length < 2}",
    		ctx
    	});

    	return block;
    }

    // (33:0) <Button action="again" on:button>
    function create_default_slot_1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Play again");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1.name,
    		type: "slot",
    		source: "(33:0) <Button action=\\\"again\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    // (34:0) <Button action="back" on:button>
    function create_default_slot(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Start Over");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot.name,
    		type: "slot",
    		source: "(34:0) <Button action=\\\"back\\\" on:button>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$2(ctx) {
    	let h1;
    	let t1;
    	let ul;
    	let t2;
    	let t3;
    	let button0;
    	let t4;
    	let button1;
    	let current;
    	let each_value = /*winners*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	function select_block_type(ctx, dirty) {
    		if (/*winners*/ ctx[0].length < 2) return create_if_block;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	button0 = new Button({
    			props: {
    				action: "again",
    				$$slots: { default: [create_default_slot_1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button0.$on("button", /*button_handler*/ ctx[1]);

    	button1 = new Button({
    			props: {
    				action: "back",
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	button1.$on("button", /*button_handler_1*/ ctx[2]);

    	const block = {
    		c: function create() {
    			h1 = element("h1");
    			h1.textContent = "Congratulations";
    			t1 = space();
    			ul = element("ul");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t2 = space();
    			if_block.c();
    			t3 = space();
    			create_component(button0.$$.fragment);
    			t4 = space();
    			create_component(button1.$$.fragment);
    			add_location(h1, file$2, 18, 0, 447);
    			attr_dev(ul, "class", "svelte-1iyue8n");
    			add_location(ul, file$2, 19, 0, 472);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, h1, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, ul, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(ul, null);
    			}

    			insert_dev(target, t2, anchor);
    			if_block.m(target, anchor);
    			insert_dev(target, t3, anchor);
    			mount_component(button0, target, anchor);
    			insert_dev(target, t4, anchor);
    			mount_component(button1, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*winners*/ 1) {
    				each_value = /*winners*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(ul, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}

    			const button0_changes = {};

    			if (dirty & /*$$scope*/ 1024) {
    				button0_changes.$$scope = { dirty, ctx };
    			}

    			button0.$set(button0_changes);
    			const button1_changes = {};

    			if (dirty & /*$$scope*/ 1024) {
    				button1_changes.$$scope = { dirty, ctx };
    			}

    			button1.$set(button1_changes);
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(button0.$$.fragment, local);
    			transition_in(button1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(button0.$$.fragment, local);
    			transition_out(button1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(h1);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(ul);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t2);
    			if_block.d(detaching);
    			if (detaching) detach_dev(t3);
    			destroy_component(button0, detaching);
    			if (detaching) detach_dev(t4);
    			destroy_component(button1, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let $players;
    	let $playerKeys;
    	validate_store(players, 'players');
    	component_subscribe($$self, players, $$value => $$invalidate(3, $players = $$value));
    	validate_store(playerKeys$1, 'playerKeys');
    	component_subscribe($$self, playerKeys$1, $$value => $$invalidate(4, $playerKeys = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Gameover', slots, []);

    	const topScore = Math.max(...$playerKeys.map(function (k) {
    		return $players[k].score;
    	}));

    	const winningKeys = $playerKeys.filter(function (k) {
    		return $players[k].score == topScore;
    	});

    	const winners = winningKeys.map(function (k) {
    		return $players[k];
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Gameover> was created with unknown prop '${key}'`);
    	});

    	function button_handler(event) {
    		bubble.call(this, $$self, event);
    	}

    	function button_handler_1(event) {
    		bubble.call(this, $$self, event);
    	}

    	$$self.$capture_state = () => ({
    		players,
    		playerKeys: playerKeys$1,
    		Player,
    		Button,
    		topScore,
    		winningKeys,
    		winners,
    		$players,
    		$playerKeys
    	});

    	return [winners, button_handler, button_handler_1];
    }

    class Gameover extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Gameover",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* src/Music.svelte generated by Svelte v3.49.0 */

    const file$1 = "src/Music.svelte";

    function create_fragment$1(ctx) {
    	let div;
    	let input;
    	let label;
    	let t0;
    	let audio;
    	let t1;
    	let p;
    	let t2;
    	let a;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			input = element("input");
    			label = element("label");
    			t0 = space();
    			audio = element("audio");
    			t1 = space();
    			p = element("p");
    			t2 = text("Music: ");
    			a = element("a");
    			a.textContent = "https://www.bensound.com";
    			attr_dev(input, "type", "checkbox");
    			attr_dev(input, "id", "music-control");
    			attr_dev(input, "class", "svelte-4vul16");
    			add_location(input, file$1, 46, 0, 795);
    			attr_dev(label, "for", "music-control");
    			attr_dev(label, "class", "svelte-4vul16");
    			add_location(label, file$1, 46, 65, 860);
    			attr_dev(audio, "id", "music");
    			add_location(audio, file$1, 47, 0, 896);
    			attr_dev(a, "href", "https://www.bensound.com");
    			add_location(a, file$1, 48, 10, 927);
    			attr_dev(p, "class", "svelte-4vul16");
    			add_location(p, file$1, 48, 0, 917);
    			attr_dev(div, "class", "svelte-4vul16");
    			add_location(div, file$1, 45, 0, 789);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, input);
    			append_dev(div, label);
    			append_dev(div, t0);
    			append_dev(div, audio);
    			append_dev(div, t1);
    			append_dev(div, p);
    			append_dev(p, t2);
    			append_dev(p, a);

    			if (!mounted) {
    				dispose = listen_dev(input, "click", toggleMusic, false, false, false);
    				mounted = true;
    			}
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function toggleMusic(event) {
    	const audio = document.getElementById('music');

    	if (event.target.checked) {
    		if (audio.src == '') {
    			audio.src = './bensound-funnysong.mp3';
    		} // only download song if someone does want music on

    		audio.play();
    	} else {
    		audio.pause();
    	}
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Music', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Music> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({ toggleMusic });
    	return [];
    }

    class Music extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Music",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src/App.svelte generated by Svelte v3.49.0 */

    const { console: console_1 } = globals;
    const file = "src/App.svelte";

    function create_fragment(ctx) {
    	let link0;
    	let t0;
    	let link1;
    	let t1;
    	let logo;
    	let t2;
    	let div1;
    	let div0;
    	let switch_instance;
    	let t3;
    	let music;
    	let current;
    	logo = new Logo({ $$inline: true });
    	var switch_value = /*view*/ ctx[0];

    	function switch_props(ctx) {
    		return { $$inline: true };
    	}

    	if (switch_value) {
    		switch_instance = new switch_value(switch_props());
    		switch_instance.$on("button", /*button*/ ctx[1]);
    	}

    	music = new Music({ $$inline: true });

    	const block = {
    		c: function create() {
    			link0 = element("link");
    			t0 = space();
    			link1 = element("link");
    			t1 = space();
    			create_component(logo.$$.fragment);
    			t2 = space();
    			div1 = element("div");
    			div0 = element("div");
    			if (switch_instance) create_component(switch_instance.$$.fragment);
    			t3 = space();
    			create_component(music.$$.fragment);
    			attr_dev(link0, "rel", "stylesheet");
    			attr_dev(link0, "href", "https://unpkg.com/papercss/dist/paper.min.css");
    			add_location(link0, file, 119, 0, 2793);
    			attr_dev(link1, "rel", "stylesheet");
    			attr_dev(link1, "href", "/icons.css");
    			add_location(link1, file, 120, 0, 2872);
    			attr_dev(div0, "class", "center svelte-1h12ehw");
    			add_location(div0, file, 126, 0, 2949);
    			attr_dev(div1, "class", "wrapper svelte-1h12ehw");
    			add_location(div1, file, 125, 0, 2927);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, link0, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, link1, anchor);
    			insert_dev(target, t1, anchor);
    			mount_component(logo, target, anchor);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);

    			if (switch_instance) {
    				mount_component(switch_instance, div0, null);
    			}

    			insert_dev(target, t3, anchor);
    			mount_component(music, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (switch_value !== (switch_value = /*view*/ ctx[0])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;

    					transition_out(old_component.$$.fragment, 1, 0, () => {
    						destroy_component(old_component, 1);
    					});

    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props());
    					switch_instance.$on("button", /*button*/ ctx[1]);
    					create_component(switch_instance.$$.fragment);
    					transition_in(switch_instance.$$.fragment, 1);
    					mount_component(switch_instance, div0, null);
    				} else {
    					switch_instance = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(logo.$$.fragment, local);
    			if (switch_instance) transition_in(switch_instance.$$.fragment, local);
    			transition_in(music.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(logo.$$.fragment, local);
    			if (switch_instance) transition_out(switch_instance.$$.fragment, local);
    			transition_out(music.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(link0);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(link1);
    			if (detaching) detach_dev(t1);
    			destroy_component(logo, detaching);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div1);
    			if (switch_instance) destroy_component(switch_instance);
    			if (detaching) detach_dev(t3);
    			destroy_component(music, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function generateCode() {
    	const chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    	return chars[Math.floor(Math.random() * chars.length)] + chars[Math.floor(Math.random() * chars.length)] + chars[Math.floor(Math.random() * chars.length)] + chars[Math.floor(Math.random() * chars.length)];
    }

    function hello() {
    	alert("hello");
    }

    function instance($$self, $$props, $$invalidate) {
    	let $state;
    	let $round;
    	let $playersCanVoteOnOwnAnswers;
    	let $numPlayers;
    	let $playerKeys;
    	let $player;
    	let $clientcode;
    	let $servercode;
    	validate_store(state, 'state');
    	component_subscribe($$self, state, $$value => $$invalidate(3, $state = $$value));
    	validate_store(round, 'round');
    	component_subscribe($$self, round, $$value => $$invalidate(4, $round = $$value));
    	validate_store(playersCanVoteOnOwnAnswers, 'playersCanVoteOnOwnAnswers');
    	component_subscribe($$self, playersCanVoteOnOwnAnswers, $$value => $$invalidate(5, $playersCanVoteOnOwnAnswers = $$value));
    	validate_store(numPlayers, 'numPlayers');
    	component_subscribe($$self, numPlayers, $$value => $$invalidate(6, $numPlayers = $$value));
    	validate_store(playerKeys$1, 'playerKeys');
    	component_subscribe($$self, playerKeys$1, $$value => $$invalidate(7, $playerKeys = $$value));
    	validate_store(player, 'player');
    	component_subscribe($$self, player, $$value => $$invalidate(8, $player = $$value));
    	validate_store(clientcode, 'clientcode');
    	component_subscribe($$self, clientcode, $$value => $$invalidate(9, $clientcode = $$value));
    	validate_store(servercode, 'servercode');
    	component_subscribe($$self, servercode, $$value => $$invalidate(10, $servercode = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('App', slots, []);
    	let view = Welcome;
    	resetAllState();
    	let stateData = null;

    	state.subscribe(function (state) {
    		console.log("state is:", state);

    		switch (state) {
    			case "back":
    				//resetAllState();
    				//stateData = null;
    				//view = Welcome;
    				location.reload();
    				break;
    			case "start":
    				set_store_value(servercode, $servercode = generateCode(), $servercode);
    				$$invalidate(0, view = Join);
    				break;
    			case "join":
    				$$invalidate(0, view = Join);
    				break;
    			case "joining":
    				set_store_value(clientcode, $clientcode = stateData.clientcode, $clientcode);
    				set_store_value(player, $player = { name: stateData.player, score: 0 }, $player);
    				console.log("num players: ", $numPlayers, $playerKeys);
    				$$invalidate(0, view = Joining);
    				break;
    			case "again":
    				console.log('resetting game');
    				resetGame();
    				set_store_value(round, $round = 0, $round);
    				console.log('reset game', $round);
    			case "question":
    				if ($numPlayers < 3) set_store_value(playersCanVoteOnOwnAnswers, $playersCanVoteOnOwnAnswers = true, $playersCanVoteOnOwnAnswers);
    				set_store_value(round, $round += 1, $round);
    				$$invalidate(0, view = Question);
    				break;
    			case "waiting":
    				$$invalidate(0, view = Waiting);
    				break;
    			case "vote":
    				$$invalidate(0, view = Vote);
    				break;
    			case "voted":
    				$$invalidate(0, view = Voted);
    				break;
    			case "scores":
    				$$invalidate(0, view = Scores);
    				break;
    			case "gameover":
    				$$invalidate(0, view = Gameover);
    				break;
    			default:
    				console.log("no hanlder for", state);
    				resetAllState();
    				stateData = null;
    				$$invalidate(0, view = Welcome);
    		}
    	});

    	function button(event) {
    		stateData = event.detail.attributes;
    		set_store_value(state, $state = event.detail.text, $state);
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({
    		state,
    		player,
    		players,
    		clientcode,
    		servercode,
    		gamecode: gamecode$2,
    		isServer,
    		round,
    		playerKeys: playerKeys$1,
    		numPlayers,
    		playersCanVoteOnOwnAnswers,
    		resetAllState,
    		resetGame,
    		Logo,
    		Welcome,
    		Join,
    		Joining,
    		Question,
    		Waiting,
    		Vote,
    		Voted,
    		Scores,
    		Gameover,
    		Music,
    		view,
    		stateData,
    		generateCode,
    		button,
    		hello,
    		$state,
    		$round,
    		$playersCanVoteOnOwnAnswers,
    		$numPlayers,
    		$playerKeys,
    		$player,
    		$clientcode,
    		$servercode
    	});

    	$$self.$inject_state = $$props => {
    		if ('view' in $$props) $$invalidate(0, view = $$props.view);
    		if ('stateData' in $$props) stateData = $$props.stateData;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [view, button];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {
    		name: 'world'
    	}
    });

    return app;

})();
//# sourceMappingURL=bundle.js.map
