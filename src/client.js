import Peer from "peerjs";
import { state, players, player, clientcode, round, question, answer, ballot, vote, numrounds, myPeerId } from "./state.js"


let peer, connection, gamecode, connid, roundQuestions = [];

function reset() {
    peer = null;
    connection = null
    gamecode = null;
    connid = null;
    roundQuestions = [];
}

function cleanup() {
    if (peer) {
        peer.disconnect();
        peer.destroy();
    }
}

function processMessage(msg) {
    const { kind, data } = msg;
    switch (kind) {
        case 'players':
            players.update(function(p) {
                return data;
            });
            break;
        case 'questions':
            roundQuestions = data;
            question.set(data[0][1]);
            state.set('question')
                // round.update(function(r) {return r+1})
            break;
        case 'vote':
            vote.set(null)
            ballot.set(data);
            state.set('vote');
            break;
        case 'voted':
            ballot.set(data);
            state.set('voted');
            break;
        case 'scores':
            state.set('scores');
            break;
        case 'numrounds':
            numrounds.set(data);
            break;
        default:
            console.log('Recived unprocessable message', kind)
    }
}

function start(code) {
    reset();
    gamecode = code
    peer = new Peer();
    peer.on('open', function(id) {
        myPeerId.set(id)
        connid = id;
    });

    connection = peer.connect(gamecode);
    window.conn = connection;
    connection.on('open', function() {

        //window.conn = connection;
        // update player to force send;

        console.log('client connection got open')
        player.subscribe(function(player) {
            connection.send({ kind: 'addPlayer', data: player });
        });


        answer.subscribe(function(a) {
            if (a) {
                var i = roundQuestions.findIndex(function(i) { return i.length < 3 });
                roundQuestions[i].push(a);
                if (i >= roundQuestions.length - 1) {
                    connection.send({ kind: 'answers', data: roundQuestions })
                    state.set('waiting');
                    console.log('submitted answers', roundQuestions);
                } else {
                    question.set(roundQuestions[i + 1][1]);
                }
            }
        });

        vote.subscribe(function(v) {
            if (v) {
                console.log('sending vote');
                connection.send({ kind: 'vote', data: v });
            }
        })

        /*
                player.update(function(p) {
                    p['peer'] = connid;
                    return p;
                });
        */

    });
    connection.on('data', function(data) {
        processMessage(data)
    });

    connection.on('close', function() {
        alert("Sorry the connection to the game host was lost.");
        state.set('back');
    });
    /*
        peer.on('disconnected',function() {
            alert("Sorry the connection to the game host was lost.");
            $state = 'back';
        });
    */
    peer.on('close', function() {
        peer.destroy();
    });


}

clientcode.subscribe(function(code) {
    if ((code) && (code != '')) {
        start('squid-splash-' + code);
    }
});