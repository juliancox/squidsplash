import Peer from "peerjs";
import { players, servercode, player, round, state, question, answer, ballot, vote, allVoted, numrounds, myPeerId, serverCommand } from "./state.js"
import { questions } from "./questions.js";
import { colors } from "./colors.js";
const shuffled = questions.reduce((a, v) => a.splice(Math.floor(Math.random() * a.length), 0, v) && a, []);
const rainbow = colors.reduce((a, v) => a.splice(Math.floor(Math.random() * a.length), 0, v) && a, []);

let peer, gamecode, playerList, playerKeys, roundQuestions = [],
    connections = {},
    allAnswers = {},
    votingOn = 0,
    colorIndex = 0,
    numberOfRounds;

function reset() {
    peer = null;
    gamecode = null;
    playerList = null;
    playerKeys = null;
    roundQuestions = [];
    connections = {};
    allAnswers = {};
    votingOn = 0;
    colorIndex = 0;
}


function randColor() {
    const c = rainbow[colorIndex];
    colorIndex += 1;
    console.log('rand color', colorIndex, c)
    if (colorIndex >= rainbow.length) colorIndex = 0;
    return c
}

function scoreForAVote() {
    return 10;
}

function storedValue(store) {
    let result;
    store.update(function (s) {
        result = s;
        return s
    })
    return result
}

function cleanup() {
    if (peer) {
        peer.disconnect();
        peer.destroy();
    }
}

function tellAll(kind, data) {
    console.log('in tell all')
    for (let key in connections) {
        const conn = connections[key];
        conn.send({ kind: kind, data: data })
    }
}

function addPlayer(peer, data) {
    data.color = randColor();
    console.log('color:', data.color, peer)
    players.update(function (p) {
        p[peer] = data;
        return p;
    });
}

function answersByIndex() {
    let aHash = {};
    let result = []
    for (var key in allAnswers) {
        var quests = allAnswers[key];
        quests.forEach(function (q) {
            aHash[q[0]] = aHash[q[0]] || [q[0], q[1],
            []
            ]
            aHash[q[0]][2].push([key, q[2],
                []
            ]);
        });
    }
    for (var key in aHash) {
        result.push(aHash[key]);
    }
    return result;
}

function voteNextAnswer() {
    const thisOne = allAnswers[votingOn];
    votingOn += 1;
    if (thisOne) {
        tellAll('vote', thisOne);
        vote.set(null)
        ballot.set([...thisOne, []]);
        state.set('vote');
        return true;
    } else {
        console.log('time to tally votes');
        return false;
    }
}

function showScores() {
    tellAll('scores', null);
    state.set('scores');
}

function tallyVotes(answers) {
    // work out scores add them into the players list
    console.log('all answers is:', allAnswers);
    players.update(function (p) {
        answers.forEach(function (answer) {
            const submitter = answer[0];
            p[submitter]['score'] += answer[3];
        });
        console.log('players is:', p)
        return p
    });
    // update of players will automatically send details to clients through player subscribe below
}

function recordAnswer(peer, data) {
    allAnswers[peer] = data;
    if (Object.keys(allAnswers).length == playerKeys.length) {
        // index by question
        // semd questions one by one to clients for voting
        // all clients see all questions but you can vote for you own answer and if there is only one other answer
        allAnswers = answersByIndex();
        votingOn = 0;
        console.log('answers indexed', allAnswers);
        voteNextAnswer();
    }
}

function recordVote(peer, data) {
    // vote comes in with data of [question number, peer of answer liked]
    // need to find matching peer in first element of b[2] array and add incoming peer to list of voters
    console.log('recording vote', peer, data)
    ballot.update(function (b) {
        const ans = b[2].find(function (i) { return i[0] == data[1] });
        console.log('matching answer', ans)
        if (ans) ans[2].push(peer);
        b[3].push(peer);
        console.log('All answers now', allAnswers)
        return b;
    });
}

function processMessage(peer, msg) {
    console.log('process: ', msg)
    const { kind, data } = msg;
    switch (kind) {
        case 'addPlayer':
            console.log('adding player', peer, data)
            addPlayer(peer, data);
            break;
        case 'answers':
            recordAnswer(peer, data)
            console.log('received answers', data)
            break;
        case 'vote':
            recordVote(peer, data)
            break;
    }
}

function assignQuestions(round) {
    // shuffle members
    allAnswers = {}
    const keys = playerKeys.reduce((a, v) => a.splice(Math.floor(Math.random() * a.length), 0, v) && a, []);
    const nextQuestion = (round - 1) * playerKeys.length;
    let start = nextQuestion;
    const quests = keys.map(function (key, i) {
        const end = i == playerKeys.length - 1 ? nextQuestion : start + 1
        let result = [key, [
            [start, shuffled[start]],
            [end, shuffled[end]]
        ]];
        start += 1;
        return result
    })
    quests.forEach(function (q) {
        const key = q[0];
        if (key == gamecode) {
            // set questions for server (shared function?)
            roundQuestions = q[1];
            question.set(q[1][0][1]);
        } else {
            const conn = connections[key];
            console.log('sending msg:', conn.peer, q[1]);
            conn.send({ kind: 'questions', data: q[1] })
        }
    });
    // state.set('question');
    console.log(quests);
}

function start(code) {
    cleanup();
    reset();
    console.log('starting server with code', code)
    gamecode = code;
    console.log('status: ', status);
    console.log('Gamecode: ', gamecode);
    console.log('b4 cleanup')
      console.log('b4 players set')
    players.set({});
    console.log('b4 new peer')
    peer = new Peer(gamecode);
    console.log('server peer', peer)
    peer.on('open', function (id) {
        myPeerId.set(id);
        console.log('Sever peer ID is: ' + id);

        numrounds.subscribe(function (n) {
            numberOfRounds = n;
            tellAll('numrounds', numberOfRounds);
        });

        players.subscribe(function (ps) {
            console.log('players changed:', ps);
            playerKeys = Object.keys(ps)
            playerList = ps;
            tellAll('players', ps);
        })

        player.subscribe(function (player) {
            if (player) {
                addPlayer(gamecode, player);
            }
        });

        round.subscribe(function (r) {
            if (r > 0) {
                console.log('setting questions for round ', r);
                assignQuestions(r, 2)
            }
        });

        answer.subscribe(function (a) {
            console.log('answer is:', a, a ? true : false);
            if (a) {
                var i = roundQuestions.findIndex(function (i) { return i.length < 3 });
                roundQuestions[i].push(a);
                if (i >= roundQuestions.length - 1) {
                    state.set('waiting');
                    recordAnswer(gamecode, roundQuestions);
                } else {
                    question.set(roundQuestions[i + 1][1]);
                }
            }
        });


        vote.subscribe(function (v) {
            if (v) {
                recordVote(gamecode, v);
            }
        });

        allVoted.subscribe(function (b) {
            console.log('abv', b)
            if (b) {
                console.log('everyone has voted send results', b)
                tellAll('voted', b);
                state.set('voted');
            } else {
                console.log('not all voted')
            }
        });

        serverCommand.subscribe(function (sc) {
            if (sc) {
                const { command, data } = sc;
                switch (command) {
                    case "tally":
                        console.log('data is:', data)
                        tallyVotes(data);
                        if (!voteNextAnswer()) {
                            showScores();
                        };
                        break;
                    default:
                        console.log("cant respond to server command", cmd);
                }
            }
        });

    });
    peer.on('error', function (err) {
        console.log('server peer err:', err);
    })
    peer.on('connection', function (c) {
        connections[c.peer] = c;
        console.log('have connection', c);
        c.on('open', function () {
            console.log('client conn open');
            c.send({ kind: 'numrounds', data: numberOfRounds })
        });
        c.on('data', function (data) {
            console.log('Received client data', data);
            processMessage(c.peer, data);
        });
        c.on('close', function () {
            players.update(function (p) {
                delete p[c.peer];
                return p
            });
        });

    });

    peer.on('close', function () {
        // peer.destroy();
    });



}

servercode.subscribe(function (code) {
    if ((code) && (code != '')) {
        start('squid-splash-' + code);
    }
});