export const random = function(start, end) {

    return fetch('https://www.random.org/integers/?num=1&min=' + start + '&max=' + end + '&col=1&base=10&format=plain&rnd=new')
        .then(function(response) {
            return response.text();
        }).then(function(txt) {
            return parseInt(txt);
        });

}

export const asyncRandom = async function(start, end) {
    return await random(start, end);
}