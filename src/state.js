import { writable, derived } from 'svelte/store';

export const state = writable('welcome');
export const servercode = writable('');
export const clientcode = writable('');
export const player = writable(null);
export const players = writable([]);
export const numrounds = writable(1);
export const playersCanVoteOnOwnAnswers = writable(true)
export const round = writable(0);
export const answer = writable();
export const question = writable();
export const ballot = writable();
export const vote = writable();
export const myPeerId = writable();
export const serverCommand = writable();

export const gamecode = derived([servercode, clientcode], ([$servercode, $clientcode]) => $servercode ? $servercode : $clientcode);
export const isServer = derived([servercode, clientcode], ([$servercode, $clientcode]) => $servercode ? true : false);
export const playerKeys = derived([players], ([$players]) => Object.keys($players));
export const numPlayers = derived([playerKeys], ([$playerKeys]) => $playerKeys.length);
// export const allVoted = derived([ballot, numPlayers], ([$ballot, $numplayers]) => $ballot && $ballot[2] && ($ballot[2].reduce((count, answer) => count + answer[2].length, 0) >= $numplayers) ? $ballot : false);
export const allVoted = derived([ballot, numPlayers], ([$ballot, $numplayers]) => $ballot && $ballot[3] && ($ballot[3].length == $numplayers) ? $ballot : false);
export const iVoted = derived([ballot, player], ([$ballot, $myPeerId]) => $ballot && $ballot[2] && ($ballot[2].reduce((combined, answer) => combined.concat(answer[2]), []).indexOf($myPeerId) >= 0));

export const timeoutAnswer = 'Doh! Too slow to answer';

export const resetAllState = function () {
    state.set('welcome');
    servercode.set('');
    clientcode.set('');
    player.set(null);
    players.set([]);
    numrounds.set(1);
    playersCanVoteOnOwnAnswers.set(true)
    round.set(0);
    answer.set(null);
    question.set(null);
    ballot.set(null);
    vote.set(null);
    myPeerId.set(null);
    serverCommand.set(null);
}

export const resetGame = function() {
    round.set(0);
    answer.set(null);
    question.set(null);
    ballot.set(null);
    vote.set(null);
}